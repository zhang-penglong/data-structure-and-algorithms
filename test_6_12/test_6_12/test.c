# define  _CRT_SECURE_NO_WARNINGS 1
#pragma once
//#include <stdio.h>
//#include <string.h>

//int main()
//{
//	//char arr[] = { 'a', 'b', 'c', '\0', '2', '2', '2' };
//	char arr[] = "www.1234@qq.com";
//	char str[30];
//	strcpy(str, arr);  
//	char* sep = "@.";
//	char* tmp = NULL;
//	for (tmp = strtok(str, sep); tmp != NULL; tmp = strtok(NULL, sep))
//	{
//		printf("%s\n", tmp);
//	}
//	return 0;
//}

//int main()
//{
//	char* name = (char*)"shuju";
//	char str[20];
//	int n = snprintf(str, 20,"name=%s", name);
//	printf("%s\n", str);
//	return 0;
//}
//
//#include <stdio.h>
//int main()
//{ 
//	char str[10];
//	snprintf(str, 10, "%s","Hello World");
//	printf("%s\n", str);
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	printf("%p\n", Add);
//	printf("%p\n", &Add);
//	int(*pf)(int, int) = Add;
//	int ret=pf(10, 20);
//	printf("ret=%d\n", ret);
//	return 0;
//}

//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x*y;
//}
//
//int main()
//{
//	int(*pfArr[3])(int, int) = { Add, Sub, Mul };
//	int ret1=pfArr[2](10, 20);
//	printf("%d\n", ret1);
//	return 0;
//}


///////////////////////回调函数////////////////////

//将函数指针作为参数传递给另一个函数，当特定的条件发生时，通过此指针调用其所指向的函数，称之为回调函数
//void test()
//{
//	printf("Hello Linux!\n");
//}
//
//void print(void(*pf)())
//{
//	if (pf != NULL)
//	{
//		pf();
//	}
//}
//int main()
//{
//	print(test);
//	return 0;
//}


//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x*y;
//}
//
//int calc(int(*pf)(int, int))
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	if (pf != NULL)
//	{
//		return pf(x, y);
//	}
//	return 0;
//}
//
//int main()
//{
//	int(*pf[3])(int, int) = { Add, Sub, Mul };
//	for (int i = 0; i < 3; i++)
//	{
//		int ret=calc(pf[i]);
//		printf("%d\n", ret);
//	}
//	return 0;
//}

//enum Day
//{
//	Mon=1,
//	Tues=3,
//	Wed=0,
//	Thur=7
//};
//int main()
//{
//	//printf("Mon:%d Thes:%d Wed:%d Thur:%d\n", Mon,Tues,Wed,Thur);
//	enum Day day = Wed;
//	day = 20.0;
//	return 0;
//}

//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		static int a = 0;
//		a++;
//		printf("a=%d\n", a);
//	}
//	return 0;
//}



///////////////////结构体中应用函数指针///////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//struct Test
//{
    //c语言不支持函数作为成员函数
//	void print(int a, int b)
//	{
//		a = b = 10;
//		print("%d %d\n", a, b);
//	}
//
//};
//int main()
//{
//	return 0;
//}

//解决方案(linux操作系统-struct file对象中操作方法集合(文件的操作方法 struct file_operations))
//驱动程序的操作方法
void print(int a, int b)
{
	printf("a=%d,b=%d\n", a, b);
}
typedef struct Test
{
	void(*fun_t)(int, int);
}Test;
int main()
{
	Test* t = malloc(sizeof(Test));
	t->fun_t = print;//初始化
	t->fun_t(10, 20);
	free(t);
	return 0;
}


























