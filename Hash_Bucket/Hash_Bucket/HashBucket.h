#pragma once
#include <iostream>
#include <vector>
using namespace std;

namespace HashBucket
{
	template<class K>
	struct HashFunc
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	// 特化
	template<>
	struct HashFunc<string>
	{
		size_t operator()(const string& s)
		{
			size_t hash = 0;
			for (auto e : s)
			{
				hash += e;
				hash *= 31;
			}

			return hash;
		}
	};

	template<class K, class V>
	struct HashNode
	{
		HashNode<K, V>* _next;
		pair<K, V> _kv;
		HashNode(const pair<K, V>& kv)
		{
			_kv = kv;
			_next = nullptr;
		}
	};

	template<class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
		typedef HashNode<K, V> Node;
	public:
		//构造函数
		HashTable(size_t n = 10)
		{
			_tables.resize(n, nullptr);
			_n = 0;
		}
		//析构函数
		~HashTable()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur != nullptr)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_tables[i] = nullptr;
			}
		}
		//拷贝构造函数  ht2(ht1)
		HashTable(const HashTable& ht)
		{
			Hash hs;
			//开辟相同大小的空间
			_tables.resize(ht._tables.size());
			//遍历旧表,头插到新表
			for (size_t i = 0; i < ht._tables.size(); i++)
			{
				Node* cur = ht._tables[i];
				while (cur != nullptr)
				{
					Node* next = cur->_next;

					//计算新表的插入位置
					size_t hashi = hs(cur->_kv.first) % _tables.size();

					cur->_next = _tables[hashi];
					_tables[hashi] = cur;
					cur = next;
				}
			}
			_n = ht._n;
		}

		//ht2=ht1
		HashTable& operator=(HashTable ht)
		{
			_tables.swap(ht._tables);
			swap(_n, ht._n);

			return *this;
		}

		Node* Find(const K& key)
		{
			Hash hs;
			size_t hashi = hs(key) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur != nullptr)
			{
				if ((cur->_kv).first == key)
				{
					return cur;
				}
				cur = cur->_next;
			}
			return nullptr;
		}


		bool Insert(const pair<K, V>& kv)
		{
			//若键值对中的键值key已存在，则插入失败
			Node* ret = Find(kv.first);
			if (ret != nullptr)
			{
				return false;
			}

			//控制负载因子为1，即哈希表中实际存放的数据个数与哈希表长度长度相等
			if (_n == _tables.size())
			{
				//新表扩容到旧表的两倍
				vector<Node*> _newtables(_tables.size() * 2);
				//遍历旧表,取旧表结点头插到新表
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur != nullptr)
					{
						Node* nextnode = cur->_next;
						Hash hs;
						size_t hashi = hs(cur->_kv.first) % _newtables.size();
						cur->_next = _newtables[hashi];
						_newtables[hashi] = cur;

						cur = nextnode;
					}
					_tables[i] = nullptr;
				}

				//交换新表与旧表
				_tables.swap(_newtables);
			}

			//插入
			//计算插入位置
			Hash hs;
			size_t hashi = hs(kv.first) % _tables.size();
			Node* newnode = new Node(kv);

			//单链表头插
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;

			++_n;
			return true;
		}

		bool Erase(const K& key)
		{
			Hash hs;
			//确定待删除数据的位置
			size_t hashi = hs(key) % _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			while (cur != nullptr)
			{
				if ((cur->_kv).first == key)
				{
					//删除
					if (prev != nullptr)
					{
						prev->_next = cur->_next;
					}
					else
					{
						_tables[hashi] = cur->_next;
					}
					delete cur;

					--_n;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}
			return false;
		}
	private:
		vector<Node*> _tables;
		size_t _n;//记录哈希表中实际存放的数据个数
	};

	void TestHT1()
	{
		HashTable<int, int> ht;
		int a[] = { 1, 4, 24, 34, 7, 44, 17, 37 };
		for (auto e : a)
		{
			ht.Insert(make_pair(e, e));
		}

		ht.Insert(make_pair(5, 5));
		ht.Insert(make_pair(15, 15));
		ht.Insert(make_pair(25, 25));

		ht.Erase(5);
		ht.Erase(15);
		ht.Erase(25);
		ht.Erase(35);

		//ht.Print();

		HashTable<string, string> dict1;
		dict1.Insert(make_pair("sort", "排序"));
		dict1.Insert(make_pair("string", "字符串"));
		HashTable<string, string> dict2(dict1);
	}

}