# define  _CRT_SECURE_NO_WARNINGS 1
#include <set>
#include <map>
#include <iostream>
#include <string>
using namespace std;

//namespace bit
//{
	//键值对
	//template <class T1, class T2>
	//struct pair
	//{
	//	T1 first;
	//	T2 second;
	//	//键值对构造函数
	//	pair()
	//		:first(T1())
	//		, second(T2())
	//	{}
	//	//键值对拷贝构造函数
	//	pair(const T1& a, const T2& b)
	//		:first(a)
	//		, second(b)
	//	{}
	//};
//}

//int main()
//{
//	set<int> s1;
//	s1.insert(5);
//	s1.insert(4);
//	s1.insert(3);
//	s1.insert(6);
//	s1.insert(7);
//	s1.insert(1);
//	s1.insert(5);
//
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	set<int>::iterator pos = s1.find(7);
//	s1.insert(pos, 8);
//	set<int>::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	set<int> s1;
//	s1.insert(5);
//	s1.insert(4);
//	s1.insert(3);
//	s1.insert(6);
//	s1.insert(7);
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//erase()版本1
//	set<int>::iterator pos = s1.find(3);
//	if (pos != s1.end())
//	{
//		cout << "查找到元素val,删除val:" << endl;
//		s1.erase(pos);
//	}
//	for (auto& e : s1)
//	{
//		cout << e <<" ";
//	}
//	cout << endl;
//
//	//erase版本2
//	//删除的元素存在,返回1
//	size_t ret1 = s1.erase(5);
//	cout << "ret1=" << ret1 << endl;
//
//	//删除的元素不存在不做任何处理,返回0
//	size_t ret2 = s1.erase(30);
//	cout << "ret2=" << ret2 << endl;
//
//	for (auto& e : s1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	cout << s1.size() << endl;
//	cout << s1.empty() << endl;
//	s1.clear();
//	for (auto& e : s1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	cout << s1.size() << endl;
//	cout << s1.empty() << endl;
//	return 0;
//}


//int main()
//{
//	set<int> s1;
//	s1.insert(5);
//	s1.insert(4);
//	s1.insert(3);
//	s1.insert(6);
//	s1.insert(7);
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//使用count查找set容器中查找值为val的元素,返回值为val的元素个数
//	//(val在set容器中,返回1;  val不在set容器中,返回0;)
//	if (s1.count(5))
//	{
//		cout << "在" << endl;
//	}
//	else
//	{
//		cout << "不在" << endl;
//	}
//	return 0;
//}
//int main()
//{
//	set<int> s1;
//	s1.insert(7);
//	s1.insert(3);
//	s1.insert(9);
//	s1.insert(1);
//	s1.insert(4);
//	s1.insert(6);
//	s1.insert(10);
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	// >= val
//	set<int>::iterator itlow = s1.lower_bound(2);
//	cout << *itlow << endl;
//	// > val
//	set<int>::iterator itup = s1.upper_bound(7);
//	cout << *itup << endl;
//	//遍历[itlow,itup)区间范围内的元素
//	while (itlow != itup)
//	{
//		cout << *itlow << " ";
//		++itlow;
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	// 排序
//	multiset<int> s;
//	s.insert(5);
//	s.insert(1);
//	s.insert(6);
//	s.insert(3);
//	s.insert(4);
//	s.insert(1);
//	s.insert(1);
//	s.insert(5);
//	s.insert(2);
//	s.insert(10);
//	multiset<int>::iterator it = s.begin();
//	while (it != s.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	//multiset容器count函数返回值不再非1即0
//	cout << "cout1=" << s.count(1) << endl;
//	cout << "cout2=" << s.count(5) << endl;
//	//find()函数返回中序遍历multiset容器中第一个值为val元素的迭代器
//	it = s.find(5);
//	while (it != s.end() && *it == 5)
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	return 0;
//}
//int main()
//{
//	map<string, string> dict;
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("integrity", "正直"));
//	dict.insert(make_pair("endeavor", "努力"));
//	dict.insert(make_pair("learn", "推断"));
//	// 迭代器遍历
//	map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		//cout << *it << endl;(×)(it为结构体指针,*it为pair)
//
//		// 访问方式一(√)
//		//cout << (*it).first << ":" << (*it).second << endl;
//
//		//访问方式二(√)
//		cout << it->first << ":" << it->second << endl;
//		++it;
//	}
//	return 0;
//}

//int main()
//{
//	map<string, string> dict;
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("integrity", "正直"));
//	dict.insert(make_pair("endeavor", "努力"));
//	dict.insert(make_pair("learn", "推断"));
//	map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		cout << (*it).first << ":" << (*it).second << endl;
//		++it;
//	}
//	cout << endl;
//
//	//[]功能一: 插入
//	dict["sort"];
//	//[]功能二: 查找
//	cout << dict["insistence"] << endl;
//	//[]功能三: 修改
//	dict["learn"] = "推断";
//	//[]功能四: 插入+修改
//	dict["charming"] = "迷人的";
//
//	for (auto& kv : dict)//范围for遍历时加上引用,减少拷贝,提升效率
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//	return 0;
//}
////模拟重载如下:
//value& operator[](const key& k)
//{
//	pair<iterator, bool> ret = insert(make_pair(k, value()));
//	return *(ret.first).second
//}

//int main()
//{
//	map<string, string> dict;
//	dict.insert(pair<string, string>("sort", "排序"));
//
//	pair<string, string> kv("string", "字符串");
//	//pair<string, string> kv = { "string", "字符串" };
//	dict.insert(kv);
//
//	// C++11 多参数隐式类型转换(构造函数)
//	dict.insert({ "apple", "苹果" });
//
//	// C++98
//	dict.insert(make_pair("sort", "排序"));
//
//	//map<string, string>::iterator it = dict.begin();
//	auto it = dict.begin();
//	while (it != dict.end())
//	{
//		//cout << *it << endl;
//		//cout << (*it).first << (*it).second << endl;
//		cout << it->first << it->second << endl;
//		++it;
//	}
//	cout << endl;
//
//	for (auto& kv : dict)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	return 0;
//}

//int main()
//{
//	multimap<string, string> dict;
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("integrity", "正直"));
//	dict.insert(make_pair("endeavor", "努力"));
//	dict.insert(make_pair("learn", "推断"));
//
//	// 迭代器遍历
//	multimap<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		cout << it->first << ":" << it->second << endl;
//		++it;
//	}
//	return 0;
//}

//统计次数
int main()
{
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
		"苹果", "香蕉", "苹果", "西瓜", "香蕉", "草莓" };
	map<string, int> countMap;
	for (auto& e : arr)
	{
		map<string, int>::iterator it = countMap.find(e);
		if (it != countMap.end())
		{
			it->second++;
		}
		else
		{
			countMap.insert(make_pair(e, 1));
		}
	}

	for (auto& e : arr)
	{
		pair<map<string, int>::iterator, bool> ret;
		ret = countMap.insert(make_pair(e, 1));

		// 已经存在了
		if (ret.second == false)
		{
			ret.first->second++;
		}
	}

	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;

	return 0;
}




