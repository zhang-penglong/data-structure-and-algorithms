# define  _CRT_SECURE_NO_WARNINGS 1

#include "BinarySearchTree.h"

//int main()
//{
//	BinarySearchTree<int> T;
//	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
//	//for (auto e : a)
//	//{
//	//	T.Insert(e);
//	//}
//	//T.InOrder();
//	//T.Erase(14);//单孩子结点
//	//T.InOrder();
//	//T.Erase(1);//叶子节点
//	//T.InOrder();
//	//T.Erase(8);//根
//	//T.InOrder();
//
//
//	BinarySearchTree<int> t;
//	for (auto e : a)
//	{
//		t.Insert(e);
//	}
//	t.InOrder();
//
//	for (auto e : a)
//	{
//		t.Erase(e);
//		t.InOrder();
//	}
//	t.InOrder();
//	return 0;
//}

int main()
{
	BinarySearchTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
	{
		t.Insert(e);
	}
	t.InOrder();

	BinarySearchTree<int> t1(t);
	t1.InOrder();

	return 0;
}







