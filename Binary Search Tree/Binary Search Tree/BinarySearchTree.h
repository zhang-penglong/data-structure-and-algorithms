
#include <iostream>
using namespace std;

//结点类的实现
template <class K>
struct BinarySearchTreeNode
{
	typedef BinarySearchTreeNode Node;
	Node* _left;//指向左子树
	Node* _right;//指向右子树
	K _key;//存储当前结点数据

	//开辟新结点需要构造函数
	BinarySearchTreeNode(const K& key)
		:_left(nullptr)
		,_right(nullptr)
		,_key(key)
	{}
};

template <class K>
class BinarySearchTree
{
	typedef BinarySearchTreeNode<K> Node;
public:
	//搜索二叉树的查找,找到了返回true,否则返回false
	//思路: 首先与根结点处的键值进行比较,
	//若key小于根结点处的键值,则去左子树查找;若key大于根结点处的键值,则去右子树查找;
	//查找过程中走到结点指针为空,找不到key,返回false;
	//若查找过程中某个结点数据与key相等,停止查找,返回true;
	bool Find(const K& key)
	{
		Node* cur = _root;
		while (cur != nullptr)
		{
			//key大于根结点处的键值,则去右子树查找
			if (cur->_key < key)
			{
				cur = cur->_right;
			}
			//若key小于根结点处的键值,则去左子树查找
			else if (cur->_key > key)
			{
				cur = cur->_left;
			}
			//查找过程中某个结点数据与key相等,停止查找,返回true;
			else
			{
				return true;
			}
		}
		//cur走空,找不到返回false
		return false;
	}
    //搜索二叉树的插入
	//思路:插入的要求是插入的结点依旧维持搜索二叉树的逻辑结构
	//首先确定插入的位置,从根节点开始,将插入的数值与该结点处的数值相比较
	//若大于该结点处的数值，前往该节点作为根节点的右子树查找
	//若小于该结点处的数值，前往该节点作为根节点的左子树查找
	//直至走到结点指针为空的位置,每次查找需要记录该结点的父亲结点的位置,方便建立链接关系
	bool Insert(const K& key)
	{
		// 空树
		if (_root == nullptr)
		{
			_root= new Node(key);
			return true;
		}
		//非空树
		//parent记录父节点位置
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key>key)
			{
				parent = cur;
				cur = cur->_left;
			}
			//为维护搜索二叉树逻辑结构,同一颗树不能出现相等的数值
			else
			{
				return false;
			}
		}
		//cur已经走空,开辟节点,存储数据,建立链接关系
		cur = new Node(key);
		//存储值为key的结点链接到父节点的何处？
		if (parent->_key > key)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		return true;
	}
	//搜索二叉树的遍历
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	//搜索二叉树的删除:删除的要求是删除结点后依旧维持搜索二叉树的逻辑结构
	//首先查找待删除元素是否在搜索二叉树中,不在返回false,否则返回true;
	//
	bool Erase(const K& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key>key)
			{
				parent = cur;
				cur = cur->_left;
			}
			//查找到待删除元素所在的结点
			else
			{
				//删除结点
				//删除结点分为如下三种情形
			//1.删除叶子结点 
	        //2.删除单孩子结点(其中又分为待删除节点处的左孩子不存在,右孩子存在或者左孩子存在,右孩不存在)
            //3.删除左右孩子皆存在的结点
            //情况1与情况2具有相同的删除逻辑

				//待删除结点左孩子不存在,右孩子存在(注:叶子结点按照此处逻辑进行删除)
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_right;
					}
					else
					{
						if (parent->_key < cur->_key)
						{
							parent->_right = cur->_right;
						}
						else
						{
							parent->_left = cur->_right;
						}
					}
					delete cur;
					return true;
				}

				//待删除结点左孩子存在,右孩子不存在
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_left;
					}
					else
					{
						if (parent->_key < cur->_key)
						{
							parent->_right = cur->_left;
						}
						else
						{
							parent->_left = cur->_right;
						}
					}
					delete cur;
					return true;
				}
				//删除左右孩子皆存在的结点---替换法删除
				//替换法思路: 
				//1.选取待删除结点作为根结点的左子树的最大结点(左子树的最右节点)
				// 或者选取待删除结点作为根结点的右子树的最小结点(右子树的最左节点)
				//2.假设选取右子树的最左结点作为替换结点,查找替换结点RightMin,将RightMin结点处的值直接赋予cur;
				//3.删除RightMin节点
				//注:替换结点不一定为叶子结点,但必为单孩子结点(若右子树最左结点为替换结点,则替换结点必定没有左孩子)
				else
				{
					//寻找RightMin结点即右子树的最左结点
					Node* RightMinParent = cur;
					Node* RightMin = cur->_right;
					while (RightMin->_left != nullptr)
					{
						RightMinParent = RightMin;
						RightMin = RightMin->_left;
					}
					//找到RightMin结点,替换删除
					cur->_key = RightMin->_key;
					//链接
					if (RightMinParent->_left==RightMin)//RightMin->_left==nullptr
					{
						RightMinParent->_left = RightMin->_right;
					}
					if (RightMinParent->_right == RightMin)
					{
						RightMinParent->_right = RightMin->_right;
					}
					delete RightMin;
					return true;
				}
			}
		}
		return false;
	}
	//搜索二叉树不允许修改结点的值,修改会破坏搜索二叉树的逻辑结构


	//析构函数--结点在堆区申请空间(需要手动释放空间)
	~BinarySearchTree()
	{
		Destroy(_root);
	}
	//拷贝构造函数(结点指针为自定义类型,涉及浅拷贝,导致同一块空间被析构两次,需要实现深拷贝与深赋值)
	//BinarySearchTree<int> t1(t)
	//思路:
	//1. 创建一个新二叉搜索树，作为拷贝的目标树;
	//2. 从始树的根节点开始，递归地复将其插入到目标树中;
	//3.对于每个节点，复制其值，并递归地复制其左子树和右子树;
	//4.返回一个拷贝后的二叉搜索树的根节点;
	BinarySearchTree(const BinarySearchTree<K>& t)
	{
		_root = copy(t._root);
	}
	Node* copy(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}
		Node* NewRoot = new Node(root->_key);
		NewRoot->_left = copy(root->_left);
		NewRoot->_right = copy(root->_right);
		return NewRoot;
	}
	//赋值运算符重载
    //BinarySearchTree<int> t2 = t1;
	BinarySearchTree<K>& operator=(const BinarySearchTree<K>& t)
	{
		swap(_root, t._root);
		return *this;
	}
	//构造函数
	BinarySearchTree() = default;//强制生成默认构造
private:
	//搜索二叉树的中序遍历(左子树-->根-->右子树)
	//外部需要将_root传递给root,但是外部无法访问_root
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_key << " ";
		_InOrder(root->_right);
	}
	//首先销毁左子树,其次销毁右子树,最后销毁根结点;
	void Destroy(Node* root)
	{
		if (root == nullptr)
			return;
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	Node* _root=nullptr;
};


















