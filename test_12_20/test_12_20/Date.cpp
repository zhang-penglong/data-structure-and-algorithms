# define  _CRT_SECURE_NO_WARNINGS 1
# include "Date.h"

void Date::Print()
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

int Date::GetMonthDay(int year, int month)
{
	assert(year >= 1 && (month <= 12 && month >= 1));
	int MonthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		return 29;
	}
	return MonthArray[month];
}

//构造函数
//默认构造函数以缺省参数的形式给出，对于缺省参数，声明与定义不可同时给出;
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
	//日期的合法检查
	if (_year <1 || month <1 || month>12 || day>GetMonthDay(year, month) || day < 1)
	{
		//assert(整型表达式) 表达式为真,不做任何处理,表达式为假,直接报错;
		assert(false);
	}
}
//析构函数
Date::~Date()
{
	_year = 0;
	_month = 0;
	_day = 0;
}
//拷贝构造函数
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

//d1=d2
Date& Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	return *this;
}

//d1==d2
bool Date::operator==(const Date& d)
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}
//d1!=d2
bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}
//d1>d2
bool Date::operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year&&_month > d._month)
	{
		return true;
	}
	else if (_year == d._year&&_month == d._month&&_day > d._day)
	{
		return true;
	}
	return false;
}
//d1>=d2
bool Date::operator>=(const Date& d)
{
	return (((*this) == d) || ((*this) > d));
}
//d1<d2  注: 小于的对立面为大于等于;
bool Date::operator<(const Date& d)
{
	return !((*this) >= d);
}
//d1<=d2 注: 小于等于的对立面为大于;
bool Date::operator<=(const Date& d)
{
	return !((*this) > d);
}
//d1+=day
Date& Date::operator+=(int day)
{
	if (day < 0)
	{
		return *this -= (-day);
	}
	_day += day;
	while (_day>GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return (*this);
}
//d1+day
Date Date::operator+(int day)
{
	Date tmp(*this);
	tmp += day;
	return tmp;
}
//d1-=day
Date& Date::operator-=(int day)
{

	if (day < 0)
	{
		return *this += (-day);
	}
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}
	return (*this);
}
//d1-day
Date Date::operator-(int day)
{
	Date tmp(*this);
	tmp -= day;
	return tmp;
}
//++d1 前置++无参,后置++带int
Date& Date::operator++()
{
	*this = *this + 1;
	return *this;
}
//d1++
Date Date::operator++(int x)
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}
//--d1 前置--无参,后置--带int
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}
//d1--
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}
int Date::operator-(const Date&d)
{
	//假设左大右小
	int flag = 1;
	Date max = *this;
	Date min = d;

	//假设错了,左小右大
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int count = 0;//计数
	while (min != max)
	{
		++min;
		++count;
	}
	return count * flag;
}
