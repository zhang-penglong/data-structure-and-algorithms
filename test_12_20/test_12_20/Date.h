# include <iostream>
# include <assert.h>
using namespace std;
class Date
{
private:
	int _year;
	int _month;
	int _day;
public:
	//函数声明
	//构造函数
	Date(int year = 1, int month = 1, int day = 1);
	//析构函数
	~Date();
	//拷贝构造函数
	Date(const Date& d);

	//d1=d2
	Date& operator=(const Date& d);
	//d1==d2
	bool operator==(const Date& d);
	//d1!=d2
	bool operator!=(const Date& d);
	//d1>d2
	bool operator>(const Date& d);
	//d1>=d2
	bool operator>=(const Date& d);
	//d1<d2
	bool operator<(const Date& d);
	//d1<=d2
	bool operator<=(const Date& d);

	//d+=day
	Date& operator+=(int day);
	//d+day
	Date operator+(int day);
	//d-=day
	Date& operator-=(int day);
	//d-day
	Date operator-(int day);
	//++d1
	Date& operator++();
	//d1++
	Date operator++(int x);
    //--d1
	Date& operator--();
	//d1--
	Date operator--(int);
	//d1-d2
	int operator-(const Date&d);

	int GetMonthDay(int year, int month);
	void Print();
};