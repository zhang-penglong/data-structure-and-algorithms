# define  _CRT_SECURE_NO_WARNINGS 1
#include "BloomFilter.h"
int main()
{
	string strs[] = { "百度", "字节", "腾讯" };
	bloomfilter<10> bf;
	for (auto& s : strs)
	{
		bf.set(s);
	}

	for (auto& s : strs)
	{
		cout << bf.test(s) << endl;
	}

	for (auto& s : strs)
	{
		cout << bf.test(s + 'a') << endl;
	}
	cout << bf.test("摆渡") << endl;
	cout << bf.test("百渡") << endl;
	return 0;
}