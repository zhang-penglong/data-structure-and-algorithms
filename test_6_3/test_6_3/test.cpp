# define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <set>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

//int main()
//{
//	set<int> s1;//无参构造
//	//pair<set<int>::iterator,bool> pair=s1.insert(6);
//	//cout << *(pair.first) << ":" << pair.second << endl;
//	s1.insert(6);
//	s1.insert(5);
//	s1.insert(7);
//	s1.insert(20);
//	s1.insert(10);
//	s1.insert(3);
//	s1.insert(20);
//	s1.insert(3);
//	s1.insert(4);
//
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	//set<T>::iterator find(const T& value)const
//	set<int>::iterator pos = s1.find(7);
//	s1.insert(pos, 2);
//	set<int>::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1<< " ";
//		++it1;
//	}
//	cout << endl;
//
//
//	//迭代器区间构造
//	set<int> s2(s1.begin(),pos);
//	set<int>::iterator it2 = s2.begin();
//	while (it2 != s2.end())
//	{
//		cout << *it2 << " ";
//		++it2;
//	}
//	cout << endl;
//
//	return 0;
//}


//int main()
//{
//	set<int> s1;
//	s1.insert(10);
//	s1.insert(5);
//	s1.insert(15);
//	s1.insert(8);
//	s1.insert(2);
//	s1.insert(20);
//
//	set<int>::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	set<int>::iterator pos = s1.find(8);
//	s1.erase(pos);
//	size_t retval=s1.erase(30);
//	set<int>::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;
//	cout << retval << endl;
//	return 0;
//}


//int main()
//{
//	map<string, string> dict;
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("sistence", "坚持"));
//	dict.insert(make_pair("integrity", "正直"));
//	dict.insert(make_pair("endeavor", "努力"));
//	dict.insert(make_pair("leran", "学习"));
//	dict.insert(make_pair("learn", "推断"));
//
//	map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		//cout << (*it).first << ":" << (*it).second << endl;
//		cout << it->first << ":" << it->second << endl;
//		it++;
//	}
//	cout << endl;
//
//	/*map<string,string>::iterator pos = dict.find("learn");
//	dict.erase(pos);
//	map<string,string>::iterator it1 = dict.begin();
//	while (it1 != dict.end())
//	{
//		cout << (*it1).first << ":" << (*it1).second << endl;
//		it1++;
//	}*/
//
//	return 0;
//}


//int main()
//{
//	map<string, string> dict;
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("learn", "推断"));
//	dict.insert(make_pair("endeavor", "努力"));
//
//	map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		cout << (*it).first << ":" << (*it).second << endl;
//		++it;
//	}
//	cout << endl;
//
//	map<string, string>::iterator pos = dict.find("learn");
//	dict.erase(pos);
//	dict.erase("courage");
//	dict.erase("level");
//
//	for (auto& kv : dict)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	
//	return 0;
//}


//int main()
//{
//	map<string, string> dict;
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("learn", "推断"));
//	dict.insert(make_pair("endeavor", "努力"));
//
//	map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		cout << (*it).first << ":" << (*it).second << endl;
//		++it;
//	}
//	cout << endl;
//	string value = dict["brave"];
//	cout << "value:" << value << endl;
//	return 0;
//}


//int main()
//{
//	map<string, int> mapcount;
//	mapcount.insert(make_pair("banana", 10));
//	mapcount.insert(make_pair("orange", 5));
//	mapcount.insert(make_pair("apple", 25));
//	mapcount.insert(make_pair("grape", 5));
//	for (auto& kv : mapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	//[]功能一:查找
//	int retval = mapcount["apple"];
//	cout << "retval:" << retval << endl;
//
//	//[]功能二:插入
//	int ret=mapcount["peach"];
//	cout << "ret:" << ret << endl;
//	//[]功能三:修改
//	mapcount["peach"] = 3;
//	for (auto& kv : mapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//	
//	//[]功能四:插入+修改
//	mapcount["pear"] = 8;
//	for (auto& kv : mapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	//--->
//	// value& operator[](const key& key)
//	// {
//	//   pair<iterator,bool> ret=insert(make_pair(key,value()));
//	//   return *(ret.first).second;
//	// }
//
//}


//int main()
//{
//	unordered_map<string, string> dict;
//	dict.insert(make_pair("courage", "勇气"));
//	dict.insert(make_pair("insistence", "坚持"));
//	dict.insert(make_pair("endeavor", "努力"));
//	dict.insert(make_pair("learn", "学习"));
//	dict.insert(make_pair("learn", "推断"));
//
//	unordered_map<string, string>::iterator it = dict.begin();
//	while (it != dict.end())
//	{
//		cout << it->first << ":" << it->second << endl;
//		++it;
//	}
//	cout << endl;
//
//	unordered_map<string, string>::iterator pos = dict.find("learn");
//	//cout << pos->first << ":" << (*pos).second << endl;
//	if (pos != dict.end())
//	{
//		dict.erase(pos);
//	}
//
//	dict.erase("courage");
//
//	for (auto& kv : dict)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	dict["insistence"] = "持续";
//
//	for (auto& kv : dict)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//}


//int main()
//{
//	unordered_map<string, int> umapcount;
//	umapcount.insert(make_pair("apple", 20));
//	umapcount.insert(make_pair("pear", 10));
//	umapcount.insert(make_pair("grape", 8));
//	umapcount.insert(make_pair("banana", 9));
//	for (auto& kv : umapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	int ret = umapcount["pear"];
//	cout << "ret:" << ret << endl << endl;
//
//	umapcount["pear"] = 100;
//	for (auto& kv : umapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	umapcount["cherry"] = 48;
//	for (auto& kv : umapcount)
//	{
//		cout << kv.first << ":" << kv.second << endl;
//	}
//	cout << endl;
//
//	return 0;
//}

int main()
{
	unordered_set<int> uset;
	uset.insert(10);
	uset.insert(20);
	uset.insert(6);
	uset.insert(9);
	uset.insert(15);

	unordered_set<int>::iterator it = uset.begin();
	while (it != uset.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	auto pos = uset.find(10);
	if (pos != uset.end())
	{
		uset.erase(pos);
	}
	for (auto& us : uset)
	{
		cout << us << " ";
	}
	cout << endl;

	uset.erase(20);
	for (auto& us : uset)
	{
		cout << us << " ";
	}
	cout << endl;

	return 0;
}