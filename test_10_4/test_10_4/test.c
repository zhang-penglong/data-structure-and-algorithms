# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <assert.h>
# include <stdlib.h>

typedef int SLTDatatype;

typedef struct SLTNode
{
	SLTDatatype data;
	struct SLTNode* next;
}SLTNode;
//显示链表
void SLTPrint(SLTNode* phead)
{

	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
//创建新结点
SLTNode* BuySLTNode(SLTDatatype x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	if (newnode == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}

//尾插
void SLTpushback(SLTNode** pphead,SLTDatatype x)
{
	assert(pphead);
	SLTNode* newnode = BuySLTNode(x);
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}
//销毁单链表
void DestorySLT(SLTNode** pphead)
{
	SLTNode* cur = *pphead;
	while (cur != NULL)
	{
		SLTNode* del = cur;
		cur = cur->next;
		free(del);
	}
	*pphead = NULL;
}
int main()
{
	SLTNode* plist = NULL;
	SLTpushback(&plist, 1);
	SLTpushback(&plist, 2);
	SLTpushback(&plist, 3);
	SLTpushback(&plist, 4);
	SLTpushback(&plist, 5);
	SLTPrint(plist);

	DestorySLT(&plist);
	SLTPrint(plist);

	return 0;
}



//利用头插反转单链表
//修改结点的指针域实现头插时，会丢失下个节点;为记录下个结点，创建curNext保存下一个节点的地址；
//struct ListNode 
//{
//	int val;
//	struct ListNode *next;
//};
//struct ListNode* reverseList(struct ListNode* head)
//{
//	struct ListNode* cur = head;
//	struct ListNode* newhead = NULL;
//	while (cur != NULL)
//	{
//		struct ListNode* curNext = cur->next;
//		//头插
//		cur->next = newhead;
//		newhead = cur;
//		cur = curNext;
//	}
//	return newhead;
//}


//创建一个带哨兵位的头结点，然后取小的进行尾插
struct ListNode
{
	int val;
	struct ListNode *next;
};
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2)
{
	struct ListNode* head = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* tail = head;
	if (list1 == NULL)
	{
		return list2;
	}
	if (list2 == NULL)
	{
		return list1;
	}
	while ((list1 != NULL) && (list2 != NULL))
	{
		if ((list1->val)<(list2->val))
		{
			//list1链表进行尾插
			tail->next = list1;
			tail = tail->next;
			list1 = list1->next;
		}
		else
		{
			//list2链表进行尾插
			tail->next = list2;
			tail = tail->next;
			list2 = list2->next;
		}
	}
	if (list1 != NULL)
	{
		tail->next = list1;
	}
	if (list2 != NULL)
	{
		tail->next = list2;
	}
	struct ListNode* del = head;
	head = head->next;
	free(del);

	return head;
}