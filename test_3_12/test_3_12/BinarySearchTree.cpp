# define  _CRT_SECURE_NO_WARNINGS 1

//KV模型(Key-Value模型)

#include <string>
#include <iostream>
using namespace std;

//结点类的实现
template <class K,class Value>
struct BinarySearchTreeNode
{
	typedef BinarySearchTreeNode Node;
	Node* _left;//指向左子树
	Node* _right;//指向右子树
	K _key;//存储当前结点数据
	Value _value;

	//开辟新结点需要构造函数
	BinarySearchTreeNode(const K& key,const Value& val)
		:_left(nullptr)
		,_right(nullptr)
		,_key(key)
		,_value(val)
	{}
};

template <class K,class Value>
class BinarySearchTree
{
	typedef BinarySearchTreeNode<K,Value> Node;
public:
	//Find()函数返回值需要修改，找到返回结点指针，找不到返回nullptr;
	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur != nullptr)
		{
			//key大于根结点处的键值,则去右子树查找
			if (cur->_key < key)
			{
				cur = cur->_right;
			}
			//若key小于根结点处的键值,则去左子树查找
			else if (cur->_key > key)
			{
				cur = cur->_left;
			}
			//查找过程中某个结点数据与key相等,停止查找,返回true;
			else
			{
				return cur;
			}
		}
		//cur走空,找不到返回false
		return nullptr;
	}
	//Insert()函数插入时需要增加value值
	bool Insert(const K& key,const Value& val)
	{
		// 空树
		if (_root == nullptr)
		{
			_root = new Node(key,val);
			return true;
		}
		//非空树
		//parent记录父节点位置
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key>key)
			{
				parent = cur;
				cur = cur->_left;
			}
			//为维护搜索二叉树逻辑结构,同一颗树不能出现相等的数值
			else
			{
				return false;
			}
		}
		//cur已经走空,开辟节点,存储数据,建立链接关系
		cur = new Node(key,val);
		//存储值为key的结点链接到父节点的何处？
		if (parent->_key > key)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		return true;
	}
	//搜索二叉树的遍历
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool Erase(const K& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_key>key)
			{
				parent = cur;
				cur = cur->_left;
			}
			//查找到待删除元素所在的结点
			else
			{
				//待删除结点左孩子不存在,右孩子存在(注:叶子结点按照此处逻辑进行删除)
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_right;
					}
					else
					{
						if (parent->_key < cur->_key)
						{
							parent->_right = cur->_right;
						}
						else
						{
							parent->_left = cur->_right;
						}
					}
					delete cur;
					return true;
				}

				//待删除结点左孩子存在,右孩子不存在
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_left;
					}
					else
					{
						if (parent->_key < cur->_key)
						{
							parent->_right = cur->_left;
						}
						else
						{
							parent->_left = cur->_right;
						}
					}
					delete cur;
					return true;
				}
				else
				{
					//寻找RightMin结点即右子树的最左结点
					Node* RightMinParent = cur;
					Node* RightMin = cur->_right;
					while (RightMin->_left != nullptr)
					{
						RightMinParent = RightMin;
						RightMin = RightMin->_left;
					}
					//找到RightMin结点,替换删除
					cur->_key = RightMin->_key;
					//链接
					if (RightMinParent->_left == RightMin)//RightMin->_left==nullptr
					{
						RightMinParent->_left = RightMin->_right;
					}
					if (RightMinParent->_right == RightMin)
					{
						RightMinParent->_right = RightMin->_right;
					}
					delete RightMin;
					return true;
				}
			}
		}
		return false;
	}
	
	~BinarySearchTree()
	{
		Destroy(_root);
	}
	BinarySearchTree(const BinarySearchTree<K,Value>& t)
	{
		_root = copy(t._root);
	}
	Node* copy(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}
		Node* NewRoot = new Node(root->_key,root->_value);
		NewRoot->_left = copy(root->_left);
		NewRoot->_right = copy(root->_right);
		return NewRoot;
	}
	BinarySearchTree<K,Value>& operator=(const BinarySearchTree<K,Value>& t)
	{
		swap(_root, t._root);
		return *this;
	}
	BinarySearchTree() = default;
private:
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_InOrder(root->_left);
		cout << root->_key << " ";
		_InOrder(root->_right);
	}
	void Destroy(Node* root)
	{
		if (root == nullptr)
			return;
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	Node* _root = nullptr;
};

int main()
{
	BinarySearchTree<string, string> dict;
	dict.Insert("sort", "排序");
	dict.Insert("left", "左边");
	dict.Insert("right", "右边");
	dict.Insert("string", "字符串");
	dict.Insert("bastard", "混蛋");
	dict.Insert("王正", "蹲坑王");
	dict.Insert("沈文定", "虚逼");
	string str;
	while (cin >> str)
	{
		auto ret = dict.Find(str);
		if (ret)
		{
			cout << ret->_value << endl;
		}
		else
		{
			cout << "无此单词" << endl;
		}
	}

	return 0;
}






























