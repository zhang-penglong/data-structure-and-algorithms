# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <string>
#include <initializer_list>
using namespace std;
//void test1()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	vector<int> v;
//	for (auto e : arr)
//	{
//		v.push_back(e);
//	}
//	vector<int>::iterator it = v.begin();
//	while (it != v.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	vector<int> v1{ 1, 2, 3, 4, 5, 6, 7, 8 };
//	for (auto e : v1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//void test2()
//{
//
//	
//}
//struct person
//{
//	const char* name;
//	int age;
//};
//int main()
//{
//	int array1[]{ 1, 2, 3, 4, 5 };
//	//整型数组未指定初始化内容,按照默认值0初始化数组未指定的内容
//	int array2[3]{ 1 };
//	person p{ "Linda", 23 };
//	return 0;
//}

//单参数隐式类型转换
//class Date
//{
//public:
//	//单参数(缺省)的构造函数
//	/*Date(int year=2000)
//		:_year(year)
//	{
//		cout << "调用缺省单参数构造函数" << endl;
//	}*/
//
//	//单参数(无参)的构造函数
//	Date(int year)
//		:_year(year)
//	{
//		cout << "调用单参数(无参)构造函数" << endl;
//	}
//private:
//	int _year;
//};
//int main()
//{
//	Date d1(2000);
//	//d1-->自定义类型
//	//10-->整型
//	d1 = 10;//类型转换 
//	return 0;
//}
//结论:单参数构造函数(1.缺省  2.无参)皆支持隐式类型转换

//多参数隐式类型转换
//class Date
//{
//public:
//	//全缺省的默认构造函数
//	//Date(int year, int month, int day)//(不支持隐式类型转换)
//	//Date(int year, int month, int day=1)//(不支持隐式类型转换)
//	//Date(int year, int month=1, int day=1)//(支持隐式类型转换)
//	Date(int year=1, int month = 1, int day = 1)//(支持隐式类型转换)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "调用构造函数" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022);//2022 1 1
//	//2023-->int类型 d1-->Date类型
//	d1 = 2023;//2023 1 1
//	return 0;
//}
////结论: 1.多参数第一个参数无默认值，其余参数皆存在默认值支持隐式类型转换
////结论: 2.多参数所有参数存在默认值支持隐式类型转换

//class Date
//{
//public:
//	//Date(int year, int month, int day)//(不支持隐式类型转换)
//	//Date(int year, int month, int day = 1)//(不支持隐式类型转换)
//	//Date(int year, int month=1, int day=1)//(支持隐式类型转换)
//	Date(int year=2000, int month=1, int day = 1)//(支持隐式类型转换)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "调用构造函数" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	//单参数
//	string s1("abcdef");//直接调用构造函数
//	string s2 = "abcdef";//隐式类型转换(单参数构造函数支持隐式类型转换) 
//	//首先用常量字符串构造一个const string类型的匿名对象，然后匿名对象拷贝构造s2;
//	//最后编译器优化为直接构造
//
//	//多参数
//	//构造+拷贝构造-->编译器优化为直接构造
//	Date d3 = { 2024,5,1};//本质:隐式类型转换(多参数构造函数支持隐式类型转换)
//	return 0;
//}

////int main()
////{
////	Date d1(2024, 3, 1);
////	Date d2(2024, 4, 1);
////	//自定义类型数组未指定初始化内容，按照自定义类型的默认构造函数初始化数组未指定的内容;
////	Date* darr1 = new Date[3]{ d1, d2 };// c++ 98
////	Date* darr2 = new Date[3]{{ 2024, 3, 1 }, { 2024, 4, 1 }, { 2024, 5, 1 } }; //c++ 11(推荐使用)
////	return 0;
////}
//

//int main()
//{
//	vector<int> v = { 1, 2, 3, 4, 5 };
//
//	return 0;
//}
//int main()
//{
//	auto il = { 1, 2, 3, 4, 5, 6, 7, 8 };
//	cout << "size=" << il.size() << endl;
//	initializer_list<int>::iterator lit = il.begin();
//	while (lit != il.end())
//	{
//		cout << *lit << " ";
//		++lit;
//	}
//	cout << endl;
//	return 0;
//}


//namespace zbw 
//{
//	template <class T>
//	class vector 
//	{
//		typedef T* iterator;
//		typedef const T* const_iterator;
//	public:
//		//如何进行范围形式的初始化
//		vector(initializer_list<T>& il) 
//		{
//			_start = new T[il.size()];
//			_finish = _start + il.size();
//			_endofstorage = _start + il.size();
//			iterator vit = _start;
//			//利用迭代器进行赋值
//			typename initializer_list<T>::iterator lit = il.begin();
//			while (lit != il.end())
//			{
//				*vit++ = *lit++;
//			}
//		}
//		//针对这个 operator = 赋值运算符的重载 还是复用上述的构造函数
//		vector<T>& operator=(initializer_list<T> il) 
//		{
//			vector<T> tmp(il);
//			std::swap(_start, tmp._start);
//			std::swap(_finish, tmp._finish);
//			std::swap(_endofstorage, tmp._endofstorage);
//			return *this;
//		}
//	private:
//		iterator _start;
//		iterator _finish;
//		iterator _endofstorage;
//	};
//}
//int main() 
//{
//	zbw::vector<int> v = initializer_list<int>{ 1, 2, 3, 4, 5 };
//	return 0;
//}