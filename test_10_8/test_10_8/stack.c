# define  _CRT_SECURE_NO_WARNINGS 1


# include "stack.h"

//初始化栈
void InitStack(Stack* ps)
{
	assert(ps != NULL);
	//开辟一定量的空间
	ps->nums = (STDataType*)malloc(4*sizeof(STDataType));
	//判断空间开辟是否成功
	if (ps->nums == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	ps->top = -1;
	ps->capacity = 4;
}

//栈的销毁
void DestroyStack(Stack* ps)
{
	assert(ps != NULL);

	free(ps->nums);
	ps->nums = NULL;

	ps->top = -1;
	ps->capacity = 0;
}

//压栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps != NULL);

	//栈满扩容
	if (ps->top == ps->capacity - 1)
	{
		STDataType* tmp = (STDataType*)realloc(ps->nums, sizeof(STDataType)* 2 * (ps->capacity));
		if (tmp == NULL)
		{
			perror("realloc failed:");
			exit(-1);
		}
		ps->nums = tmp;
		ps->capacity = 2 * (ps->capacity);
	}
	//插入数据
	ps->nums[ps->top + 1]=x;
	ps->top++;
}

//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);

	//空栈
	assert(ps->top > (-1));

	return ps->nums[ps->top];
}

//出栈
void StackPop(Stack* ps)
{
	assert(ps);
	//空栈时不可进行删除操作
	assert(ps->top > -1);

	ps->top = ps->top - 1;
}

//判空
bool StackEmpty(Stack* ps)
{
	assert(ps);

	return (ps->top == -1);
}

//获取栈中有效数据的个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top + 1;
}