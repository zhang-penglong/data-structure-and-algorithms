# define  _CRT_SECURE_NO_WARNINGS 1

# include "stack.h"

int main()
{
	Stack st;
	InitStack(&st);
	StackPush(&st, 10);
	StackPush(&st, 20);
	StackPush(&st, 30);
	StackPush(&st, 40);
	StackPush(&st, 50);
	while (!StackEmpty(&st))
	{
		printf("%d->", StackTop(&st));
		StackPop(&st);
	}
	printf("\n");
	DestroyStack(&st);
	return 0;
}