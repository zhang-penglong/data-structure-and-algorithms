# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>

typedef int STDataType;
typedef struct Stack
{
	STDataType* nums;
	int top;//记录栈顶元素在顺序栈中的位置;
	int capacity;//记录栈的大小;
}Stack;

//初始化栈
void InitStack(Stack* ps);

//栈的销毁
void DestroyStack(Stack* ps);

//压栈
void StackPush(Stack* ps, STDataType x);

//获取栈顶元素
STDataType StackTop(Stack* ps);

//出栈
void StackPop(Stack* ps);

//判空
bool StackEmpty(Stack* ps);

int StackSize(Stack* ps);










