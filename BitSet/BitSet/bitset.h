#include <iostream>
#include <vector>
#include <assert.h>
using namespace std;


template<size_t N>
class BitSet
{
public:
	BitSet()
	{
		_bits.resize(N / 32 + 1, 0);
	}
	//将x映射的比特位设置为1
	void set(size_t x)
	{
		//计算x在第i个整型
		size_t i = x / 32;
		//计算x在第j个比特位
		size_t j = x % 32;

		_bits[i] = _bits[i] | (1 << j);
	}
	//将x映射的比特位设置为0
	void reset(size_t x)
	{
		size_t i = x / 32;
		size_t j = x % 32;

		_bits[i] = _bits[i] & ~(1 << j);
	}
	//检测数值x是否存在
	bool test(size_t x)
	{
		size_t i = x / 32;
		size_t j = x % 32;

		return _bits[i] & (1 << j);
	}
private:
	vector<int> _bits;
};


template<size_t N>
class two_bit_set
{
public:
	void set(size_t x)
	{
		// 原先为00,数据x出现后变为01
		if (_bs1.test(x) == false
			&& _bs2.test(x) == false)
		{
			_bs2.set(x);
		}
		else if (_bs1.test(x) == false
			&& _bs2.test(x) == true)
		{
			//原先为01,数据x出现后变为10
			_bs1.set(x);
			_bs2.reset(x);
		}
		//一次及以上不做处理
	}

	//数值x出现0次返回0，出现1次返回1，出现1次及以上返回2
	int test(size_t x)
	{
		if (_bs1.test(x) == false
			&& _bs2.test(x) == false)
		{
			return 0;
		}
		else if (_bs1.test(x) == false
			&& _bs2.test(x) == true)
		{
			return 1;
		}
		else
		{
			return 2; // 2次及以上
		}
	}
private:
	BitSet<N> _bs1;
	BitSet<N> _bs2;
};




















