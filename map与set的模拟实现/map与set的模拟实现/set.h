#pragma once
#include "RBTree.h"
namespace bit
{
	template<class K>
	class set
	{
		//仿函数
		struct SetKeyOfT//获取set容器中数据的键值key
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename RBTree<K, K, SetKeyOfT>::iterator iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		pair<iterator, bool> insert(const K& k)
		{
			return _t.insert(k);
		}

		iterator Find(const K& key)
		{
			return _t.Find();
		}

	private:
		RBTree<K, K, SetKeyOfT> _t;
	};



	void test_set1()
	{
		set<int> s;
		int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
		for (auto e : a)
		{
			s.insert(e);
		}

		set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;

	}
}