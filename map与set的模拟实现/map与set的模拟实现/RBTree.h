#pragma once
#include<assert.h>
#include <iostream>
using namespace std;


enum Color
{
	RED,
	BLACK
};

template<class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;//指向左孩子
	RBTreeNode<T>* _right;//指向右孩子
	RBTreeNode<T>* _parent;//指向父节点
	Color _col;//结点颜色
	T _data;//存储有效数据

	RBTreeNode(const T& data)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_col(RED)
		,_data(data)
	{}
};

template<class T,class Ptr,class Ref>
struct RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T, Ptr, Ref> Self;
	Node* _node;

	//构造函数
	RBTreeIterator(Node* node)
		:_node(node)
	{}

	Ptr operator->()
	{
		return &_node->_data;
	}

	 Ref operator*()
	{
		return _node->_data;
	}

	//++it
	Self& operator++()
	{
		//it指向当前结点,若右子树不为空,下一个访问的是右子树的最左结点
		if (_node->_right != nullptr)
		{
			Node* SubLeft = _node->_right;
			while (SubLeft->_left!= nullptr)
			{
				SubLeft = SubLeft->_left;
			}
			_node = SubLeft;
		}
		//it指向当前结点,若右子树为空,寻找祖先结点中某个结点是其父亲的左子树的结点
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent&&parent->_right == cur)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;
		}

		return *this;
	}
	//it++
	Self operator++(int)
	{
		//记录++之前的it位置
		Self tmp = Self(_node);
		//it指向当前结点,若右子树不为空,下一个访问的是右子树的最左结点
		if (_node->_right != nullptr)
		{
			Node* SubLeft = _node->_right;
			while (SubLeft->_left != nullptr)
			{
				SubLeft = SubLeft->_left;
			}
			_node = SubLeft;
		}
		//it指向当前结点,若右子树为空,寻找祖先结点中某个结点是其父亲的左子树的结点
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent&&parent->_right == cur)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;
		}
		return tmp;
	}

	Self& operator--()
	{
		if (_node == nullptr)
		{
			//特殊处理(未找到解决方案,具体参见带有哨兵位的红黑树结构)
		}
		//it指向当前结点,若左子树不为空,下一个访问的是左子树的最右结点
		if (_node->_left != nullptr)
		{
			Node* SubRight = _node->_left;
			while (SubRight->_left != nullptr)
			{
				SubRight = SubRight->_right;
			}
			_node = SubRight;
		}
		//it指向当前结点,若左子树为空,寻找祖先结点中某个结点是其父亲的右子树的结点
		else
		{
			Node* parent = _node->_parent;
			Node* cur = _node;
			while (parent&&parent->_left == cur)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)
	{
		return _node == s._node;
	}
};
// KeyOfT仿函数 取出T对象中的key
template <class K, class T,class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef RBTreeIterator<T, T*, T&> iterator;
	typedef RBTreeIterator<T, const T*, const T&> const_iterator;
	//左子树的最左结点(中序遍历的第一个)
	iterator begin()
	{
		Node* SubLeft = _root;
		while (SubLeft && SubLeft->_left)
		{
			SubLeft = SubLeft->_left;
		}
		return iterator(SubLeft);
	}

	//空结点
	iterator end()
	{
		return iterator(nullptr);
	}
	pair<iterator,bool> insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root), true);
		}
		Node* parent = nullptr;
		Node* cur = _root;
		KeyOfT kot;
		while (cur != nullptr)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(cur->_data)>kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}
		//开辟结点,存储数据，建立链接关系
		cur = new Node(data);
		Node* newnode = cur;
		if (kot(parent->_data) > kot(data))
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		cur->_parent = parent;

		//平衡化操作
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			if (parent == grandfather->_left)
			{
				Node* uncle = grandfather->_right;
				// 情况一：叔叔存在且为红
				if (uncle && uncle->_col == RED)
				{
					// 变色
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}
			}
			//parent==grandfather->_right
			else
			{
				Node* uncle = grandfather->_left;
				// 情况一：叔叔存在且为红
				if (uncle && uncle->_col == RED)
				{
					// 变色
					parent->_col = uncle->_col = BLACK;
					grandfather->_col = RED;

					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_right)
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}

					break;
				}
			}
		}
		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* ppnode = parent->_parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subR;
			}
			else
			{
				ppnode->_right = subR;
			}
			subR->_parent = ppnode;
		}
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;

		Node* ppnode = parent->_parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subL;
			}
			else
			{
				ppnode->_right = subL;
			}
			subL->_parent = ppnode;
		}
	}
	//红黑树的检测
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << endl;
		_InOrder(root->_right);
	}

	void InOrder()
	{
		_InOrder(_root);
	}
	bool Check(Node* cur)
	{
		if (cur == nullptr)
			return true;

		//检查红黑树性质二
		if (cur->_col == RED && cur->_parent->_col == RED)
		{
			cout << cur->_kv.first << "存在连续的红色节点" << endl;
			return false;
		}

		return Check(cur->_left) && Check(cur->_right);
	}

	iterator* Find(const T& data)
	{
		KeyOfT kot;//定义仿函数对象
		Node* cur = _root;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				cur = cur->_right;
			}
			else if (kot(cur->_data)  > kot(data))
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return nullptr;
	}

	bool Check(Node* cur, int blackNum, int refBlackNum)
	{
		if (cur == nullptr)
		{
			if (refBlackNum != blackNum)
			{
				cout << "黑色节点的数量不相等" << endl;
				return false;
			}

			return true;
		}

		if (cur->_col == RED && cur->_parent->_col == RED)
		{
			cout << cur->_kv.first << "存在连续的红色节点" << endl;
			return false;
		}

		if (cur->_col == BLACK)
			++blackNum;

		return Check(cur->_left, blackNum, refBlackNum)
			&& Check(cur->_right, blackNum, refBlackNum);
	}
	bool IsRBTree()
	{
		//检查根结点是否为黑色
		if (_root && _root->_col == RED)
			return false;

		int refBlackNum = 0;//基准值
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				refBlackNum++;

			cur = cur->_left;
		}

		return Check(_root, 0, refBlackNum);
	}
private:
	Node* _root = nullptr;
};



