# define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
using namespace std;

//int main()
//{
//	list<int> lt;
//	//尾插元素
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//	list<int>::iterator it = lt.begin();
//	while (it != lt.end())
//	{
//	    cout << *it << " ";
//		++it;
//	}
//    cout << endl;
//	//头插结点,结点中数据值为10
//	lt.push_front(10);
//	cout << lt.front() << endl;
//	//头删首节点
//	lt.pop_front();
//	cout << lt.front() << endl;
//    //尾插结点,结点中数据值为20
//	lt.push_back(20);
//	cout << lt.back() << endl;
//	//尾删尾结点
//	lt.pop_back();
//	cout << lt.back() << endl;
//	return 0;
//}

////list容器中的position位置插入值为val的结点,返回刚刚插入到list中的元素的迭代器
//iterator insert(iterator position, const T& val);
//
////从该list容器中的position位置开始,插入n个值为val的结点
//void insert(iterator position, size_t n, const T& val);
//
////将迭代器区间[first,last)中的值以结点的形式插入到pos位置
//template <class InputIterator>
//void insert(iterator position, InputIterator first, InputIterator last);

//int main()
//{
//	list<int> lt;
//	lt.push_back(1);
//	lt.push_back(2);
//	lt.push_back(3);
//	lt.push_back(4);
//	lt.push_back(5);
//	list<int>::iterator it = lt.begin();
//	while (it != lt.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	//list容器中的第二个位置插入值为10的结点
//	it = lt.begin();
//	++it;
//	it=lt.insert(it, 10);
//	for (list<int>::iterator it = lt.begin(); it != lt.end(); it++)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//    //返回刚刚插入到list中的元素的迭代器
//	cout << *it << endl;
//	return 0;
//}


//int main()
//{
//	//string s("Hello Linux!");
//	//list<int> lt(s.begin(), s.end());//迭代器区间构造
//
//	////正向遍历
//	//list<int>::iterator it = lt.begin();
//	//while (it != lt.end())
//	//{
//	//	cout << *it << " ";
//	//	++it;
//	//}
//	//cout << endl;
//
//	////反向遍历
//	//list<int>::reverse_iterator rit = lt.rbegin();
//	//while (rit != lt.rend())
//	//{
//	//	cout << *rit << " ";
//	//	++rit;
//	//}
//	//cout << endl;
//
//	return 0;
//}


int main()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	list<int>::iterator it = lt.begin();
	++it;
	it=lt.erase(it);//删除第二个位置的元素
	for (list<int>::iterator it = lt.begin(); it != lt.end(); it++)
	{
		cout << *it << " ";
	}
	cout << endl;

	//返回待删元素的下一个元素的迭代器
	cout << *it << endl;

	return 0;
}