# define  _CRT_SECURE_NO_WARNINGS 1

# include "List.h"
void Test1()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	PrintList(plist);
}
void Test2()
{
	ListNode* plist = InitList();
	ListPushFront(plist, 1);
	ListPushFront(plist, 2);
	ListPushFront(plist, 3);
	ListPushFront(plist, 4);
	ListPushFront(plist, 5);
	PrintList(plist);

}
void Test3()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

}
void Test4()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

}
void Test5()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	ListNode* pos = ListFind(plist, 20);
	printf("%p\n", pos);

}
void Test6()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	PrintList(plist);

	ListNode* pos = ListFind(plist, 20);
	ListInsert(pos, 25);
	PrintList(plist);

}
void Test7()
{
	ListNode* plist = InitList();
	ListPushBack(plist, 10);
	ListPushBack(plist, 20);
	ListPushBack(plist, 30);
	ListPushBack(plist, 40);
	ListPushBack(plist, 50);
	PrintList(plist);

	ListNode* pos = ListFind(plist, 20);
	ListEarse(pos);
	PrintList(plist);
}
void Test8()
{
	ListNode* plist = InitList();
	Listpushback(plist, 10);
	Listpushback(plist, 20);
	Listpushback(plist, 30);
	Listpushback(plist, 40);
	Listpushback(plist, 50);
	PrintList(plist);
}
void Test9()
{
	ListNode* plist = InitList();
	Listpushfront(plist, 1);
	Listpushfront(plist, 2);
	Listpushfront(plist, 3);
	Listpushfront(plist, 4);
	Listpushfront(plist, 5);
	PrintList(plist);

}
void Test10()
{
	ListNode* plist = InitList();
	Listpushback(plist, 10);
	Listpushback(plist, 20);
	Listpushback(plist, 30);
	Listpushback(plist, 40);
	Listpushback(plist, 50);
	PrintList(plist);

	Listpopback(plist);
	PrintList(plist);

	Listpopback(plist);
	PrintList(plist);

	Listpopback(plist);
	PrintList(plist);

	Listpopback(plist);
	PrintList(plist);

	Listpopback(plist);
	PrintList(plist);
}
void Test11()
{
	ListNode* plist = InitList();
	Listpushback(plist, 10);
	Listpushback(plist, 20);
	Listpushback(plist, 30);
	Listpushback(plist, 40);
	Listpushback(plist, 50);
	PrintList(plist);

	Listpopfront(plist);
	PrintList(plist);

	Listpopfront(plist);
	PrintList(plist);

	Listpopfront(plist);
	PrintList(plist);

	Listpopfront(plist);
	PrintList(plist);

	Listpopfront(plist);
	PrintList(plist);

}
int main()
{
	Test11();
	return 0;
}