# define  _CRT_SECURE_NO_WARNINGS 1
# include "List.h"

ListNode* BuyListNode(ListDataType x)
{
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (newnode == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	newnode->prev = NULL;

	return newnode;
}

ListNode* InitList()
{
	ListNode* phead = BuyListNode(0);

	phead->prev = phead;
	phead->next = phead;
	return phead;
}

void PrintList(ListNode* phead)
{
	assert(phead != NULL);
	printf("phead<==>");
	ListNode* cur = phead->next;
	while (cur != phead)
	{
		printf("%d<==>", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

void DestoryList(ListNode* phead)
{
	ListNode* cur = phead;
	//断开循环
	ListNode* tail = cur->prev;
	tail->next = NULL;
	while (cur != NULL)
	{
		ListNode* curnext = cur->next;
		free(cur);
		cur = curnext;
	}
	return;
}

void ListPushBack(ListNode* phead, ListDataType x)
{
	assert(phead != NULL);

	ListNode* tail = phead->prev;
	ListNode* newnode = BuyListNode(x);
    
	//尾插
	newnode->prev = tail;
	tail->next = newnode;

	newnode->next = phead;
	phead->prev = newnode;
}

void ListPushFront(ListNode* phead, ListDataType x)
{
	assert(phead != NULL);
  
	//寻找首结点
	ListNode*  frontnode = phead->next;
	//创建新结点
	ListNode* newnode = BuyListNode(x);

	//头插
	newnode->prev = phead;
	phead->next = newnode;
	//链表为空时，frontnode即为phead;
	newnode->next = frontnode;
	frontnode->prev = newnode;

}

void ListPopBack(ListNode* phead)
{
	assert(phead);
	//空链表不可删除
	assert(phead->next != phead);
	//链表中只有首节点，tailprev即为哨兵位结点;
	ListNode* tail = phead->prev;
	ListNode* tailprev = tail->prev;
	free(tail);
	tailprev->next = phead;
	phead->prev = tailprev;
}

void ListPopFront(ListNode* phead)
{
	assert(phead != NULL);
	assert(phead->next != phead);
	//寻找首节点
	ListNode* frontnode = phead->next;
	ListNode* secondnode = frontnode->next;
	free(frontnode);
	secondnode->prev = phead;
	phead->next = secondnode;
}

ListNode* ListFind(ListNode* phead,ListDataType x)
{
	assert(phead != NULL);
	ListNode* cur = phead->next;
	while (cur != phead)
	{
		if ((cur->data) == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void ListInsert(ListNode* pos, ListDataType x)
{
	assert(pos != NULL);
	ListNode* newnode = BuyListNode(x);
	ListNode* posprev = pos->prev;

	newnode->prev = posprev;
	posprev->next = newnode;
	newnode->next = pos;
	pos->prev = newnode;

}

void ListEarse(ListNode* pos)
{
	assert(pos != NULL);
	ListNode* posprev = pos->prev;
	ListNode* posnext = pos->next;
	free(pos);
	posprev->next = posnext;
	posnext->prev = posprev;
}

void Listpushback(ListNode* phead, ListDataType x)
{
	assert(phead != NULL);
	ListInsert(phead, x);
}

void Listpushfront(ListNode* phead, ListDataType x)
{
	assert(phead != NULL);
	ListInsert(phead->next, x);
}

void Listpopback(ListNode* phead)
{
	assert(phead != NULL);
	assert(phead->next != phead);

	ListEarse(phead->prev);
}

void Listpopfront(ListNode* phead)
{
	assert(phead != NULL);
	assert(phead->next != phead);

	ListEarse(phead->next);
}