# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>

typedef int ListDataType;
typedef struct ListNode
{
	struct ListNode* prev;
	ListDataType data;
	struct ListNode* next;
}ListNode;

//双向链表创建结点
ListNode* BuyListNode(ListDataType x);

//双向链表初始化
ListNode* InitList();

//显示双向链表
void PrintList(ListNode* phead);

//销毁双向链表
void DestoryList(ListNode* phead);

//双向链表尾插
void ListPushBack(ListNode* phead, ListDataType x);

//双向链表头插
void ListPushFront(ListNode* phead, ListDataType x);

//双向链表尾删
void ListPopBack(ListNode* phead);

//双向链表头删
void ListPopFront(ListNode* phead);

//双向链表查找(查找到返回结点地址，查找不到返回NULL)
ListNode* ListFind(ListNode* phead,ListDataType x);

//双向链表pos位置前插
void ListInsert(ListNode* phead, ListDataType x);

//双向链表删除pos位置
void ListEarse(ListNode* pos);

//尾插函数改造版本
void Listpushback(ListNode* phead, ListDataType x);

//头插函数改造版本
void Listpushfront(ListNode* phead, ListDataType x);

//尾删函数改造版本
void Listpopback(ListNode* phead);

//头删函数改造版本
void Listpopfront(ListNode* phead);