

//堆是一种完全二叉树,适合使用数组存储,不会造成空间浪费;
//二叉树的顺序存储在物理上是数组,在逻辑上是二叉树;
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>
# include <string.h>

//堆结构的定义
//由于是顺序存储，采用顺序表定义
typedef int HeapDataType;
typedef struct Heap
{
	HeapDataType* nums;//数组
	int size;//数据个数
	int capacity;//记录数组容量
}Heap;

//堆的初始化 
void HeapInit(Heap* php);


//堆的创建(建堆-利用原先数组的空间，将其调整为堆)
void HeapCreate(Heap* php, HeapDataType* nums, int n);

//堆的销毁
void HeapDestroy(Heap* php);

//堆的插入
void HeapPush(Heap* php, HeapDataType x);

//堆的显示
void HeapPrint(Heap* php);

//堆的删除
void HeapPop(Heap* php);

//堆的判空
bool HeapEmpty(Heap* php);

//取堆顶元素
HeapDataType HeapTop(Heap* php);

//堆的数据个数
int HeapSize(Heap* hp);


void swap(HeapDataType* p1, HeapDataType* p2);
//堆的向上调整算法
void AdjustUp(HeapDataType* nums, int child);
//堆的向下调整算法
void AdjustDown(HeapDataType* nums, int n, int parent);







