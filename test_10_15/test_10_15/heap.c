

# include "heap.h"

//堆的初始化
void HeapInit(Heap* php)
{
	assert(php != NULL);
	php->nums = (HeapDataType*)malloc(sizeof(HeapDataType)*4);
	if (php->nums == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	php->size = 0;
	php->capacity = 4;
}

//堆的销毁
void HeapDestroy(Heap* php)
{
	assert(php != NULL);

	free(php->nums);
	php->nums = NULL;
	php->size = 0;
	php->capacity = 0;
}

//交换
void swap(HeapDataType* p1, HeapDataType* p2)
{
	HeapDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

//堆的向上调整算法
void AdjustUp(HeapDataType* nums, int child)
{
	int parent = (child - 1) / 2;
	while (child>0)
	{
		if (nums[child] > nums[parent])
		{
			swap(&nums[child], &nums[parent]);
			child = parent;
			parent = (parent - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//堆的插入
void HeapPush(Heap* php, HeapDataType x)
{
	assert(php != NULL);
	//检查数组容量，容量满时扩容;
	if (php->size == php->capacity)
	{
		HeapDataType* tmp = (HeapDataType*)realloc(php->nums, sizeof(HeapDataType)* 2 * (php->capacity));
		if (tmp == NULL)
		{
			perror("realloc failed:");
			exit(-1);
		}
		php->nums = tmp;
		php->capacity = 2 * (php->capacity);
	}
	//插入堆的叶结点即二叉树的叶节点
	php->nums[php->size] = x;
	php->size++;
	//任意数据插入之后还是堆吗？
	//结论是否定的-->向上调整算法-->调整为堆
	//参数设计为指向数组首元素的指针与数组尾元素的下标，返回值为空
	AdjustUp(php->nums, php->size - 1);

}

//堆的显示
void HeapPrint(Heap* php)
{
	assert(php != NULL);
	for (int i = 0; i < php->size; i++)
	{
		printf("%d ", php->nums[i]);
	}
	printf("\n");
}

//堆的向下调整算法
void AdjustDown(HeapDataType* nums, int n, int parent)
{
	//假设法求同一深度左右孩子最小的一个,假设左孩子为最小
	int child = parent * 2 + 1;
	while (child<n)
	{
		if (child + 1 < n &&nums[child] < nums[child + 1])
		{
			child++;
		}
		//无论左右孩子,child为最小,调整为小堆;
		if ( nums[child] > nums[parent])
		{
			swap(&nums[child], &nums[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}
//堆的删除
void HeapPop(Heap* php)
{
	//删除数组首元素,并且保证逻辑为堆;
	assert(php != NULL);
	assert(php->size > 0);

	swap(&php->nums[0],&php->nums[php->size - 1]);
	php->size--;

	//向下调整
	AdjustDown(php->nums, php->size, 0);
}

//堆的判空
bool HeapEmpty(Heap* php)
{
	assert(php);

	return php->size == 0;
}

//取堆顶元素
HeapDataType HeapTop(Heap* php)
{
	assert(php != NULL);
	assert(php->size > 0);

	return php->nums[0];
}

//堆的数据个数
int HeapSize(Heap* php)
{
	assert(php != NULL);
	
	return php->size;
}

//堆的创建
void HeapCreate(Heap* php, HeapDataType* nums, int n)
{
	assert(php != NULL);
	php->nums = (HeapDataType*)malloc(sizeof(HeapDataType)*n);
	if (php->nums == NULL)
	{
		perror("malloc fail:");
		exit(-1);
	}
	php->size = php->capacity = n;
	memcpy(php->nums, nums, sizeof(HeapDataType)*n);
	//建堆
	for (int i = 1; i < n; i++)
	{
		AdjustUp(php->nums, i);
	}
}

