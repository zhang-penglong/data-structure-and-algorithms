# define  _CRT_SECURE_NO_WARNINGS 1

# include "heap.h"


//int main()
//{
//	int arr[] = { 10, 9, 8, 7, 3, 5 };
//	Heap hp;
//	HeapInit(&hp);
//	int length = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < length; i++)
//	{
//		HeapPush(&hp, arr[i]);
//	}
//	HeapPrint(&hp);
//
//
//	while (!HeapEmpty(&hp))
//	{
//		printf("%d ", HeapTop(&hp));
//		HeapPop(&hp);
//	}
//
//	HeapDestroy(&hp);
//
//	return 0;
//}

//堆排序-升序
//建小堆-小堆根节点处的数值为整棵树的最小值,但是取到最小值后，堆的逻辑结构被破环
//需要重新建堆，建堆的时间复杂度为Nlog(N)，假设有N个数据，那么排序的代价为N*N*log(N);
//建大堆-大堆的根结点处的数值为整棵树的最大值,交换根结点与数组尾元素的数值;
//最大值已处于数组尾元素的位置，交换后控制调整范围从根节点到数组尾元素的上一个位置继续向下调整为堆
//假设共有N个结点，则每次调整的时间复杂度为O(logN),调整N个数据，时间复杂度为N*O(logN);
void HeapSort(int* nums, int n)
{
	//建大堆
	for (int i = 1; i < n; i++)
	{
		AdjustUp(nums, i);
	}
	int end = n - 1;
	while (end>0)
	{
		swap(&nums[end], &nums[0]);
		AdjustDown(nums, end, 0);
		end--;
	}
}
int main()
{
	int arr[] = { 21, 3, 99, 10, 23, 45, 75, 36, 28, 29, 48 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	HeapSort(arr, sz);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}






