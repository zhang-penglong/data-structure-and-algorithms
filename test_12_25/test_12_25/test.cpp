# define  _CRT_SECURE_NO_WARNINGS 1
# include <iostream>
using namespace std;

//初始化列表
//class A
//{
//private:
//	int _a;
//public:
//	//构造函数,非默认构造函数
//	A(int a )
//		:_a(a)
//	{
//
//	}
//};
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//	const int _n;
//	int& _ref;
//	A _aa;//自定义类型
//public:
//	//构造函数
//	Date(int year, int month, int day)
//		//初始化列表为每个成员变量定义的地方;
//		: _n(10)      //const成员变量
//		, _ref(year)  //引用变量	
//		, _aa(10)  //自定义类型成员
//	{
//
//	}
//};
//int main()
//{
//	Date d(2000,12,20);
//	return 0;
//}

//class Stack
//{
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//public:
//	Stack(int capacity = 4)
//		:_a((int*)malloc(sizeof(int)*capacity))
//		, _capacity(4)
//		, _top(4)
//	{
//		if (_a == nullptr)
//		{
//			perror("malloc failed");
//			exit(-1);
//		}
//	}
//};

//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{
//
//	}
//
//	void Print() 
//	{
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//int main() 
//{
//	A aa(1);
//	aa.Print();
//}

//static 成员函数

//void Func()
//{
//	int a = 0;
//    static int b = 0;
//	a++;
//	b++;
//	cout << "a=" << a << "b=" << b << endl;
//}
//int main()
//{
//	for (int i = 0; i < 2; i++)
//	{
//		Func();
//	}
//	return 0;
//}

//test.cpp文件
//extern int k;//外部声明
//int main()
//{
//	cout << "k=" << k << endl;
//	return 0;
//}
//namespace zbw
//{
//	int count = 0;
//}
//class A
//{
//private:
//public:
//	//构造函数
//	A()
//	{
//		zbw::count++;
//	}
//	//拷贝构造函数
//	A(const A& t)
//	{
//		zbw::count++;
//	}
//	//析构函数
//	~A()
//	{}
//};
//A func()
//{
//	A aa;
//	return aa;
//}
//int main()
//{
//	A aa;
//	func();
//	cout << "count=" << zbw::count << endl;
//	return 0;
//}

//count全局变量为防止被修改变为A类的成员变量,变为成员变量后能否完成计数?
//创建每个对象时，每个对象都存在count，无法完成计数
//class A
//{
//private:
//	//静态成员变量声明
//	static int count;
//	int _a = 0;
//public:
//	//静态成员函数，特点:没有this指针
//	static int GetCount()
//	{
//		_a++;
//		return count;
//	}
//	//构造函数
//	A()
//	{
//	   count++;
//	}
//	//拷贝构造函数
//	A(const A& t)
//	{
//		count++;
//	}
//	//析构函数
//	~A()
//	{}
//};
////静态成员变量定义
//int A::count = 0;
//int main()
//{
//	A aa;
//	cout <<A::GetCount()<< endl;
//	return 0;
//}

//explicit关键字
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	//多参数的构造函数
//	Date(int year=1, int month=1, int day=1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//
//	}
//};
//int main()
//{
//	Date d = 2000;
//	return 0;
//}

//编译器优化
// 扩展：一些构造时的优化，不同的编译器可能会不同（了解一下）
class A
{
public:
	//构造函数
	A(int a = 0)
		:_a(a)
	{
		cout << "调用构造函数" << endl;
	}
	//拷贝构造函数
	A(const A& aa)
		:_a(aa._a)
	{
		cout << "调用拷贝构造函数" << endl;
	}
	//析构函数
	~A()
	{
		cout << "调用析构函数" << endl;
	}
private:
	int _a;
};

//int main()
//{
//	A aa1 = 1;  
//	return 0;
//}

//void func(A aa1)
//{
//
//}
//
//int main()
//{
//	// 构造
//	A aa(1);
//	// 拷贝构造
//	func(aa);
//
//	//构造+拷贝构造->构造 
//	func(A(2));
//
//	//构造+拷贝构造->构造 
//	func(3);
//
//	return 0;
//}

//注：编译器优化时，无论a,b,c哪种情形，必须在同一表达式中执行;
A Func()
{
	A aa;
	return aa;
}
int main()
{
	A aa2;
	//aa2对象已经存在
	//Func()函数运行到返回时先将aa拷贝构造到临时对象中，
	//由于aa2对象已存在,再将临时对象中赋值拷贝到aa2中
	aa2 = Func();
	return 0;
}
