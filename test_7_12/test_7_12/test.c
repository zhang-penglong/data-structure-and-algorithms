# define  _CRT_SECURE_NO_WARNINGS 1

# include<stdio.h>

//空心三角形的打印
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) == 1)
//	{
//		int i = 0;
//		int j = 0;
//		for (i = 0; i<n; i++)
//		{
//			for (j = 0; j <= i; j++)
//			{
//				if ((i == n - 1) || (j == 0) || (j == i))
//				{
//					printf("* ");
//				}
//				else
//				{
//					printf("  ");
//				}
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}

//x型图案的打印
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) == 1)
//	{
//		int i = 0;
//		int j = 0;
//		for (i = 0; i < n; i++)
//		{
//			for (j = 0; j < n; j++)
//			{
//				if (i == j || i + j == n - 1)
//				{
//					printf("*");
//				}
//				else
//				{
//					printf(" ");
//				}
//			}
//			printf("\n");
//		}
//	}
//	return 0;
//}


//有一个整数序列（可能有重复的整数），现删除指定的某一个整数，
//输出删除指定数字之后的序列，序列中未被删除数字的前后位置没有发生改变

//输入描述：
//第一行输入一个整数(0≤N≤50)
//第二行输入N个整数，输入用空格分隔的N个整数
//第三行输入想要进行删除的一个整数

//思路:
//

int main()
{
	int n = 0;
	int arr[50] = { 0 };
	scanf("%d", &n);
	int i = 0;
	int j = 0;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
	}
	int del_number = 0;
	scanf("%d", &del_number);
	//删除
	i = 0;//i遍历数组;
	j = 0;//j记录可以存放数据的当前位置;
	for (i = 0; i < n; i++)
	{
		if (arr[i] != del_number)
		{
			arr[j] = arr[i];
			j++;
		}
	}
	for (i = 0; i < j; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

































