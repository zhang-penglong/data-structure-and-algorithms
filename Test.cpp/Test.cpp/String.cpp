# define  _CRT_SECURE_NO_WARNINGS 1
//string类函数定义
#include "String.h"
namespace zbw
{
	//构造函数
	string::string(const char* str)
	{
		_size = strlen(str);
		_capacity = _size;
		_str = new char[_capacity + 1];
		strcpy(_str, str);
	}
	//析构函数
	string::~string()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}
	//拷贝构造函数-深拷贝
	//s2(s1)
	string::string(const string& s)
	{
		_str = new char[s._capacity + 1];
		strcpy(_str, s._str);
		_size = s._size;
		_capacity = s._capacity;
	}
	//赋值运算符重载 s1=s2
	string& string::operator=(const string& s)
	{
		if (this != &s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);
			delete[] _str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;
		}
		return *this;
	}
	//扩容(异地扩容)
	void string::reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;
			_str = tmp;
			_capacity = n;
		}
	}
	//尾插单个字符
	void string::push_back(char ch)
	{
		//判断容量
		if (_capacity == _size)
		{
			//string s1-->_capacity=0;
			size_t NewCapacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(NewCapacity);
		}
		//尾插
		_str[_size] = ch;
		_size++;
		//处理字符'\0'
		_str[_size] = '\0';
	}
	//尾插字符串
	void string::append(const char* s)
	{
		size_t len = strlen(s);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		strcpy(_str + _size, s);
		_size += len;
	}
	// +=
	string& string::operator+=(char ch)
	{
		push_back(ch);
		return *this;
	}
	string& string::operator+=(const char* s)
	{
		append(s);
		return *this;
	}
	//pos位置插入单个字符
	void string::insert(size_t pos, char ch)
	{
		assert(pos < _size);
		//检查容量
		if (_capacity == _size)
		{
			size_t NewCapacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(NewCapacity);
		}
		//从后向前向后移动数据
		int end = _size;
		while (end >= (int)pos)
		{
			_str[end + 1] = _str[end];
			end--;
		}
		//插入字符ch
		_str[pos] = ch;
		_size++;
	}
	//pos位置插入字符串
	void string::insert(size_t pos, const char* str)
	{
		assert(pos < _size);
		//检查容量
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		//移动数据,步长为len
		int end = _size;
		while (end >= (int)pos)
		{
			_str[end + len] = _str[end];
			--end;
		}
		//插入字符串(注意待插入字符串的'\0')
		strncpy(_str + pos, str, len);
		_size += len;
	}
	//pos位置删除len个字符
	void string::erase(size_t pos, size_t len)
	{
		//pos+len>_size 删除pos位置之后的所有字符
		//len未指定 删除pos位置之后的所有字符
		assert(pos < _size);
		if (len == npos || pos + len>_size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}

	//从pos位置向后查找字符ch在string中第一次出现的位置
	size_t string::find(char ch, size_t pos)
	{
		for (size_t i = pos; i < _size;i++)
		{
			if (ch == _str[i])
			{
				return i;
			}
		}

		return npos;
	}
	//从pos位置向后查找子串str在string中第一次出现的位置
	//字符串匹配--BM算法 KMP算法
	size_t string::find(const char* str, size_t pos)
	{
		const char* ptr = strstr(_str + pos, str);
		if (ptr == nullptr)
		{
			return npos;
		}
		else
		{
			return ptr - _str;
		}
	}
	string string::substr(size_t pos, size_t len)
	{
		assert(pos < _size);
		size_t end = pos + len;
		if (len == npos || pos + len >= _size)
		{
			end = _size;
		}

		string str;
		str.reserve(end - pos);
		for (size_t i = pos; i < end; i++)
		{
			str += _str[i];
		}

		return str;
	}


}
