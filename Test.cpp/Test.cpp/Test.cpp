# define  _CRT_SECURE_NO_WARNINGS 1
#include "String.h"

namespace zbw
{
	void TestString1()
	{
		string s1("hello world");
		//迭代器遍历
		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << "";
			it++;
		}
		//下标+[]遍历
		cout << endl;
		string s2("xxxxxxxxxxx");
		cout << s2.c_str() << endl;
		s1 = s2;
		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;
		cout << s1.c_str() << endl;
	}

	void TestString2()
	{
			string s1("hello world");
			cout << s1.c_str() << endl;

			s1 += ' ';
			cout << s1.c_str() << endl;
			s1 += "xxxxxx";
			cout << s1.c_str() << endl;

			s1.insert(5, 'y');
			s1.insert(5, 'y');
			s1.insert(5, 'y');
			cout << s1.c_str() << endl;

			s1.insert(0, 'y');
			cout << s1.c_str() << endl;

			s1.insert(0, "zzzzzzz");
			cout << s1.c_str() << endl;
	}
	void TestString3()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		s1.erase(5, 4);
		cout << s1.c_str() << endl;

		s1.erase(5, 100);
		cout << s1.c_str() << endl;

		s1.erase(2);
		cout << s1.c_str() << endl;
	}
	void TestString4()
	{
		string s1("hello world");
		string s2("xxxx");
		std::swap(s1, s2);
		s1.swap(s2);

		string str("https://legacy.cplusplus.com/reference/string/string/substr/");
		string sub1, sub2, sub3;
		size_t pos1 = str.find(':');
		sub1 = str.substr(0, pos1 - 0);
		cout << sub1.c_str() << endl;

		size_t pos2 = str.find('/', pos1 + 3);
		sub2 = str.substr(pos1 + 3, pos2 - (pos1 + 3));
		cout << sub2.c_str() << endl;

		sub3 = str.substr(pos2 + 1);
		cout << sub3.c_str() << endl;
	}
}


int main()
{
	zbw::TestString4();
	return 0;
}