# define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <assert.h>
namespace zbw
{
	class string
	{
	private:
		char*  _str;
		size_t _size;
		size_t _capacity;
		const static size_t npos = -1;
	public:

		//构造函数
		string(const char* str = "");
		//拷贝构造函数
		string(const string& str);
		//析构函数
		~string();
		//赋值运算符重载
		string& operator=(const string& s);

		//string类转换为字符串
		const char* c_str()
		{
			return _str;
		}
		//普通迭代器--迭代器指向的内容可以被修改
		typedef char* iterator;
        iterator begin()
		{
			return _str;
		}
	    iterator end()
		{
			return _str + _size;
		}
		//const迭代器--迭代器指向的内容不可被修改
		typedef const char* const_iterator;
		const_iterator begin()const
		{
			return _str;
		}
		const_iterator end()const
		{
			return _str + _size;
		}
		//下标+[]
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		const char& operator[](size_t pos)const
		{
			assert(pos < _size);
			return _str[pos];
		}
		size_t size()const
		{
			return _size;
		}
		size_t capacity()const
		{
			return _capacity;
		}
		bool empty()const
		{
			return _size == 0;

		}
		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		//reserve-->VS扩容不缩容
		void reserve(size_t n);
		//尾插单个字符;
		void push_back(char ch);
		//尾插字符串
		void append(const char* str);
		//尾插 +=
		string& operator+=(char ch);
		string& operator+=(const char* s);
		//pos位置插入单个字符
		void insert(size_t pos, char ch);
		//pos位置插入字符串
		void insert(size_t pos, const char* str);
		//pos位置删除len个字符
		void erase(size_t pos, size_t len = npos);
		//从pos位置向后查找字符ch在string中第一次出现的位置
		size_t find(char ch, size_t pos = 0);
		//从pos位置向后查找子串str在string中第一次出现的位置
		size_t find(const char* str, size_t pos = 0);
		//从pos位置开始向后提取长度为len的子字符串
		string substr(size_t pos, size_t len = npos);

		bool operator<(const string& s)const
            {
			int res = strcmp(_str, s._str);
			if (res < 0)
				return true;
			return false;
		}
		bool operator<=(const string& s)const
		{
			return !(*this > s);
		}
		bool operator>(const string& s)const
		{
			int res = strcmp(_str, s._str);
			if (res > 0)
            return true;
			return false;
		}
		bool operator>=(const string& s)const
		{
			return !(*this < s);
		}
		bool operator==(const string& s)const
		{
			int res = strcmp(_str, s._str);
			if (res == 0)
				return true;

			return false;
		}
		bool operator!=(const string& s)const
		{
			return !(*this == s);
		}
	};
}