# define  _CRT_SECURE_NO_WARNINGS 1
# include "SList.h"

//显示顺序表
void PrintSList(SListNode* phead)
{
	SListNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

//创建新结点
SListNode* BuySListNode(SLTDatatype x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));
	if (newnode == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

//尾插新结点
void SLTpushback(SListNode** pphead, SLTDatatype x)
{
	SListNode* newnode = BuySListNode(x);
	//链表为空,没有结点
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	//链表不为空,存在结点
	else
	{
		SListNode* tail = *pphead;
		while ((tail->next) != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}

//头插新结点
void SLTpushfront(SListNode** pphead, SLTDatatype x)
{
	SListNode* newnode = BuySListNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

//单链表尾删
void SLTpopback(SListNode** pphead)
{
    //单链表为空
	assert(*pphead != NULL);
   //单链表只有一个结点
	if ((*pphead)->next == NULL)
	{
		free(*pphead);//其实*pphead就是pList
		*pphead = NULL;
	}
   //单链表有多个结点
	else
	{
		SListNode* tailPrev = NULL;//记录尾结点的前一个结点
		SListNode* tail = *pphead; //寻找尾结点
		while (tail->next!=NULL)
		{
			tailPrev = tail;
			tail = tail->next;
		}
		free(tail);
		tailPrev->next = NULL;
	}
}

//单链表头删
void SLTpopfront(SListNode** pphead)
{
	//判空
	assert(*pphead!=NULL);
	//非空
	SListNode* newhead = (*pphead)->next;
	free(*pphead);
	*pphead = newhead;
}

//单链表查找
SListNode* SListFind(SListNode* phead, SLTDatatype x)
{
	assert(phead != NULL);

	SListNode* cur = phead;
	while (cur != NULL)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void SListInsert(SListNode** pphead, SListNode* pos, SLTDatatype x)
{
	assert(pos != NULL);

	if (*pphead == pos)
	{
		SLTpushfront(pphead, x);
	}
	else
	{
		SListNode* posprev = *pphead;
		while (posprev->next != pos)
		{
			posprev = posprev->next;
		}
		SListNode* newnode = BuySListNode(x);
		newnode->next = pos;
		posprev->next = newnode;
	}
}

void SListInsertAfter(SListNode* pos, SLTDatatype x)
{
	assert(pos != NULL);

	SListNode* newnode = BuySListNode(x);
	newnode->next = pos->next;
	pos->next = newnode;

}

void SListEarse(SListNode** pphead, SListNode* pos)
{
	assert(pos != NULL);

	if (*pphead == pos)
	{
		SLTpopfront(pphead);
	}
	else
	{
		SListNode* posprev = *pphead;
		while (posprev->next != pos)
		{
			posprev = posprev->next;
		}
		posprev->next = pos->next;
		free(pos);
	}
}

void SListEarseAfter(SListNode* pos)
{
	assert(pos);

	// 检查pos是否是尾节点
	assert(pos->next);

	SListNode* posnext = pos->next;

	pos->next = posnext->next;

	free(posnext);
	posnext = NULL;

}