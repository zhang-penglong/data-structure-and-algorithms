# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
typedef int SLTDatatype;

typedef struct SListNode
{
	SLTDatatype data;
	struct SListNode* next;
}SListNode;

//显示单链表
void PrintSList(SListNode* phead);

//创建新结点
SListNode* BuySListNode(SLTDatatype x);

//尾插新结点
void SLTpushback(SListNode** pphead, SLTDatatype x);

//头插新结点
void SLTpushfront(SListNode** pphead, SLTDatatype x);

//单链表尾删
void SLTpopback(SListNode** pphead);

//单链表头删
void SLTpopfront(SListNode** pphead);

//单链表查找，按照数据进行查找，找到返回结点位置，找不到返回空指针

SListNode* SListFind(SListNode* phead, SLTDatatype x);

//单链表在pos位置之前插入数据x(pos位置指结点地址)
void SListInsert(SListNode** pphead, SListNode* pos, SLTDatatype x);

//单链表在pos位置之后插入数据x(pos位置指结点地址)
void SListInsertAfter(SListNode* pos, SLTDatatype x);

//单链表删除pos位置处的值
void SListEarse(SListNode** pphead, SListNode* pos);

void SListEarseAfter(SListNode* pos);