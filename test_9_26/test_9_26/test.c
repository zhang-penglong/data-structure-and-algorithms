# define  _CRT_SECURE_NO_WARNINGS 1
# include "SList.h"

void Test1()
{
	SListNode* p1 = (SListNode*)malloc(sizeof(SListNode));
	p1->data = 10;
	SListNode* p2 = (SListNode*)malloc(sizeof(SListNode));
	p2->data = 20;
	SListNode* p3 = (SListNode*)malloc(sizeof(SListNode));
	p3->data = 30;

	p1->next = p2;
	p2->next = p3;
	p3->next = NULL;
	PrintSList(p1);
}

void Test2()
{
	SListNode* n1 = BuySListNode(10);
	SListNode* n2 = BuySListNode(20);
	SListNode* n3 = BuySListNode(30);

	n1->next = n2;
	n2->next = n3;
	PrintSList(n1);
}

void Test3()
{
	int n;
	printf("请输入链表的长度: \n");
	scanf("%d", &n);
	printf("请依次输入每个节点的值: \n");
	SListNode* plist = NULL;

	for (int i = 0; i < n; i++)
	{
		int val;
		scanf("%d", &val);
		SListNode* newnode = BuySListNode(val);
		// 头插
		newnode->next = plist;
		plist = newnode;
	}
	PrintSList(plist);
}


//尾插错误示例1
//void SLTpushback(SListNode* phead, SLTDatatype x)
//{
//	SListNode* newnode = BuySListNode(x);
//	SListNode* tail = phead;
//
//	while (tail != NULL)
//	{
//		tail = tail->next;
//	}
//	tail = newnode;
//}

//尾插正确做法1(链表中存在元素)
//结点是由malloc()函数在堆区申请的空间,改变堆区的数据随着函数栈帧的销毁数据并不会销毁，
//而形式参数，局部变量在栈空间上创建，函数栈帧销毁后随之销毁;
//只需要改变尾结点的指针域，使得其指向新结点，从而就可以实现尾插;
//void SLTpushback(SListNode* phead, SLTDatatype x)
//{
//	SListNode* newnode = BuySListNode(x);
//	SListNode* tail = phead;
//
//	while ((tail->next) != NULL)
//	{
//		tail = tail->next;
//	}
//	tail->next = newnode;
//}

//尾插错误示例2
//void SLTpushback(SListNode* phead, SLTDatatype x)
//{
//	SListNode* newnode = BuySListNode(x);
//	//链表为空,没有结点
//	if (phead == NULL)
//	{
//		phead = newnode;
//	}
//	//链表不为空,存在结点
//	else
//	{
//		SListNode* tail = phead;
//		while ((tail->next) != NULL)
//		{
//			tail = tail->next;
//		}
//		tail->next = newnode;
//	}
//}

//单链表尾删实现方式2
//void SLTpopback(SListNode** pphead)
//{
//	//单链表为空
//	assert(*pphead != NULL);
//	//单链表只有一个结点
//	if ((*pphead)->next == NULL)
//	{
//		free(*pphead);//其实*pphead就是pList
//		*pphead = NULL;
//	}
//	//单链表有多个结点
//	else
//	{
//		SListNode* tail = *pphead;
//		while (tail->next->next!=NULL)
//		{
//			tail = tail->next;
//		}
//
//		free(tail->next);
//		tail->next = NULL;
//	}
//}

//链表中没有节点
void Test4()
{
	SListNode* pList = NULL;
	SLTpushback(&pList, 1);
	SLTpushback(&pList, 2);
	SLTpushback(&pList, 3);

	PrintSList(pList);
}

void Test5()
{
	SListNode* pList = NULL;
	SLTpushfront(&pList, 10);
	SLTpushfront(&pList, 20);
	SLTpushfront(&pList, 30);
	SLTpushfront(&pList, 40);
	PrintSList(pList);
}
void Test6()
{
		SListNode* plist = NULL;
		SLTpushback(&plist, 11);
		SLTpushback(&plist, 22);
		SLTpushback(&plist, 33);
		PrintSList(plist);


		SLTpopback(&plist);
		PrintSList(plist);

		SLTpopback(&plist);
		PrintSList(plist);

		SLTpopback(&plist);
		PrintSList(plist);
}
void Test7()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 10);
	SLTpushback(&plist, 20);
	SLTpushback(&plist, 30);
	PrintSList(plist);

	SLTpopfront(&plist);
	PrintSList(plist);

	SLTpopfront(&plist);
	PrintSList(plist);

	SLTpopfront(&plist);
	PrintSList(plist);
}

void Test8()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 10);
	SLTpushback(&plist, 20);
	SLTpushback(&plist, 30);
	SLTpushback(&plist, 40);
	SLTpushback(&plist, 50);
	printf("请输入要查找的值\n");
	int x = 0;
	scanf("%d", &x);
	SListNode* pos = SListFind(plist, x);
	if (pos != NULL)
	{
		pos->data = pos->data * 10;
	}
	PrintSList(plist);
}

void Test9()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 1);
	SLTpushback(&plist, 2);
	SLTpushback(&plist, 3);
	SLTpushback(&plist, 4);
	SLTpushback(&plist, 5);

	int x = 0;
	scanf("%d", &x);
	SListNode* pos = SListFind(plist, x);
	if (pos != NULL)
	{
		SListInsert(&plist, pos, x*10);
	}
	PrintSList(plist);
}

void Test10()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 1);
	SLTpushback(&plist, 2);
	SLTpushback(&plist, 3);
	SLTpushback(&plist, 4);
	SLTpushback(&plist, 5);

	int x = 0;
	scanf("%d", &x);
	SListNode* pos = SListFind(plist, x);
	if (pos != NULL)
	{
		SListInsertAfter(pos, x * 10);
	}
	PrintSList(plist);
}

void Test11()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 1);
	SLTpushback(&plist, 2);
	SLTpushback(&plist, 3);
	SLTpushback(&plist, 4);
	SLTpushback(&plist, 5);
	PrintSList(plist);

	int x = 0;
	scanf("%d", &x);
	SListNode* pos = SListFind(plist, x);
	if (pos != NULL)
	{
		SListEarse(&plist, pos);
	}
	PrintSList(plist);
}
void Test12()
{
	SListNode* plist = NULL;
	SLTpushback(&plist, 1);
	SLTpushback(&plist, 2);
	SLTpushback(&plist, 3);
	SLTpushback(&plist, 4);
	SLTpushback(&plist, 5);
	PrintSList(plist);

	int x = 0;
	scanf("%d", &x);
	SListNode* pos = SListFind(plist, x);
	if (pos != NULL)
	{
		SListEarseAfter(pos);
	}
	PrintSList(plist);
}
int main()
{
	//Test2();
	//Test3();
	//Test4();
	//Test5();
	//Test6();
	//Test7();
	//Test8();
	//Test9();
	//Test10();
	//Test11();
	Test12();
	return 0;
}

