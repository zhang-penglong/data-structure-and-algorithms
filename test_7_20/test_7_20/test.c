# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <string.h>
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串
//int is_left_move(char*str1, char* str2)
//{
//	int j = 0;
//	int len = strlen(str1);
//
//	for (j = 0; j < len; j++)
//	{
//		char tmp = *str1;
//		int len = strlen(str1);
//		int i = 0;
//		for (i = 0; i < len - 1; i++)
//		{
//			*(str1 + i) = *(str1 + i + 1);
//		}
//		*(str1 + len - 1) = tmp;
//		if (strcmp(str1, str2) == 0)
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "cdefab";
//	int ret = is_left_move(arr1, arr2);
//	if (ret == 1)
//	{
//		printf("Yes\n");
//	}
//	else
//		printf("No\n");
//	return 0;
//}

//找出单身狗版本1
//有一个数组只有一个数字出现一次，其余数字都是成对出现;
//找出只出现一次的数字;
//1 2 3 4 5 1 2 3 4

//异或法
//相异为1，相同为0;
// a^a=0;   0^a=a;
// a^b=b^a;

//将数组中的数字全部异或在一起即可得到单身狗数字
//int find_single_dog1(int arr[], int sz)
//{
//	int i = 0;
//	int ret = 0;
//	for (i = 0; i < sz; i++)
//	{
//		ret = ret^arr[i];
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 1, 2, 3, 4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int dog=find_single_dog1(arr, sz);
//	printf("%d\n", dog);
//	return 0;
//}

//找出单身狗版本2
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字

//将原始数据进行分组;
//分组后，每组元素的特点只有一个数字出现一次;其他数字成对出现;

//1 2 3 4 5 6 1 2 3 4
//1 1 3 3 5 - 第一组
//2 2 4 4 6 - 第二组
//void find_single_dog2(int arr[], int sz)
//{
//	//1.所有数字异或在一起;
//	//1^1^2^2^3^3^4^4^5^6=5^6
//	int ret = 0;
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		ret = ret^arr[i];
//	}
//	//计算ret的第几位为1，就以第几位为1进行分组;
//	int pos = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((ret >> 1) & 1) == 1)
//		{
//			pos = i;
//			break;
//		}
//	}
//	int dog1 = 0;
//	int dog2 = 0;
//	//计算数组中第pos位为1的进行异或
//	for (i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> pos) & 1) == 1)
//		{
//			dog1 ^= arr[i];
//		}
//	}
//	//计算数组中第pos位为0的进行异或
//	/*for (i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> pos) & 1) == 0)
//		{
//			dog2 ^= arr[i];
//		}
//	}*/
//	dog2 = ret^dog1;
//	printf("%d %d", dog1, dog2);
//}
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 1, 2, 3, 4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	find_single_dog2(arr, sz);
//	return 0;
//}


//模拟实现strncpy
#include<assert.h>
//char* my_strncpy(char*dest, const char*src, size_t n)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char*ret = dest;
//	while (n)
//	{
//		*dest = *src;
//		src++;
//		dest++;
//		n--;
//	}
//	return ret;
//}
//int main()
//{
//	char arr[20] = { 0 };
//	char*p = "bit";
//	int n = 0;
//	printf("请输入拷贝的字符个数：\n");
//	scanf("%d", &n);
//	char*ret = my_strncpy(arr, p, n);
//	printf("%s\n", ret);
//	return 0;
//}

char* my_strncat(char* dest, const char* src, size_t n)
{
	assert(dest && src);
	int count = 1;//计数器。
	int len = strlen(src);//与count判断。
	char* ret = dest;//存储目的首地址。
	int lend = strlen(dest);//目的字符串长度。
	int lens = strlen(src);//源字符串长度。
	while (n)
	{
		if (count <= len)//当count=len，执行一次，将源字符串最后一个字符拷贝到目的地址中。
		{
			dest[lend] = *src;//从目的字符串的‘\0’开始。
			lend++;
			src++;//当count=len，src指向的时源字符串中的‘\0’。
			count++;//成功交换一次则自增。
			n--;
		}
		else
		{
			dest[lend] = '\0';//此时len已经完成自增，赋值为'\0'。
			break;
		}
	}
	if (n == 0)//while循环结束则加'\0',else语句中break，则不加。
	{
		dest[lend] = '\0';
	}
	return ret;//返回目的首地址。
}

int main()
{
	char ch[30] = "xxx\0xxxxxxxxx";
	char* p = "hello";
	char* ret = my_strncat(ch, p, 8);
	printf(ret);
	return 0;
}
# include <stdlib.h>
//atoi 将数字字符串转换为整型
//int main()
//{
//	int ret = atoi("-123");
//	printf("%d\n", ret);
//	return 0;
//}

//int my_atoi(const char* str)
//{
//	int flag = 1;
//	if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	int ret = 0;
//	while (*str != '\0')
//	{
//		ret = ret * 10 + (*str) - '0';
//		str++;
//	}
//	return ret*flag;
//}
//改进代码 解决如下四个问题
// 1.空指针   2.空字符串
// 3.空白字符 4.数字超过范围
//# include<ctype.h>
//# include<limits.h>
//enum state
//{
//	VAILD,
//	INVAILD
//}state = INVAILD;//全局的状态,它的值标志返回的值合法还是非法
//int my_atoi(const char* str)
//{
//	int flag = 1;
//	assert(str != NULL);
//	//空字符串
//	if (*str == '\0')
//	{
//		return 0;
//	}
//   //跳过空白字符
//	while (isspace(*str))
//	{
//		str++;
//	}
//	//处理正负号的问题
//	if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	else if (*str == '+')
//	{
//		str++;
//	}
//	long long ret = 0;
//	while (*str != '\0')
//	{
//		if (isdigit(*str))
//		{
//			ret = ret * 10 + flag*((*str) - '0');
//			if (ret<INT_MIN || ret>INT_MAX)
//			{
//				return 0;
//			}
//		}
//		else
//		{
//			return (int)ret;
//		}
//		str++;
//	}
//	if (*str == '\0')
//	{
//		state = VAILD;
//		return (int)ret;
//	}
//}
//int main()
//{
//	int ret = my_atoi("  123");
//	if (state == VAILD)
//	{
//		printf("%d\n", ret);
//	}
//	else
//	{
//		printf("非法返回:%d", ret);
//	}
//	return 0;
//}






















