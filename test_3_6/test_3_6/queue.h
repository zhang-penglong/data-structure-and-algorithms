
#include <iostream>
#include <vector>
#include <functional>
using namespace std;
namespace p_que
{
template<class T, class Container =vector<T>, class Compare = less<T>>
class priority_queue
{
public:
	bool empty() const
	{
		return _con.empty();
	}
	size_t size() const
	{
		return _con.size();
	}
	const T& top() const
	{
		return _con[0];
	}
	T& top()
	{
		return _con[0];
	}
	//向上调整算法
	void adjustup(size_t child)
	{
		size_t parent = (child - 1) / 2;
		while (child > 0)
		{
			//_con[parent]<_con[child],父节点小于其孩子节点，交换向上调整--->大堆
			if (_cmp(_con[parent], _con[child]))
			{
				swap(_con[parent], _con[child]);
				child = parent;
				parent = (child - 1) / 2;
			}
			else
			{
				break;
			}
		}
	}
	void push(const T& val)
	{
		_con.push_back(val);
		adjustup(_con.size() - 1);
	}
	//向下调整算法
	void adjustdown(size_t parent)
	{
		size_t child = parent * 2 + 1;
		while (child <_con.size())
		{
			 //child+1< _con.size()保证右孩子结点存在
			//less<T> _cmp ---> _con[child]<_con[child+1]--->child++ --->挑选大的孩子结点
			if (child + 1 < _con.size() && _cmp(_con[child], _con[child + 1]))
			{
				child++;
			}
			//_con[parent]<_con[child],父节点小于其孩子结点,交换向下调整---->大堆
			if (_cmp(_con[parent], _con[child]))
			{
				swap(_con[parent], _con[child]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
			{
				break;
			}
		}
	}
	void pop()
	{
		std::swap(_con[0], _con[_con.size()-1]);
		_con.pop_back();
		adjustdown(0);
	}
private:
	Container _con;
	Compare _cmp;//less<T> _cmp;
};
}