# define  _CRT_SECURE_NO_WARNINGS 1

////此模版参数接收数据类型
//template<class T>
//struct Greater
//{
//	//重载operator()称为仿函数
//	bool operator()(const T& x, const T& y)
//	{
//		return x > y;
//	}
//};
////此模版参数接收仿函数类型
//template<class compare>
//class A
//{
//public:
//	void func(int a, int b)
//	{
//		compare com;
//		cout << com(a, b) << endl;
//		//com(a,b)--->com.operator()(a,b)--->com为仿函数对象
//	}
//};
//
//int main()
//{
//	//A类中传递仿函数类型Greater<T>-->仿函数类中传递数据类型Greater<int>
//	A<Greater<int>> aa1;
//	aa1.func(10, 20);
//
//	A<Greater<int>>aa2;
//	aa2.func(20, 10);
//	return 0;
//}
//void Func()
//{
//	int x = 10;
//	int y = 20;
//	//创建一个仿函数对象
//	Greater<int> Great;
//	//通过对象调用仿函数
//	cout << Great(x, y) << endl;
//	//cout<<Great.operator()(x,y)<<endl;
//	//通过匿名对象调用仿函数
//	cout << Greater<int>()(x, y) << endl;
//}
//int main()
//{
//	Func();
//	return 0;
//}

//#include <iostream>
//#include<vector>
//#include <functional>
//template<class T, class Container = vector<T>>
//class priority_queue
//{
//public:
//	//获取堆顶元素即数组首元素
//	const T& top()
//	{
//		return _con[0];
//	}
//	//获取堆的数据个数即容器中的数据个数
//	size_t size()
//	{
//		return _con.size();
//	}
//	//检测堆是否为空即容器是否为空
//	bool empty()
//	{
//		return _con.empty();
//	}
//
//	//向上调整为小堆
//	void adjustup(int child)
//	{
//		int parent = (child - 1) / 2;
//		while (child > 0)
//		{
//			if (_con[child] < _con[parent])
//			{
//				//交换
//				swap(_con[child], _con[parent]);
//				//向上走
//				child = parent;
//				parent = (child - 1) / 2;
//			}
//			else
//			{
//				break;
//			}
//		}
//	}
//
//	void push(const T& val)
//	{
//		//插入数据
//		_con.push_back(val);
//
//		//从插入值为val的孩子结点的下标开始向上调整为小堆/大堆
//		adjustup(_con.size()-1);
//	}
//	//向下调整为小堆
//	void adjustdown(int parent)
//	{
//		int child = 2 * parent + 1;
//		while (child < _con.size())
//		{
//			//假设法求同一层孩子节点最小者
//			if (child + 1 < _con.size() && _con[child] > _con[child + 1])
//			{
//				child++;
//			}
//			//逻辑关系不满足小堆，交换调整
//			if (_con[child] < _con[parent])
//			{
//				swap(_con[child], _con[parent]);
//				parent = child;
//				child = 2 * parent + 1;
//			}
//			else
//			{
//				break;
//			}
//		}
//	}
//	void pop()
//	{
//		//将堆顶元素与堆中最后一个元素交换
//		swap(_con[0], _con[size() - 1]);
//		//删除最后一个元素
//		_con.pop_back();
//		//将堆顶元素利用向下调整算法调整到满足其逻辑结构为堆
//		adjustdown(0);
//	}
//private:
//	Container _con;
//};




//int main()
//{
//	priority_queue<int, vector<int>,greater<int>> pq;
//	//尾插数据
//	pq.push(1);
//	pq.push(2);
//	pq.push(6);
//	pq.push(2);
//	pq.push(3);
//	cout << "size=" << pq.size() << endl;
//	while (!pq.empty())
//	{
//		//取堆顶数据
//		cout << pq.top() << " ";
//		//删除堆顶元素
//		pq.pop();
//	}
//	cout << endl;
//	return 0;
//}
#include "queue.h"
int main()
{
	p_que::priority_queue<int, vector<int>,greater<int>> pq;
	//尾插数据
	pq.push(1);
	pq.push(2);
	pq.push(6);
	pq.push(2);
	pq.push(3);
	cout << "size=" << pq.size() << endl;
	while (!pq.empty())
	{
		//取堆顶数据
		cout << pq.top() << " ";
		//删除堆顶元素
		pq.pop();
	}
	cout << endl;
	return 0;
}