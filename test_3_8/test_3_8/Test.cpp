# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;

//  父类/基类
//class Person
//{
//public:
//	void print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	string _name = "Peter";
//private:
//	int _age = 20;
//};
////  子类/派生类
//class Student :public Person
//{
//protected:
//	int _stuid=1;//学号
//};
////  子类/派生类
//class Teacher :public Person
//{
//protected:
//	int _jobid=0;//工号
//};
//
//int main()
//{
//	Student s;
//	Teacher t;
//	s.print();//子类Student调用父类Person的成员函数
//	t.print();//子类Teacher调用父类Person的成员函数
//	return 0;
//}

//class Person
//{
//public:
//	void print()
//	{
//		cout << "Person::print()" << endl;
//	}
//protected:
//	string _name = "Peter";
//private:
//	int _age = 20;
//};
//class Student :private Person
//{
//public:
//	//Func函数测试父类Person成员函数与成员变量的访问权限
//	void Func()
//	{
//		cout << "Student::" << _name << endl;
//		//cout << "Student::" << _age << endl;
//	}
//protected:
//	int _stuid;//学号
//};
//int main()
//{
//	Student stu;
//	//stu.print();//子类Student创建对象stu调用父类成员函数
//	stu.Func();//子类Student调用自身成员函数,该成员函数包含父类private成员变量与protected成员变量
//	return 0;
//}


//  父类/基类
//class Person
//{
//public:
//      //...
//protected:
//	string _name = "Peter";
//	string _sex = "woman";
//	int _age = 20;
//};
////  子类/派生类
//class Student :public Person
//{
//public:
//
//protected:
//	int _stuid=1;//学号
//};
//int main()
//{
//	Student stu;
//	Person& per = stu;//子类对象赋值给父类引用
//	return 0;
//}

//父类公有
//class Person
//{
//public:
//	string _name = "Peter";
//	int _age = 20;
//};
//// 子类保护继承/私有继承
//class Student :protected Person
//{
//public:
//
//protected:
//	int _stuid = 1;//学号
//};



//int main()
//{
//	Student stu;
//	Person& per = stu;//子类对象赋值给父类引用
//	return 0;
//}


//int main()
//{
//	//d1为double类型,j为int类型，发生类型转换时,会产生临时变量,临时变量具有常属性,这里也会导致权限的放大，为啥不报错？
//	double d1 = 3.14;
//	int j = d1;
//
//	double d2 = 1.2;
//	//int& i = d2; //err
//	const int& i = d2;
//
//	return 0;
//}


//class Person
//{
//protected:
//	string _name = "Peter";
//	int _num = 123456;//身份证号
//};
//class Student :public Person
//{
//public:
//	void Print()
//	{
//		cout << "姓名:" << _name << endl;
//		cout << "学号:" << _num << endl;
//		cout << "身份证号:" << _num << endl;//这种方式是不能输出父类中的_num的信息的
//		cout << "身份证号:" << Person::_num << endl;//必须以父类::父类成员的方式访问同名的父类的成员变量
//	}
//protected:
//	int _num= 1;//学号
//};
//int main()
//{
//	Student s;
//	s.Print();
//	return 0;
//}

//class A
//{
//public:
//	void Func()
//	{
//		cout << "调用A类的Func()函数" << endl;
//	}
//};
//class B :public A
//{
//public:
//	void Func(int n)
//	{
//		cout << "调用B类的Func()函数" << endl;
//	}
//};
//int main()
//{
//	B b;
//	//调用B类(子类)的Func()函数
//	b.Func(10);
//
//	//继承体系的不同作用域中,同名函数构成隐藏,无法以如下方式调用父类成员函数
//	//b.Func();
//
//	//只有通过父类::父类成员的方式才能调用构成隐藏关系的父类成员函数
//	b.A::Func();
//	return 0;
//}

//class Date
//{
//public:
//	//构造函数函数名与类名相同且无返回值
//	//有参构造
//	Date(int year,int month,int day)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "调用构造函数" << endl;
//	}
//private:
//	int _year = 2024;
//	int _month = 3;
//	int _day = 1;
//};
//int main()
//{
//	//显式调用
//	Date d(2024, 3, 15);
//	return 0;
//}



//class A
//{
//public:
//	~A()
//	{
//		cout << "调用A类的析构函数" << endl;
//	}
//private:
//	int _a = 1;
//};
//
//class B
//{
//public:
//	~B()
//	{
//		cout << "调用B类的析构函数" << endl;
//	}
//private:
//	int _b = 2;
//};
//
//class C
//{
//public:
//	~C()
//	{
//		cout << "调用C类的析构函数" << endl;
//	}
//private:
//	int _c = 3;
//};
//class D
//{
//public:
//	~D()
//	{
//		cout << "调用D类的析构函数" << endl;
//	}
//private:
//	int _d = 4;
//};
//
//C c;
//int main()
//{
//	A a;
//	B b;
//	static D d;
//	return 0;
//}

//class Test
//{
//public:
//	Test()
//	{
//		cout << "调用构造函数" << endl;
//		//_a = new int[10];//堆区申请空间
//	}
//	~Test()
//	{
//		cout << "调用析构函数" << endl;
//		//delete[] _a;//释放堆区开辟的空间
//	}
//private:
//	int* _a;
//};
//
//int main()
//{
//	Test t;
//	t.~Test();
//	return 0;
//}

//子类构造函数
//class Person
//{
//public:
//	//父类不存在默认构造函数(无参构造、全缺省构造、编译器自动生成的构造)
//	//父类显示实现构造函数,编译器不再自动生成
//	Person(const char* name)
//		: _name(name)
//	{}
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name = "Peter", int id = 10)
//		//: _name(name) (×)非法的成员初始化
//		//: person:: _name(name) (×) 子类成员隐藏才需要指定父类的作用域
//		//子类中的父类成员必须调用父类的构造函数初始化
//
//		: Person (name)//父类成员显示调用父类构造函数的规定写法
//		, _stuid(id)
//	{}
//protected:
//	int _stuid; //子类自身成员
//};
//
//int main()
//{
//	Student d("Karlen", 20);
//	return 0;
//}


//class A
//{
//public:
//	A(int a=0)
//	{
//		cout << "调用构造函数" << endl;
//		_a = a;
//	}
//	~A()
//	{
//		cout << "调用析构函数" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A(10);//匿名对象
//	//利用无参构造函数生成了一个A类的匿名对象,执行完此处代码
//	//由于外部没有接收此匿名对象的变量,此匿名对象自动被析构
//	A a(10);//有名对象
//	return 0;
//}

//class Person
//{
//public:
//	//父类默认构造函数
//	Person(const char* name="Linda")
//		: _name(name)
//	{
//	}
//protected:
//	string _name; //姓名
//};
//
//class Student : public Person
//{
//public:
//	//写法一
//	Student(const char* name = "Peter", int id = 10)
//		:_stuid(id)
//		, Person(name)//父类成员显示调用父类构造函数的规定写法
//	{}
//   //写法二
//    Student(int id)
//	:_stuid(id)
//	{}
//protected:
//	int _stuid; //子类自身成员
//};
//
//int main()
//{
//	Student d1("Karlen", 20);
//	Student d2(10);
//	return 0;
//}
//class Time
//{
//private:
//	int _hour;
//	int _minute;
//	int _second;
//public:
//	//显示定义构造函数
//	Time()
//		:_hour(0)
//		, _minute(0)
//		, _second(0)
//	{
//		cout << _hour << "-" << _minute << "-" << _second << endl;
//	}
//};
//
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _t;
//public:
//	Date(int year,int month,int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//
//	}
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//};
//int main()
//{
//	Date d(2023, 10, 1);
//	d.print();
//	return 0;
//}

//namespace str
//{
//	class string
//	{
//	public:
//		string(const char* str)
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//	private:
//		int _size;
//		int _capacity;
//		char* _str;
//	};
//	class Person
//	{
//	public:
//		Person(const char* name)
//			:_name(name)
//		{
//		}
//	private:
//		string _name;
//	};
//
//}
//
//int main()
//{
//	str::Person p("Linda");
//	return 0;
//}

////子类拷贝构造函数
//class Person
//{
//public:
//	Person(const char* name)
//		:_name(name)
//	{}
//	//父类拷贝构造函数显示实现
//	//拷贝构造函数也是构造函数,父类显示写出,父类无默认构造函数可用，因此子类必须在初始化列表位置显示调用
//	Person(const Person& p)//p为person类对象,p引用切片对象
//		:_name(p._name)
//	{}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
//protected:
//	string _name;//姓名
//};
//
//class Student : public Person
//{
//public:
//	//子类构造函数
//	Student(const char* name,int id)
//		:_stuid(id)
//		, Person(name)
//	{}
//	//子类拷贝构造函数(假设涉及资源申请)
//	Student(const Student& stu)
//		: _stuid(stu._stuid)
//		, Person(stu)//stu为student类对象,显示调用父类拷贝构造,传参为赋值兼容原则
//	{
//	}
//protected:
//	//string _name //继承于父类的子类成员---自定义类型
//	int _stuid; //子类自身成员---内置类型
//};
//
//int main()
//{
//	Student d1("karlen", 20);
//	Student d2(d1);
//
//	return 0;
//}

////子类赋值运算符重载函数
//class Person
//{
//public:
//	//父类的构造函数
//	Person(const char* name)
//		:_name(name)
//	{}
//	//父类赋值运算符重载显示实现
//	Person& operator=(const Person& p)
//	{
//		if (this != &p)
//		{
//			_name = p._name;
//		}
//		return *this;
//	}
//protected:
//	string _name;//姓名
//};
//
//class Student : public Person
//{
//public:
//	//子类构造函数
//	Student(const char* name,int id)
//		:_stuid(id)
//		, Person(name)
//	{}
//
//	//子类的赋值运算符重载(假设涉及资源申请)
//	Student& operator=(const Student& stu)
//	{
//		if (this != &stu)
//		{
//			//父类成员切片传入父类的赋值运算符重载函数
//			Person::operator=(stu);//继承体系中只要函数名相同就构成隐藏关系,访问父类成员方式 父类::父类成员
//
//			//子类成员根据是否涉及资源申请实现深浅拷贝
//			_stuid = stu._stuid;
//		}
//		return *this;
//	}
//protected:
//	int _stuid;
//};
//
//int main()
//{ 
//	Student d1("karlen", 20);
//	Student d2("Linda", 10);
//	d1 = d2;
//    return 0;
//}

//class Person
//{
//public:
//	Person()
//	{
//		cout << "调用父类构造函数" << endl;
//	}
//protected:
//	string _name="karlen";
//};
//
//class Student : public Person
//{
//public:
//	//子类构造函数
//	Student()
//	{
//		cout << "调用子类构造函数" << endl;
//	}
//protected:
//	int _stuid=10;
//};
//int main()
//{
//	Student d1;
//	return 0;
//}


//子类析构函数

class Person
{
public:
	Person(const char* name)
		:_name(name)
	{}
	//父类析构函数
	~Person()
	{
		cout << "调用父类的析构函数" << endl;
	}
protected:
	string _name;
};
class Student : public Person
{
public:
	//子类构造函数
	Student(const char* name, int id)
		:_stuid(id)
		, Person(name)
	{}
	//子类析构函数
	~Student()
	{
		cout << "调用子类的析构函数" << endl; 
		//父类析构函数的显示调用方式
		Person::~Person();
		//析构函数的名字会被编译器统一处理为destructor()
		//子类的析构函数和父类的析构函数之间构成隐藏,只能通过父类::父类成员的方式调用
	}
protected:
	int _stuid; 
};
int main()
{
	Student d1("karlen", 20);
	return 0;
}