# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>

//int RemoveElement(int* nums, int length, int val)
//{
//	int* tmp = (int*)malloc(length*sizeof(int));
//	int count = 0;
//	for (int start = 0; start< length; start++)
//	{
//		if (nums[start] != val)
//		{
//			*tmp = nums[start];
//			tmp++;
//			count++;
//		}
//		else
//		{
//			start++;
//		}
//	}
//	return count;
//	free(tmp);
//	tmp = NULL;
//}

//int RemoveElement(int* nums, int numsSize, int val)
//{
//	int src = 0;
//	int dest = 0;
//	while (src<numsSize)
//	{
//		if (nums[src] != val)
//		{
//			nums[dest] = nums[src];
//			dest++;
//			src++;
//		}
//		else
//		{
//			src++;
//		}
//	}
//	return dest;
//}
//
//int main()
//{
//	int nums[] = { 0, 1, 2, 2, 3, 0, 4, 2 };
//	int val = 0;
//	int sz = sizeof(nums) / sizeof(nums[0]);
//	scanf("%d", &val);
//	int ret = RemoveElement(nums, sz, val);
//	printf("ret=%d", ret);
//	return 0;
//}


//int removeDuplicates(int* nums, int numsSize)
//{
//	if (numsSize == 0)
//	{
//		return 0;
//	}
//	int fast = 1, slow = 1;
//	while (fast < numsSize)
//	{
//		if (nums[fast] != nums[fast - 1])
//		{
//			nums[slow] = nums[fast];
//			++slow;
//		}
//		++fast;
//	}
//	return slow;
//}

//void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n)
//{
//	int end1 = m - 1;
//	int end2 = n - 1;
//	int end = m + n - 1;
//	while ((end1 >= 0) && (end2 >= 0))
//	{
//		if (nums2[end2]>nums1[end1])
//		{
//			nums1[end] = nums2[end2];
//			end2--;
//			end--;
//		}
//		else
//		{
//			nums1[end] = nums1[end1];
//			end1--;
//			end--;
//		}
//	}
//	while (end2 >= 0)
//	{
//		nums1[end] = nums2[end2];
//		end--;
//		end2--;
//	}
//}

////数据结构之单链表
typedef int SLTDatatype;

typedef struct SLTNode
{
	SLTDatatype data;
	struct SLTNode* next;
}SLTNode;

void PrintSList(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
int main()
{
	SLTNode* p1 = (SLTNode*)malloc(sizeof(SLTNode));
	p1->data = 10;

	SLTNode* p2 = (SLTNode*)malloc(sizeof(SLTNode));
	p2->data = 20;

	SLTNode* p3 = (SLTNode*)malloc(sizeof(SLTNode));
	p3->data = 30;

	p1->next = p2;
	p2->next = p3;
	p3->next = NULL;

	PrintSList(p1);
	return 0;
}       