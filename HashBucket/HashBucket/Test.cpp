# define  _CRT_SECURE_NO_WARNINGS 1

# include "HashBucket.h"

void TestHT1()
{
	HashBucket::HashTable<int, int> ht;
	int a[] = { 1, 4, 24, 34, 7, 44, 17, 37 };
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}

	ht.Insert(make_pair(5, 5));
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(25, 25));
	ht.Insert(make_pair(35, 35));
	ht.Erase(5);
	ht.Erase(15);
	ht.Erase(25);
	ht.Erase(35);


}

int main()
{
    TestHT1();
	return 0;
}


