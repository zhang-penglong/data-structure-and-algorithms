# define  _CRT_SECURE_NO_WARNINGS 1

# include "Seqlist.h"

//初始化顺序表
void InitSeqlist(Seqlist* ps)
{
	assert(ps != NULL);
	//开辟一定容量的空间 (*ps).nums成员类型为SLDataType*，malloc返回值类型为void*,所以发生强转
	ps->nums = (SLDataType*)malloc(4 * sizeof(SLDataType*));
	//判断空间开辟是否成功
	if (ps->nums == NULL)
	{
		perror("malloc");
		exit(-1);
		//exit()函数 exit(0)表示正常退出，exit(x),x不为0表示异常退出，终止正在执行的进程;
	}
	ps->size = 0;
	ps->capacity = 4;
}

//销毁顺序表
void DestroySeqlist(Seqlist* ps)
{
	assert(ps != NULL);
	free(ps->nums);
	ps->nums = NULL;
	ps->size = 0;
	ps->capacity = 0;
}
void CheckCapacity(Seqlist* ps)
{
	assert(ps != NULL);
	//首先应该判断顺序表是否已满,若空间已满，进行扩容;
	//当ps->size=ps->capacity说明空间已满
	if (ps->capacity == ps->size)
	{
		//用realloc()函数进行扩容，出现两种情况 1.本地扩容 2.异地扩容
		//必须新建变量接收扩容后的地址变量;
		SLDataType* tmp = (SLDataType*)realloc(ps->nums, (ps->capacity) * 2 * sizeof(SLDataType));
		if (tmp == NULL)
		{
			perror("realloc failed");
			exit(-1);
		}
		ps->capacity = (ps->capacity) * 2;
		ps->nums = tmp;
	}
}

//顺序表尾插元素
void Seqlistpushback(Seqlist* ps, SLDataType x)
{
	assert(ps != NULL);
	////首先应该判断顺序表是否已满,若空间已满，进行扩容;
	////当ps->size=ps->capacity说明空间已满
	//if (ps->capacity == ps->size)
	//{
	//	//用realloc()函数进行扩容，出现两种情况 1.同地扩容 2.异地扩容
	//	//必须新建变量接收扩容后的地址变量;
	//	SLDataType* tmp = (SLDataType*)realloc(ps->nums, (ps->capacity) * 2 * sizeof(SLDataType));
	//	if (tmp == NULL)
	//	{
	//		perror("realloc failed");
	//		exit(-1);
	//	}
	//	ps->capacity = (ps->capacity) * 2;
	//	ps->nums = tmp;
	//}
	//扩容成功，尾插元素
	//总共有ps->size个有效数据,数组下标范围为0到(ps->size)-1;
	CheckCapacity(ps);
	ps->nums[ps->size] = x;
	ps->size++;

}

//显示顺序表
void printSeqlist(Seqlist* ps)
{
	assert(ps != NULL);
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->nums[i]);
	}
	printf("\n");
}


//顺序表尾删元素
void Seqlistpopback(Seqlist* ps)
{
	assert(ps != NULL);
   //首先检查顺序表中是否可以有元素删除,若不做检查，可能导致指针越界访问;

  //检查方式一
	/*if (ps->size == 0)
	{
		return;
	}*/
  //检查方式二
	assert(ps->size > 0);
	//尾删元素

	ps->size--;

}

//顺序表头插元素

void Seqlistpushfront(Seqlist* ps, SLDataType x)
{
	assert(ps != NULL);
	//检查空间是否足够,不够进行扩容
	//当ps->size=ps->capacity说明空间已满
	//if (ps->capacity == ps->size)
	//{
	//	//用realloc()函数进行扩容，出现两种情况 1.同地扩容 2.异地扩容
	//	//必须新建变量接收扩容后的地址变量;
	//	SLDataType* tmp = (SLDataType*)realloc(ps->nums, (ps->capacity) * 2 * sizeof(SLDataType));
	//	if (tmp == NULL)
	//	{
	//		perror("realloc failed");
	//		exit(-1);
	//	}
	//	ps->capacity = (ps->capacity) * 2;
	//	ps->nums = tmp;
	//}
	CheckCapacity(ps);
	//从后向前依次拷贝数据，直至首元素
	int end = ps->size - 1;
	while (end >= 0)
	{
		ps->nums[end + 1] = ps->nums[end];
		--end;
	}
	//插入元素
	ps->nums[0] = x;
	ps->size++;
}

//顺序表头删元素
void Seqlistpopfront(Seqlist* ps)
{
	assert(ps != NULL);
	assert(ps->size > 0);
	int begin = 1;
	while (begin < ps->size)
	{
		ps->nums[begin - 1] = ps->nums[begin];
		++begin;
	}
	ps->size--;
	
}

//顺序表在pos位置插入x
void SeqlistInsert(Seqlist* ps, int pos, SLDataType x)
{
	assert(ps != NULL);
	assert(pos >= 0 && pos <= ps->size);
	CheckCapacity(ps);
	//从顺序表最后一个元素向前拷贝，向后移动，直到pos位置为止
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->nums[end + 1] = ps->nums[end];
		end--;
	}
	ps->nums[pos] = x;
	ps->size++;
}

//顺序表查找某个元素(查找到该元素返回下标，查找不到返回-1)
int SeqlistFind(Seqlist* ps, SLDataType x)
{
	assert(ps != NULL);
	int j = 0;
	for (j = 0; j < ps->size; j++)
	{
		if (ps->nums[j] == x)
		{
			return j;
			break;
		}
	}
	//查找不到该元素
	return -1;
}

//顺序表删除pos位置的值
void SeqlistErase(Seqlist* ps, int pos)
{
	assert(ps != NULL);
	//首先判断pos位置下标是否合法，避免数组越界访问
	assert(pos >= 0 && pos < ps->size);

	//坐标合法,从pos下一个位置从前向后拷贝
	int begin = pos + 1;
	while (begin<ps->size)
	{
		ps->nums[begin - 1] = ps->nums[begin];
		begin++;
	}

	ps->size--;

}

//头插函数改造版
void SeqlistPushfront(Seqlist* ps, SLDataType x)
{
	SeqlistInsert(ps, 0, x);
}

//尾插函数改造版
void SeqlistPushback(Seqlist* ps, SLDataType x)
{
	SeqlistInsert(ps, ps->size, x);
}

//头删函数改造版
void SeqlistPopfront(Seqlist* ps)
{
	SeqlistErase(ps, 0);
}

//尾删函数改造版
void SeqlistPopback(Seqlist* ps)
{
	SeqlistErase(ps, ps->size - 1);
}

// 顺序表修改pos位置的值
void SeqlistModify(Seqlist* ps, int pos, SLDataType x)
{

	//检查要修改某个元素的下标是否合法
	assert(pos >= 0 && pos < ps->size - 1);

	ps->nums[pos] = x;

}







