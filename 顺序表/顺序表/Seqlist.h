# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
//动态顺序表
typedef int SLDataType;
typedef struct Seqlist
{
	SLDataType* nums;//nums指向动态开辟的数组
	int size;//记录有效数据的个数
	int capacity;//存储有效数据的容量大小,单位为每个结构体的大小
}Seqlist;

//数据结构—内存中存储管理数据的结构即增删查改

//创建顺序表

//初始化顺序表
void InitSeqlist(Seqlist* ps);

//销毁顺序表
void DestroySeqlist(Seqlist* ps);

//顺序表尾插元素(画图思考),插入元素的类型为数组元素的类型

void Seqlistpushback(Seqlist* ps, SLDataType x);

//显示顺序表

void printSeqlist(Seqlist* ps);

//数据表尾删元素

void Seqlistpopback(Seqlist* ps);

//顺序表头插元素(本质为向后挪动数据)
void Seqlistpushfront(Seqlist* ps, SLDataType x);

//顺序表头删元素(从数组第二个元素开始从前向后依次拷贝)
void Seqlistpopfront(Seqlist* ps);

//顺序表在pos位置插入x
void SeqlistInsert(Seqlist* ps, int pos, SLDataType x);

//顺序表查找某个元素(查找到该元素返回下标，查找不到返回-1)
int SeqlistFind(Seqlist* ps, SLDataType x);

//顺序表删除pos位置的值
void SeqlistErase(Seqlist* ps, int pos);

//利用SeqlistInsert()函数改造头插 尾插函数
//头插函数Seqlistpushfront改造版本为SeqlistPushfront()函数
void SeqlistPushfront(Seqlist* ps, SLDataType x);

//尾插函数Seqlistpushback改造版本为SeqlistPushback()函数
void SeqlistPushback(Seqlist* ps, SLDataType x);


//利用SeqlistErase()函数改造头删 尾删函数
//头删函数Seqlistpopfront改造版本为SeqlistPopfront()函数

void SeqlistPopfront(Seqlist* ps);

//尾删函数Seqlistpopback改造版本为SeqlistPopback()函数
void SeqlistPopback(Seqlist* ps);

//顺序表修改pos位置的值

void SeqlistModify(Seqlist* ps, int pos, SLDataType x);

