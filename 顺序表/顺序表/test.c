﻿# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>

//静态顺序表

//# define N 100
//typedef int SLDataType;
//struct Seqlist
//{
//	SLDataType arr[N];//数组存储数据
//	size_t size;//记录有效数据的个数
//};

# include "Seqlist.h"

//尾插元素测试
void Test1()
{
	Seqlist SL;
	InitSeqlist(&SL);
	
	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

//尾删元素测试
void Test2()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
    Seqlistpopback(&SL);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

//头插元素测试
void Test3()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushfront(&SL, 6);
	Seqlistpushfront(&SL, 6);
	Seqlistpushfront(&SL, 6);
	Seqlistpushfront(&SL, 6);
	Seqlistpushfront(&SL, 6);
	printSeqlist(&SL);

	DestroySeqlist(&SL);

}

//头删元素测试
void Test4()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	Seqlistpopfront(&SL);
	Seqlistpopfront(&SL);
	Seqlistpopfront(&SL);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

//顺序表在pos位置插入x测试
void Test5()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);

	SeqlistInsert(&SL, 3, 20);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

//顺序表查找某一元素测试
void Test6()
{

	Seqlist SL;
	InitSeqlist(&SL);
	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	int x;
	printf("请输入要查找的元素\n");
	scanf("%d", &x);

	int pos = SeqlistFind(&SL, x);
	if (pos != -1)
	{
		SeqlistInsert(&SL, pos, x * 10);
		printSeqlist(&SL);
	}
	else
	{
		printf("查找的元素不存在\n");
	}

	DestroySeqlist(&SL);
}

//顺序表删除指定位置的值测试
void Test7()
{

	Seqlist SL;
	InitSeqlist(&SL);
	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);
	int x;
	printf("请输入要删除的元素\n");
	scanf("%d", &x);

	int pos = SeqlistFind(&SL, x);
	if (pos != -1)
	{
		SeqlistErase(&SL, pos);
		printSeqlist(&SL);
	}
	else
	{
		printf("删除的元素不存在\n");
	}

	DestroySeqlist(&SL);
}

//头插函数Seqlistpushfront改造版本测试
void Test8()
{
	Seqlist SL;
	InitSeqlist(&SL);

	SeqlistPushfront(&SL, 10);
	SeqlistPushfront(&SL, 20);
	SeqlistPushfront(&SL, 30);
	SeqlistPushfront(&SL, 40);
	SeqlistPushfront(&SL, 50);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

void Test9()
{
	Seqlist SL;
	InitSeqlist(&SL);

	SeqlistPushback(&SL, 11);
	SeqlistPushback(&SL, 12);
	SeqlistPushback(&SL, 13);
	SeqlistPushback(&SL, 14);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

void Test10()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);

	SeqlistPopfront(&SL);
	printSeqlist(&SL);

	DestroySeqlist(&SL);
}

void Test11()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);

	SeqlistPopback(&SL);
	SeqlistPopback(&SL);
	printSeqlist(&SL);

	DestroySeqlist(&SL);

}

void Test12()
{
	Seqlist SL;
	InitSeqlist(&SL);

	Seqlistpushback(&SL, 1);
	Seqlistpushback(&SL, 2);
	Seqlistpushback(&SL, 3);
	Seqlistpushback(&SL, 4);
	Seqlistpushback(&SL, 5);
	printSeqlist(&SL);

	SeqlistModify(&SL, 2, 100);
	printSeqlist(&SL);

	SeqlistModify(&SL, 2, 200);
	printSeqlist(&SL);

	DestroySeqlist(&SL);

}
//测试接口一
int main()
{
	//尾插元素测试
	//Test1();
	
	//尾删元素测试
	//Test2();

	//头插元素测试
	//Test3();

	//头删元素测试
	//Test4();

	//顺序表在pos位置插入x测试
	//Test5();

	//顺序表查找某一元素测试
	//Test6();

	//顺序表删除指定位置的值测试
	//Test7();

	//头插函数改造版本SeqlistPushfront测试
	//Test8();

	//尾插函数改造版本SeqlistPushback测试
	//Test9();

	//头删函数改造版本SeqlistPopfront测试
	//Test10();

	//尾删函数改造版本SeqlistPopback测试
	//Test11();

	//顺序表修改pos位置的值测试
	Test12();
	return 0;
}







