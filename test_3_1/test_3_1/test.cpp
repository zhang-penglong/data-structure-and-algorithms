# define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;
void TestString1()
{
	string s1;//无参构造s1
	cout << s1 << endl;

	string s2("Hello Linux!");//常量字符串初始化对象s2
	cout << s2 << endl;

	string s3(s2);//string类对象s2拷贝构造s3
	cout << s3 << endl;

	string s4("Hello Windows!", 7);//字符串"Hello Windows!"的前7个字符初始化对象s4;
	cout << s4<< endl;

	string s5(10, 'A');//10个字符 'A'初始化对象;
	cout << s5 << endl;

	//string类对象s4的第0个位置开始，向后拷贝直至字符串末尾
	string s6(s4, 0);
	cout << s6 << endl;

	//string类对象s5的第3个位置开始，向后拷贝6个字符;
	string s7(s5,3,6);
	cout << s7 << endl;

}
void TestString2()
{
	string s1;
	string s2("abcdef");
	s1 = s2;//string类对象s2赋值s1
	cout << s1 << endl;

	s1 = "Linux!";//字符串常量赋值s1
	cout << s1 << endl;
	cout << s1.size() << endl;


	s2 = 'x';//单个字符赋值s2
	cout << s2 << endl;

}
void TestString3()
{
	string s1("abcdef");
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	s1.reserve(100);
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;

	string s2("Hello Linux!");
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;
	s1.reserve(10);
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;

	string s3("A candle lights others and consumes itself");
	cout << s3.size() << endl;
	cout << s3.capacity() << endl;
	s1.reserve(20);
	cout << s3.size() << endl;
	cout << s3.capacity() << endl;

}
void TestString4()
{
	string s1;
	size_t oldcapacity = s1.capacity();
	cout << s1.capacity() << endl;
	for (size_t i = 0; i < 500; i++)
	{
		s1.push_back('x');
		if (oldcapacity != s1.capacity())
		{
			cout << s1.capacity() << endl;
			oldcapacity = s1.capacity();
		}
	}
}
void TestString5()
{
	string s1("abcdef");
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	s1.resize(20);
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;

	string s2("abcdef");
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;
	s2.resize(10);
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;

	string s3("abcdef");
	cout << s3.size() << endl;
	cout << s3.capacity() << endl;
	s3.resize(4);
	cout << s3.size() << endl;
	cout << s3.capacity() << endl;
}
void TestString6()
{
	string s("Hello Linux!");
	for (auto ch : s)
	{
		cout << ch << " ";
	}
	cout << endl;
}

void Func(const string& s)
{
	//string::iterator it = s.begin(); 
	//s为const所修饰的string类对象,iterator为普通迭代器,其所指向的数据可读可写,涉及权限的放大，报错

	string::const_iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
void TestString7()
{
	string s1("Hello Linux!");
	string::reverse_iterator rit = s1.rbegin();
	while (rit != s1.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}
void TestString8()
{
	string s1("Hello");
	string s2("xxxxxx");
	cout << s1 << endl;

	// += 字符
	s1 += 'W';
	cout << s1 << endl;

	// += 常量字符串
	s1 += "Linux!";
	cout << s1 << endl;

	// += string类对象
	s1 += s2;
	cout << s1 << endl;
}
//void swap(string& s)
//{
//	std::swap(_str, s._str);
//	std::swap(_capacity, s._capacity);
//	std::swap(_size, s._size);
//}
void TestString9()
{
		string tmp("hello Linux!");
		string s1;
		// 在pos位置插入string类字符串
		// string& insert (size_t pos, const string& str);
		s1.insert(0, tmp);
		cout << s1 << endl;
		// 在pos位置插入str的子串（subpos位置开始的sublen个字符）
		// string& insert (size_t pos, const string& str, size_t subpos, size_t sublen);
		s1.insert(7, tmp, 0, 6);
		cout << s1 << endl;
		// 在pos位置插入字符指针指向的字符串
		// string& insert (size_t pos, constchar* s);
		s1.insert(2, "xxx");
		cout << s1 << endl;
		// 在pos位置插入字符指针指向的字符串的前n个字符
		// string& insert (size_t pos, const char* s, size_t n);
		s1.insert(7, "hello naiths", 8);
		cout << s1 << endl;
		// 在pos位置插入n个c字符
		// string& insert (size_t pos, size_t n, char c);
		s1.insert(0, 5, 'y');
		cout << s1 << endl;
		// 指定迭代器的位置插入n个字符c
		// void insert (iterator p, size_t n, char c);
		string::iterator it = s1.begin() + 10;
		s1.insert(it, 10, 'z');
		cout << s1 << endl;
		// 指定迭代器的位置插入字符c
		// iterator insert (iterator p, char c);
		s1.insert(s1.begin(), 'A');
		cout << s1 << endl;
		// 指定p位置插入迭代器区间的字符
		// template <class InputIterator>
		// void insert(iterator p, InputIterator first, InputIterator last);
		s1.insert(s1.begin(), tmp.begin() + 3, tmp.begin() + 8);
		cout << s1 << endl;
		// 删除pos位置开始的len个字符
		// string& erase (size_t pos = 0, size_t len = npos);
		s1.erase(2, 5);
		cout << s1 << endl;
		// 删除迭代器位置的那个字符
		// iterator erase (iterator p);
		s1.erase(s1.begin());
		cout << s1 << endl;
		// 删除迭代器区间的字符
		// iterator erase (iterator first, iterator last);
		s1.erase(s1.begin() + 2, s1.begin() + 5);
		cout << s1 << endl;
}
void TestString10()
{
	//string类对象s1中的内容通过strcpy()拷贝至字符数组cstr;
	string s1("Hello Linux!");
	char* cstr = new char[s1.size() + 1];
	strcpy(cstr, s1.c_str());
	cout << cstr << endl;
}
void TestString11()
{
	string s1("A candle lights others and consumes itself");
	string s2("others");
	string s3("linux");
	//查找string类对象s2
	size_t pos1=s1.find(s2, 5);
	cout << pos1 << endl;
	size_t pos2 = s1.find(s3);
	cout << pos2 << endl;

	//查找常量字符串
	size_t pos3=s1.find("candle");
	cout << pos3 << endl;
	size_t pos4 = s1.find("linux", 0);
	cout << pos4 << endl;

	//查找单个字符
	size_t pos5 = s1.find('l', 0);
	cout << pos5 << endl;
	size_t pos6 = s1.find('w', 0);
	cout << pos6 << endl;

}
void TestString12()
{
	string s1("Hello Linux!");
	string substr = s1.substr(6, 6);
	cout << substr << endl;
}
void TestString13()
{
	string s;
	getline(cin, s);
	cout << s << endl;
}
int main()
{
	TestString13();
	return 0;
}
