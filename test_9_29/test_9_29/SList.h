# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>

typedef int SLTDatatype;
typedef struct SLTNode
{
	SLTDatatype data;
	struct SLTNode* next;
}SLTNode;

//显示链表
void SLTprint(SLTNode* phead);

//链表开辟新节点
SLTNode* BuySLTNode(SLTDatatype x);

//链表头插节点
void SLTpushfront(SLTNode** pphead, SLTDatatype x);

//链表尾插节点
void SLTpushback(SLTNode** pphead, SLTDatatype x);

//链表尾删结点
void SLTpopback(SLTNode** pphead);

//链表头删结点
void SLTpopfront(SLTNode** pphead);

//单链表查找 根据数据查找，查找到结点返回结点的位置否则返回空指针;
void 
