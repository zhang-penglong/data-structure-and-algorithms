# define  _CRT_SECURE_NO_WARNINGS 1
# include "SList.h"

void Test1()
{
	SLTNode* p1 = (SLTNode*)malloc(sizeof(SLTNode));
	p1->data = 10;

	SLTNode* p2 = (SLTNode*)malloc(sizeof(SLTNode));
	p2->data = 20;

	SLTNode* p3 = (SLTNode*)malloc(sizeof(SLTNode));
	p3->data = 30;

	p1->next = p2;
	p2->next = p3;
	p3->next = NULL;

	SLTprint(p1);

}

void Test2()
{
	SLTNode* p1 = BuySLTNode(10);
	
	SLTNode* p2 = BuySLTNode(20);
	p1->next = p2;

	SLTNode* p3 = BuySLTNode(30);
	p2->next = p3;

	SLTprint(p1);

}

void Test3()
{
	SLTNode* plist = NULL;
	SLTpushfront(&plist, 1);
	SLTpushfront(&plist, 2);
	SLTpushfront(&plist, 3);
	SLTpushfront(&plist, 4);
	SLTpushfront(&plist, 5);
	SLTprint(plist);
}
//尾插新节点(链表不为空)
//void SLTpushback(SLTNode* phead, SLTDatatype x)
//{
//	SLTNode* newnode = BuySLTNode(x);
//	SLTNode* tail = phead;
//	if (tail->next != NULL)
//	{
//		tail = tail->next;
//	}
//	tail->next = newnode;
//}


void Test5()
{
	SLTNode* plist = NULL;
	SLTpushback(&plist, 10);
	SLTpushback(&plist, 20);
	SLTpushback(&plist, 30);
	SLTpushback(&plist, 40);
	SLTpushback(&plist, 50);
	SLTprint(plist);
}
void Test6()
{
	SLTNode* plist = NULL;
	SLTpushback(&plist, 10);
	SLTpushback(&plist, 20);
	SLTpushback(&plist, 30);
	SLTprint(plist);

	SLTpopback(&plist);
	SLTprint(plist);

	SLTpopback(&plist);
	SLTprint(plist);

	SLTpopback(&plist);
	SLTprint(plist);


}
void Test7()
{
	SLTNode* plist = NULL;
	SLTpushback(&plist, 10);
	SLTpushback(&plist, 20);
	SLTpushback(&plist, 30);
	SLTprint(plist);

	SLTpopfront(&plist);
	SLTprint(plist);

	SLTpopfront(&plist);
	SLTprint(plist);

	SLTpopfront(&plist);
	SLTprint(plist);

}
int main()
{
	//Test1();
	//Test2();
	//Test3();
	//Test5();                                                            
	//Test6();
	Test7();
	return 0;
}