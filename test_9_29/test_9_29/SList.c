# define  _CRT_SECURE_NO_WARNINGS 1
# include "SList.h"

//显示链表
void SLTprint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
//开辟新结点
 SLTNode* BuySLTNode(SLTDatatype x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));

	if (newnode == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}
 //单链表头插
 void SLTpushfront(SLTNode** pphead, SLTDatatype x)
 {
	 SLTNode* newnode = BuySLTNode(x);
	
	 newnode->next = *pphead;
	 *pphead = newnode;
 }
 //单链表尾插
 void SLTpushback(SLTNode** pphead, SLTDatatype x)
 {
	 SLTNode* newnode = BuySLTNode(x);
	 if (*pphead == NULL)
	 {
		 *pphead = newnode;
	 }
	 else
	 {
		 SLTNode* tail = *pphead;
		 while ((tail->next) != NULL)
		 {
			 tail = tail->next;
		 }
		 tail->next = newnode;
	 }
 }

//单链表尾删
 void SLTpopback(SLTNode** pphead)
 {
	 assert(*pphead != NULL);
	 //链表只有一个节点
	 if ((*pphead)->next == NULL)
	 {
		 free(*pphead);
		 *pphead = NULL;
	 }
	 //链表存在多个结点
	 else
	 {
		
		 SLTNode* tailprev = NULL;
		 SLTNode* tail = *pphead;
		 //寻找尾结点的前一个结点
		 while ((tail->next) != NULL)
		 {
			 tailprev = tail;
			 tail = tail->next;
		 }
		 //释放尾结点
		 free(tail);
		 //尾结点前一个结点的指针域置空
		 tailprev->next = NULL;
	 }
 }

//单链表头删
 void SLTpopfront(SLTNode** pphead)
 {
	 assert(*pphead != NULL);

	 SLTNode* newhead = (*pphead)->next;
	 free(*pphead);
	 *pphead = newhead;
 }