# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

//归并排序(递归)
//将待排序序列不断二分，直到每个子序列只有一个元素为止,只有一个元素,序列一定有序;
//将相邻的两个子序列合并成一个有序的序列，直到所有子序列都被合并成一个完整的序列;
void SubMergeSort(int* a, int* tmp, int begin, int end)
{
	if (begin >= end)
		return;
	int mid = (begin + end) / 2;
	//划分区间为[begin,mid]U[mid+1,end]
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	//递归终止的条件为区间只包含一个元素或者区间不存在;
	//后序遍历
	SubMergeSort(a, tmp, begin1, end1);
	SubMergeSort(a, tmp, begin2, end2);
//首先归并到tmp数组,然后拷贝到原数组;
	int index = begin;//tmp数组下标
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[index++] = a[begin1++];
		}
		else
		{
			tmp[index++] = a[begin2++];
		}
	}
	//begin1>end1与begin2>end2至少有一个发生
	while (begin1 <= end1)
	{
		tmp[index++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[index++] = a[begin2++];
	}
	//拷贝到原数组
	memcpy(a + begin, tmp + begin, (end - begin + 1)*sizeof(int));
}
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int)*n);
	if (tmp == NULL)
	{
		perror("malloc failed");
		return;
	}
	SubMergeSort(a, tmp, 0, n - 1);
}

//归并排序(非递归)
//思路:
//1.将待排序序列分成若干个长度为1的子序列，每个子序列都是有序的;
//2.将相邻的子序列进行合并，得到若干个长度为2的有序子序列(一一归并),归并到tmp数组,然后拷贝到原数组;
//3.重复步骤2，进行二二归并,四四归并,.....直到得到一个长度为n的有序序列为止;
void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int)*n);
	if (tmp == NULL)
	{
		perror("malloc failed");
		return;
	}
	
	int gap = 1;//每个区间gap个数据;
	while (gap < n)
	{
		//gap+gap归并,划分区间
		for (int i = 0; i < n; i += 2 * gap)
		{
			// [begin1,end1]U[begin2,end2] 归并
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + gap + gap - 1;
			int index = i;
			// 如果第二组不存在，这一组不用归并了
			if (begin2 >= n)
			{
				break;
			}
			// 如果第二组的右边界越界，修正一下
			if (end2 >= n)
			{
				end2 = n - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[index++] = a[begin1++];
				}
				else
				{
					tmp[index++] = a[begin2++];
				}
			}
			//begin1>end1与begin2>end2至少有一个发生
			while (begin1 <= end1)
			{
				tmp[index++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[index++] = a[begin2++];
			}
			//拷贝到原数组
			memcpy(a + i, tmp + i, (end2-i+1)*sizeof(int));
		}
		gap = 2 * gap;
	}
	free(tmp);
}
//计数排序
void CountSort(int* a, int n)
{
	//寻找最大值,最小值
	int min = a[0];
	int max = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] < min)
			min = a[i];
		
		if (a[i]>max)
			max = a[i];
	}
	//确定新数组count的大小
	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int)*range);
	if (count == NULL)
	{
		perror("malloc failed:");
		return;
	}
	//新数组全部初始化为0,方便计数
	memset(count, 0, sizeof(int)*range);
	//统计数据出现的次数
	for (int i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}
   //从前向后依次填充原数组
	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i]--)
		{
			a[j++] = i + min;
		}
	}
	free(count);
}


void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
int main()
{
	int a[] = { 5, 6, 7, 1, 3, 9, 4, 2, 5, 1, 1 };
	int n = sizeof(a) / sizeof(a[0]);
	//MergeSortNonR(a, n);
	CountSort(a, n);
	PrintArray(a, n);
	return 0;
}