# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>


typedef char STDataType;
typedef struct Stack
{
	STDataType* nums;
	int top;//记录栈顶元素在顺序栈中的位置;
	int capacity;//记录栈的大小;
}Stack;
//初始化栈
void InitStack(Stack* ps)
{
	assert(ps != NULL);
	//开辟一定量的空间
	ps->nums = (STDataType*)malloc(4 * sizeof(STDataType));
	//判断空间开辟是否成功
	if (ps->nums == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	ps->top = -1;
	ps->capacity = 4;
}

//栈的销毁
void DestroyStack(Stack* ps)
{
	assert(ps != NULL);

	free(ps->nums);
	ps->nums = NULL;

	ps->top = -1;
	ps->capacity = 0;
}

//压栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps != NULL);

	//栈满扩容
	if (ps->top == ps->capacity - 1)
	{
		STDataType* tmp = (STDataType*)realloc(ps->nums, sizeof(STDataType)* 2 * (ps->capacity));
		if (tmp == NULL)
		{
			perror("realloc failed:");
			exit(-1);
		}
		ps->nums = tmp;
		ps->capacity = 2 * (ps->capacity);
	}
	//插入数据
	ps->nums[ps->top + 1] = x;
	ps->top++;
}

//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);

	//空栈
	assert(ps->top > (-1));

	return ps->nums[ps->top];
}

//出栈
void StackPop(Stack* ps)
{
	assert(ps);
	//空栈时不可进行删除操作
	assert(ps->top > -1);

	ps->top = ps->top - 1;
}

//判空
bool StackEmpty(Stack* ps)
{
	assert(ps);

	return (ps->top == -1);
}

//获取栈中有效数据的个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->top + 1;
}
//思路:
//括号匹配只要类型匹配 顺序匹配 数量匹配即可返回true
//使用栈结构解决，左括号入栈，碰到右括号出栈，进行类型匹配;若类型不匹配，直接返回false;
//若类型匹配，继续判断下一对字符;
bool isValid(char * s)
{
	Stack st;
	InitStack(&st);
	char topval;
	while (*s != '\0')
	{
		switch (*s)
		{
		case '(':
		case '[':
		case '{':
			//入栈
			StackPush(&st, *s);
			break;
		case ')':
			//栈为空,不可进行出栈操作
			if (StackEmpty(&st))
			{
				//数量不匹配
				return false;
			}
			topval = StackTop(&st);
			StackPop(&st);
			if (topval != '(')
			{
				return false;
			}
			break;
		case ']':
			if (StackEmpty(&st))
			{
				//数量不匹配
				return false;
			}
			topval = StackTop(&st);
			StackPop(&st);
			if (topval != '[')
			{
				return false;
			}
			break;
		case '}':
			if (StackEmpty(&st))
			{

				//数量不匹配
				return false;
			}
			//出栈
			//出栈前先保存栈顶数据
			topval = StackTop(&st);
			StackPop(&st);
			if (topval != '{')
			{
				return false;
			}
			break;
		}
		s++;
	}
	//栈不为空，说明数量不匹配;
	//栈为空，数量匹配
	bool ret = StackEmpty(&st);
	return ret;
	DestroyStack(&st);

}


int main()
{
	return 0;
}












