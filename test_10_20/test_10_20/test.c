# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>


typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	int val;
	struct BinaryTreeNode* right;
}BTNode;

BTNode* BuyNode(int x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		perror("malloc fail:");
		exit(-1);
	}
	node->val = x;
	node->left = NULL;
	node->right = NULL;

	return node;
}
//前序遍历 根-->左子树-->右子树
void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	printf("%d ", root->val);
	PrevOrder(root->left);
	PrevOrder(root->right);
}

//中序遍历  左子树--根-->右子树
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->val);
	InOrder(root->right);
}

//后序遍历二叉树   左子树-->根-->右子树
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->val);
}

//二叉树结点个数
//假设思想+分治思想
//假设TreeSize()函数计算一棵数的节点个数，对于任何一棵树，皆由左子树 右子树 根组成;
//左右子树皆为树，对于一棵3结点二叉树，就是左子树的个数+右子树的个数+根
int TreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	return TreeSize(root->left) + TreeSize(root->right) + 1;
}

//二叉树叶子结点个数
//二叉链中为叶子节点的判断条件当且仅当 root->left==NULL&&root->right==NULL
//假设TreeLeafSize即求叶子结点个数，默认该函数用来计算叶子结点个数;
//对于任何一棵结点大于1的树,求该树的叶子结点可转换为求其左子树的叶子结点与右子树的叶子结点之和;
int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL&&root->right == NULL)
	{
		return 1;
	}
	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}

//二叉树第k层节点个数
//当前树的第K层的节点数 = 其左子树第K-1层节点数+其右子树第k-1层节点数
int TreeKLevel(BTNode* root, int k)
{
	assert(k > 0);
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return TreeKLevel(root->left, k - 1) + TreeKLevel(root->right, k - 1);
}

int main()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);

	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node4->left = node5;
	node4->right = node6;

	//先序遍历二叉树
	PrevOrder(node1);
	printf("\n");

	//中序遍历二叉树
	InOrder(node1);
	printf("\n");

	//后序遍历二叉树
	PostOrder(node1);
	printf("\n");

	//树中所有节点的个数
	printf("TreeSize=%d ", TreeSize(node1));
	printf("\n");

	//树中叶子结点的个数
	printf("TreeLeafSize=%d ", TreeLeafSize(node1));
	printf("\n");
	
	//树中第k层节点的个数
	int x = 0;
	printf("请输入层数:");
	scanf("%d", &x);
	printf("TreeKLevel=%d\n", TreeKLevel(node1, x));

	return 0;
}
struct TreeNode 
{
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
};
//单值二叉树
bool isUnivalTree(struct TreeNode* root)
{
	if (root == NULL)
		return true;

	if (root->left && root->left->val != root->val)
		return false;

	if (root->right && root->right->val != root->val)
		return false;

	return isUnivalTree(root->left)
		&& isUnivalTree(root->right);
}










