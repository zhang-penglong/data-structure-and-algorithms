# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;
//#include<stdio.h>
//#include <assert.h>
//int main()
//{
//	int a = 10, b = 20;
//	assert(a > b);//此处抛出Assertion failed错误
//	printf("a is greater than b\n");
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <errno.h>
//int main() 
//{
//	FILE * fp = fopen("data.txt", "r");
//	if (fp == NULL) 
//	{
//		printf("Failed to open file: %d\n", errno);
//		exit(-1);
//	}
//
//	//...
//
//	fclose(fp);
//	fp=NULL;
//	return 0;
//}

//int Division()
//{
//	int a = 0;
//	int b = 0;
//	cin >> a >> b;
//	if (b == 0)
//	{
//		throw string("除0错误");//抛出异常
//	}
//	return a / b;
//}
//
//int main()
//{
//	try
//	{
//		cout << Division() << endl;//可能会出现异常代码
//	}
//	catch (const string& errmsg)
//	{
//		cout << errmsg.c_str() << endl;//捕获异常
//	}
//	cout << "==============" << endl;
//	return 0;
//}
//int main()
//{
//
//try
//{
//	//保护代码(可能抛出异常的代码)
//}
//catch (ExceptionName1 e1)
//{
//	//处理内置类型ExceptionName1异常的代码
//}
//catch (ExceptionName2 e2)
//{
//	//处理自定义类型ExceptionName2异常的代码
//}
//catch (...)		//省略号...表示捕获任何类型的异常
//{
//	//处理任何异常的代码
//}
//
//// 若程序运行期间出现异常且未被捕获,则系统会自动调用terminate函数，使程序终止/中断 
//
//	return 0;
//}



#include <iostream>
using namespace std;

//自定义异常类
//class Exception 
//{
//public:
//	void what() 
//	{
//		cout << "自定义类型异常" << endl;
//	}
//};
//
//int division(int a, int b) 
//{
//	if (b == 0) 
//	{
//		/* 抛出异常，表示抛出的异常类型 */
//		//throw 1;				//表示抛出int类型异常
//		//throw Exception();	//使用匿名对象，表示抛出自定义异常类的异常
//		string str = "字符串类型异常";
//		throw str;				//表示抛出字符串类型异常
//	}
//	return a / b;
//}
//
//void func() 
//{
//	try 
//	{
//		division(10, 0);
//	}
//	catch (int) 
//	{		
//		//捕获int类型数据
//		cout << "int类型异常" << endl;
//	}
//	catch (Exception e) 
//	{	
//		//捕获Exception类对象
//		//通过异常类对象调用成员函数
//		e.what();
//	}
//	catch (...) 
//	{	
//		//捕获任意类型的异常（此处可捕获string类型异常）
//		//不进行处理，继续抛出
//		throw;		//继续抛出string类型异常
//	}
//}
//
//int main()
//{
//	try 
//	{
//		func();		//本例中，继续抛出string类型异常
//	}
//	catch (string)
//	{
//		cout << "处理string类型异常" << endl;
//	}
//	//若程序运行期间出现异常，且未被捕获，则系统会自动调用terminate函数，使程序会终止/中断 
//	return 0;
//}


//int division(int a, int b)
//{
//	if (b == 0)
//	{
//		throw 1;// 表示抛出int类型异常
//	}
//	return a / b;
//}
//
//void func()
//{
//	try
//	{
//		division(10, 0);
//	}
//	catch (int)
//	{
//		//捕获int类型数据
//		cout << "int类型异常" << endl;
//	}
//}
//
//int main()
//{
//	try
//	{
//		func();	
//	}
//	catch (...)
//	{
//		//捕获任意类型的异常
//		//处理异常
//		//...
//	}
//	//若程序运行期间出现异常，且未被捕获，则系统会自动调用terminate函数，使程序会终止/中断 
//	return 0;
//}

//自定义类型异常
//class Exception 
//{
//public:
//	void what() 
//	{
//		cout << "自定义类型异常" << endl;
//	}
//};
//
//int Division(int a,int b)
//{
//	if (b == 0)
//	{
//		throw string();//使用匿名对象,表示抛出string类型的异常
//	}
//	return a / b;
//}
//int main()
//{
//	try
//	{
//		cout << Division(10,5) << endl;
//	}
//	catch (int errmsg)
//	{
//		cout << "捕获int类型异常:errmsg=" << errmsg << endl;//捕获异常(未捕获)
//	}
//	catch (...)
//	{
//		//捕获任意类型的异常
//		//处理异常
//		//...
//	}
//	return 0;
//}

//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		throw "Division by zero condition!";
//	}
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int *ptr = new int[10];
//	try
//	{
//		int a, b = 0;
//		cin >> a >> b;
//		Division(a, b);
//	}
//	catch (const char* errmsg)
//	{
//		//将开辟的空间释放
//		delete[] ptr;
//		//重新抛出异常errmsg
//		throw errmsg;
//	}
//	//若没有发生异常,不会执行catch子句
//	//此处还需要释放空间
//	delete[] ptr;
//	cout << "void Func()" << endl;
//}
//
//int main()
//{
//	try 
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << "exception errmsg:" << errmsg << endl;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	cout << "===============================================" << endl;
//
//	return 0;
//}


//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		throw "Division by zero condition!";
//	}
//	else
//		return ((double)a / (double)b);
//}
//class A
//{
//public:
//	A()
//	{
//		cout << "调用构造函数" << endl;
//		_ptr1 = new int[10];
//		int x, y = 0;
//		cin >> x >> y;
//		Division(x, y);
//		_ptr2 = new int[10];
//	}
//	~A()
//	{
//		cout << "调用析构函数" << endl;
//		delete[] _ptr1;
//		delete[] _ptr2;
//	}
//private:
//	int* _ptr1 ;
//	int* _ptr2 ;
//};
//int main()
//{
//	A aa;
//	try
//	{
//		A aa;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	return 0;
//}


// 下面程序及其难看
// 智能指针解决
//void Func()
//{
//	int* p1 = new int[10];
//	int* p2, *p3;
//	try
//	{
//		p2 = new int[20];
//		try {
//			p3 = new int[30];
//		}
//		catch (...)
//		{
//			delete[] p1;
//			delete[] p2;
//			throw;
//		}
//	}
//	catch (...)
//	{
//		delete[] p1;
//		throw;
//	}
//
//	//...
//
//	delete[] p1;
//	delete[] p2;
//	delete[] p3;
//}


double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	else
		return ((double)a / (double)b);
}
//若Func()函数中抛异常,delete还能执行吗？



//void Func()
//{
//	int* p1 = new int[10];
//	//若开辟p1指向的空间失败,抛出异常,p2与p3所指向的空间也没有开辟自然也就不需要释放空间
//	//跳转到main()函数函数栈帧中捕获异常,直接结束;
//
//	int* p2 = new int[20];
//	//若开辟p2指向的空间失败,抛出异常,被迫在当前函数栈帧中捕获异常,因为必须要释放p1所指向的空间
//	//否则跳转到main()函数函数栈帧中捕获异常,导致p1所指向的空间无法释放
//
//	int* p3 = new int[30];
//	//若开辟p3指向的空间失败,抛出异常,被迫在当前函数栈帧中捕获异常,因为必须要释放p1、p2所指向的空间
//	//否则跳转到main()函数函数栈帧中捕获异常,导致p1、p2所指向的空间无法释放
//
//
//	//...
//
//	delete[] p1;
//	delete[] p2;
//	delete[] p3;
//}
//
//int main()
//{
//	try 
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << "exception errmsg:" << errmsg << endl;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	cout << "===============================================" << endl;
//	return 0;
//}

//
//void Func()
//{
//	int* p1 = new int[10];
//	int* p2 = nullptr;
//	int* p3 = nullptr;
//	try
//	{
//		p2 = new int[20];
//		try
//		{
//			p3 = new int[30];
//		}
//		catch (...)
//		{
//			delete[] p1;
//			delete[] p2;
//			//捕获什么抛出什么
//			throw;
//		}
//
//	}
//	catch (...)
//	{
//		delete[] p1;
//	}
//	//...
//
//	delete[] p1;
//	delete[] p2;
//	delete[] p3;
//}
//
//int main()
//{
//	try 
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << "exception errmsg:" << errmsg << endl;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	cout << "===============================================" << endl;
//	return 0;
//}



///* 异常的基类 */
//class BaseException 
//{
//public:
//	//基类虚函数
//	virtual void what()
//	{}
//};
//
////异常的子类1：空指针异常
//class NullPointerException : public BaseException
//{
//public:
//	//子类重写父类虚函数
//	virtual void what()
//	{
//		cout << "空指针异常" << endl;
//	}
//};
//
////异常的子类2：索引越界
//class IndexOutOfRangeException : public BaseException 
//{
//public:
//	//子类重写父类虚函数
//	virtual void what()
//	{
//		cout << "索引越界异常" << endl;
//	}
//};
//
//void Test()
//{
//	//throw NullPointerException();//抛出空指针类的匿名对象(子类对象)
//
//	throw IndexOutOfRangeException();//抛出索引越界类的匿名对象(子类对象)
//}
//
//int main() 
//{
//	try 
//	{
//		Test();
//	}
//	catch (BaseException& e)
//	{	
//		//多态使用:基类的引用类型捕获子类异常对象
//		e.what();
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	cout << "================================" << endl;
//	return 0;
//}






//父类: 总公司
class Exception
{
public:

	Exception(const char* str = nullptr, int id = 0)
		:_errmsg(str)
		, _id(id)
	{}
	virtual void what()const = 0;
protected:
	string _errmsg;//错误描述
	int _id;//错误编号
};

//子类:数据库部门
class SqlException :public Exception
{
public:
	SqlException(const char *str = nullptr, int id = 1)
		:Exception(str, id)
	{}

	virtual void what()const
	{
		cout << "error msg:" << _errmsg << endl;
		cout << "error id:" << _id << endl;
	}
};
//子类:网络部门
class HttpException :public Exception
{
public:
	HttpException(const char *str = nullptr, int id = 2)
		:Exception(str, id)
	{}

	virtual void what()const
	{
		cout << "error msg:" << _errmsg << endl;
		cout << "error id:" << _id << endl;
	}
};
//子类:缓存部门
class CacheException :public Exception
{
public:
	CacheException(const char *str = nullptr, int id = 3)
		:Exception(str, id)
	{}

	virtual void what()const{
		cout << "error msg:" << _errmsg << endl;
		cout << "error id:" << _id << endl;
	}
};

void test()
{
	//当网络连接失败,抛出网络异常
	//throw HttpException("Http Failed", 2);

	//当缓存失败时,抛出缓存异常
	//throw CacheException("Cache Failed", 3);

	//当数据库存储数据失败,抛出数据库异常
	throw SqlException("Sql Failed", 4);
}

int main()
{
	try
	{
		test();
	}
	//基类的引用类型或者指针类型捕获子类异常对象实现多态调用
	catch (const Exception& e)
	{
		e.what();
	}
	catch (...)
	{
		cout << "unknow exception" << endl;
	}
	return 0;
}





















