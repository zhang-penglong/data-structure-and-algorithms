# define  _CRT_SECURE_NO_WARNINGS 1

# include "Seqlist.h"

//初始化顺序表
void InitSeqlist(Seqlist* ps)
{
	assert(ps != NULL);
	ps->nums = (SLDatatype*)malloc(4 * sizeof(SLDatatype));
	if (ps->nums == NULL)
	{
		perror("malloc");
		exit(-1);
	}
	ps->size = 0;
	ps->capacity = 4;
}
//销毁顺序表
void DestorySeqlist(Seqlist* ps)
{
	assert(ps != NULL);
	free(ps->nums);
	ps->nums = NULL;
	ps->size = 0;
	ps->capacity = 0;
}
//显示顺序表
void PrintSeqlist(const Seqlist* ps)
{
	assert(ps != NULL);
	for (int j = 0; j < ps->size; j++)
	{
		printf("%d ", ps->nums[j]);
	}
	printf("\n");
}

void SeqlistPushback(Seqlist* ps, SLDatatype x)
{
	assert(ps != NULL);
	if (ps->capacity == ps->size)
	{
		SLDatatype* tmp = (SLDatatype*)realloc(ps->nums, 2 * (ps->capacity)*sizeof(SLDatatype));
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		ps->nums = tmp;
		ps->capacity = 2 * (ps->capacity);
	}
	ps->nums[ps->size] = x;
	ps->size++;
}

void Seqlistpopback(Seqlist* ps)
{
	assert(ps != NULL);
	//首先检查是否有元素可以删除
	/*if (ps->size == 0);
	return;*/
	
	assert(ps->size > 0);

	ps->size--;

}
void CheckCapacity(Seqlist* ps)
{
	assert(ps != NULL);
	if (ps->capacity == ps->size)
	{
		SLDatatype* tmp = (SLDatatype*)realloc(ps->nums, 2 * (ps->capacity)*sizeof(SLDatatype));
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		ps->nums = tmp;
		ps->capacity = 2 * (ps->capacity);
	}
}
void SeqlistPushfront(Seqlist* ps, SLDatatype x)
{
	CheckCapacity(ps);

	int end = ps->size - 1;
	while (end >= 0)
	{
		ps->nums[end + 1] = ps->nums[end];
		end--;
	}
	ps->nums[0] = x;
	ps->size++;

}

void SeqlistPopfront(Seqlist* ps)
{
	assert(ps != NULL);

	assert(ps->size > 0);

	int start = 0;
	while (start < ps->size-1)
	{
		ps->nums[start] = ps->nums[start + 1];
		start++;
	}
	ps->size--;
}

int FindSeqlist(const Seqlist* ps, SLDatatype x)
{
	assert(ps != NULL);
	for (int j = 0; j < ps->size; j++)
	{
		if (x == ps->nums[j])
		{
			return j;
			break;
		}
	}
	return -1;
}

void SeqlistInsert(Seqlist* ps, int pos, SLDatatype x)
{
	assert(ps != NULL);
	//首先判断坐标的合法性
	assert(pos >= 0 && pos <= ps->size);
	//判断是否需要扩容
	CheckCapacity(ps);
	//数据向后移动
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->nums[end + 1] = ps->nums[end];
		end--;
	}
	//插入元素
	ps->nums[pos] = x;
	ps->size++;
}

void SeqlistEarse(Seqlist* ps, int pos, SLDatatype x)
{
	assert(ps != NULL);
	assert(pos >= 0 && pos < (ps->size));
	assert(ps->size>0);

	int start = pos+1;
	while (start<(ps->size))
	{
		ps->nums[start-1] = ps->nums[start];
		start++;
	}

	ps->size--;
}

void SeqlistModify(Seqlist* ps, int pos, SLDatatype x)
{
	assert(ps != NULL);
	assert(pos >= 0 && pos < (ps->size));

	ps->nums[pos] = x;

}