# define  _CRT_SECURE_NO_WARNINGS 1

# include "Seqlist.h"

void Test()
{
	Seqlist SL;
	InitSeqlist(&SL);
	PrintSeqlist(&SL);
	DestorySeqlist(&SL);
}

void Test1()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 1);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}
void Test2()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 2);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 4);
	SeqlistPushback(&SL, 5);
	PrintSeqlist(&SL);

	Seqlistpopback(&SL);
	PrintSeqlist(&SL);

	Seqlistpopback(&SL);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}
void Test3()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushfront(&SL, 1);
	SeqlistPushfront(&SL, 2);
	SeqlistPushfront(&SL, 3);
	SeqlistPushfront(&SL, 4);
	SeqlistPushfront(&SL, 5);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}
void Test4()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushfront(&SL, 1);
	SeqlistPushfront(&SL, 2);
	SeqlistPushfront(&SL, 3);
	SeqlistPushfront(&SL, 4);
	SeqlistPushfront(&SL, 5);
	PrintSeqlist(&SL);

	SeqlistPopfront(&SL);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}

void Test5()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 2);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 4);
	SeqlistPushback(&SL, 5);
	PrintSeqlist(&SL);
	printf("请输入要查找的元素:> ");
	SLDatatype x = 0;
	scanf("%d", &x);
	//查找到该元素，返回下标; 查找不到,返回-1;
	int pos = FindSeqlist(&SL, x);
	if (pos != -1)
	{
		SeqlistPushback(&SL, x);
		PrintSeqlist(&SL);
	}
	else
	{
		printf("该元素查找不到\n");
	}

	DestorySeqlist(&SL);
}

void Test6()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 2);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 4);
	SeqlistPushback(&SL, 5);
	PrintSeqlist(&SL);
	printf("请输入要插入的值:> ");
	SLDatatype x = 0;
	scanf("%d", &x);
	printf("请输入插入位置的下标:> ");
	int pos = 0;
	scanf("%d", &pos);
	SeqlistInsert(&SL, pos, x);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}

void Test7()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 2);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 4);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 3);
	PrintSeqlist(&SL);
	printf("请输入要删除的值:> ");
	SLDatatype x = 0;
	scanf("%d", &x);
	printf("请输入删除位置的下标:> ");
	int pos = 0;
	scanf("%d", &pos);
	SeqlistEarse(&SL, pos, x);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}
void Test8()
{
	Seqlist SL;
	InitSeqlist(&SL);
	SeqlistPushback(&SL, 1);
	SeqlistPushback(&SL, 2);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 4);
	SeqlistPushback(&SL, 3);
	SeqlistPushback(&SL, 3);
	PrintSeqlist(&SL);
	printf("请输入要修改的值:> ");
	SLDatatype x = 0;
	scanf("%d", &x);
	printf("请输入修改位置的下标:> ");
	int pos = 0;
	scanf("%d", &pos);
	SeqlistModify(&SL, pos, x);
	PrintSeqlist(&SL);

	DestorySeqlist(&SL);
}
int main()
{
	//Test2();
	//Test3();
	//Test4();
	//Test5();
	//Test6();
	//Test7();
	Test8();
	return 0;
}

