# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>

//shift+home/end 根据需要逐个选择,home键向右，end键向左;
//创建动态顺序表
typedef int SLDatatype;
typedef struct Seqlist
{
	SLDatatype* nums;//nums指向动态开辟的空间
	int size;//记录有效数据的个数
	int capacity;//记录顺序表的容量
}Seqlist;

//顺序表的初始化
void InitSeqlist(Seqlist* ps);

//销毁顺序表
void DestorySeqlist(Seqlist* ps);

//显示顺序表中的元素
void PrintSeqlist(const Seqlist* ps);

//顺序表尾插元素
void SeqlistPushback(Seqlist* ps, SLDatatype x);

//顺序表尾删元素
void Seqlistpopback(Seqlist* ps);

//顺序表检查容量
void CheckCapacity(Seqlist* ps);

//顺序表头插元素
void SeqlistPushfront(Seqlist* ps, SLDatatype x);

//顺序表头删元素
void SeqlistPopfront(Seqlist* ps);

//顺序表查找指定元素
int FindSeqlist(const Seqlist* ps, SLDatatype x);

//顺序表在指定位置pos处插入元素
void SeqlistInsert(Seqlist* ps, int pos, SLDatatype x);

//顺序表删除指定位置的值
void SeqlistEarse(Seqlist* ps, int pos, SLDatatype x);

//顺序表修改指定位置的值
void SeqlistModify(Seqlist* ps, int pos, SLDatatype x);