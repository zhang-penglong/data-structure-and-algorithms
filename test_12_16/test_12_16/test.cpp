# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>

//class Stack
//{
//public:
//	int* a;
//	int top;
//	int capacity;
//	//初始化栈
//	void Init()
//	{
//		a = 0;  
//		top = 0;
//		capacity = 0;
//	}
//	//空栈的判断
//	bool StackEmpty(Stack* ps)
//	{
//		assert(ps);
//		return (ps->top == -1);
//	}
//}; 
//int main()
//{
//	Stack s1;
//	s1.Init();
//	//判断方式1:空栈, 若top=0,表示指向栈顶元素的下一个位置;
//	//          空栈, 若top=-1,表示指向栈顶元素的位置;
//	//栈的定义方式不同，如下判断方法可能导致出错;
//	if (s1.top==0)
//    
//   //判断方式2:采取访问成员函数进行判断，直接调用判空函数,不会出错
//	if (!s1.StackEmpty(&s1))
//	{
//
//	}
//	//如上就是成员变量私有,
//	return 0;
//}
//class Stack
//{
//	int* a;
//	int top;
//	int capacity;
//public:
//	//初始化栈
//	void Init()
//	{
//		a = 0;
//		top = 0;
//		capacity = 0;
//	}
//	//空栈的判断
//	bool StackEmpty()
//	{
//		return (top == -1);
//	}
//};
//# include "stack.h"
//int main()
//{
//	Stack s1;
//	s1.Init();
//	return 0;
//}
# include <iostream>
using namespace std;
//class Date
//{
//public:
//	void Init(int year,int month,int day)
//	{
//		_year = year;
//	}
//	int _year;
//	int _month;
//	int _day;
//};
//class A
//{
//public:
//	void f1()
//	{}
//private:
//	int _a;
//};
//class B
//{
//public:
//	void f2() {}
//};
//class C
//{
//
//};
//int main()
//{
//	Date d;
//	cout << sizeof(d) << endl;
//	A a;
//	cout << sizeof(a) << endl;
//	B b;
//	cout << sizeof(b) << endl;
//	C c;
//	cout << sizeof(b) << endl;
//	return 0;
//}
class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	/*void Print(Date* this)
	{
		cout << this->_year << "-" << this->_month << "-" << this->_day << endl;
	}*/
	void Print()
	{
		cout <<_year << "-" << _month << "-" <<_day << endl;
	}
private:
	int _year;//年
	int _month;//月
	int _day;//日
};
int main()
{
	Date d1, d2;
	d1.Init(2023, 12, 17);
	d2.Init(2023, 12, 16);
	d1.Print();
	d2.Print();


	return 0;
}