# define  _CRT_SECURE_NO_WARNINGS 1
# include "stack.h"

//stack.cpp文件定义成员函数的实现;
//初始化栈
void Stack::Init()
{
	a = 0;
	top = 0;
	capacity = 0;
}
//空栈的判断
bool Stack::StackEmpty()
{
	return (top == -1);
}