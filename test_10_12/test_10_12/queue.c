# include "queue.h"
//初始化队列
void InitQueue(Queue* ps)
{
	assert(ps != NULL);

	ps->head = NULL;
	ps->tail = NULL;
	ps->size = 0;
}

//销毁队列
void DestroyQueue(Queue* ps)
{
	assert(ps != NULL);
	QueueNode* cur = ps->head;
	while (cur != NULL)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}

	ps->head = NULL;
	ps->tail = NULL;
	ps->size = 0;
}

//队尾入队列(尾插)
void QueuePush(Queue* ps, QDataType x)
{
	assert(ps != NULL);
	//创建新队列结点
	QueueNode* newnode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newnode == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	//入队列(尾插)
	//空队列时通过赋值完成尾插，队列中存在队列结点,按照逻辑关系完成连接;
	if (ps->tail == NULL)
	{
		ps->head = ps->tail = newnode;
	}
	else
	{
		ps->tail->next = newnode;
		ps->tail = newnode;
	}
	ps->size++;
}

//队头出队列(头删)
void QueuePop(Queue* ps)
{
	assert(ps != NULL);

	//空队列不可删
	assert(ps->tail != NULL);

	//出队列
	//队列中只有一个队列结点
	if (ps->head->next == NULL)
	{
		free(ps->head);
		ps->head = ps->tail = NULL;
	}
	//队列中有两个以上队列结点
	else
	{
		QueueNode* next = ps->head->next;
		free(ps->head);
		ps->head = next;
	}
	ps->size--;
}


//获取队列头部元素
QDataType QueueFront(Queue* ps)
{
	assert(ps != NULL);
	//队列不为空
	assert(ps->head != NULL);

	return ps->head->data;
}

//获取队列尾部元素
QDataType QueueBack(Queue* ps)
{
	assert(ps != NULL);
	//队列不为空
	assert(ps->tail != NULL);

	return ps->tail->data;
}

//判断队列是否为空
bool QueueEmpty(Queue* ps)
{
	assert(ps);
	if (ps->tail == NULL)
	{
		return true;
	}
	return false;
}

//获取队列长度
int QueueLength(Queue* ps)
{
	assert(ps);

	return ps->size;
}






