
# include<stdio.h>
# include<assert.h>
# include <stdlib.h>
# include <stdbool.h>

typedef int QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;

typedef struct Queue
{
	QueueNode* head;//队头指针
	QueueNode* tail;//队尾指针
	int size;//队列长度
}Queue;


//初始化队列
void InitQueue(Queue* ps);

//销毁队列
void DestroyQueue(Queue* ps);

//队尾入队列(尾插)
void QueuePush(Queue* ps, QDataType x);

//队头出队列(头删)
void QueuePop(Queue* ps);

//获取队列头部元素
QDataType QueueFront(Queue* ps);

//获取队列尾部元素
QDataType QueueBack(Queue* ps);

//判断队列是否为空
bool QueueEmpty(Queue* ps);

//获取队列长度
int QueueLength(Queue* ps);






