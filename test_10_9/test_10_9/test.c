# define  _CRT_SECURE_NO_WARNINGS 1

# include "stack.h"

int main()
{
	LinkStack* plist=InitStack();
	StackPush(plist, 1);
	StackPush(plist, 2);
	StackPush(plist, 3);
	StackPush(plist, 4);
	StackPush(plist, 5);
	printf("count=%d\n", StackSize(plist));
	while (!StackEmpty(plist))
	{
		printf("%d ", LinkStTop(plist));
		StackPop(plist);
	}
	printf("\n");
	printf("count=%d\n", StackSize(plist));
	DestroyStack(plist);
	return 0;
}