# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>

typedef int LSDataType;
typedef struct LinkStack
{
	LSDataType data;//数据域
	struct LinkStack* next;//指针域
}LinkStack;

//初始化空栈
LinkStack* InitStack();

//销毁栈
void DestroyStack(LinkStack* phead);

//压栈
void StackPush(LinkStack* phead, LSDataType x);

//获取栈顶元素
LSDataType LinkStTop(LinkStack* phead);

//出栈
void StackPop(LinkStack* phead);

//判断栈是否为空
bool StackEmpty(LinkStack* phead);

//获取栈中有效数据的个数
int StackSize(LinkStack* phead);