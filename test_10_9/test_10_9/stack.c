# define  _CRT_SECURE_NO_WARNINGS 1
# include "stack.h"

//初始化空栈
LinkStack* InitStack()
{
	LinkStack* phead = (LinkStack*)malloc(sizeof(LinkStack));
	if (phead == NULL)
	{
		perror("malloc fail:");
		exit(-1);
	}
	phead->next = NULL;
	return phead;
}

//销毁栈
//先保存当前结点的下一个结点，然后释放当前结点;
void DestroyStack(LinkStack* phead)
{
	assert(phead);
	LinkStack* cur = phead;
	LinkStack* curnext = cur->next;
	while (curnext != NULL)
	{
		free(cur);
		cur = curnext;
		curnext = cur->next;
	}
	//此时cur指向尾结点
	free(cur);
	cur = NULL;
}

void StackPush(LinkStack* phead, LSDataType x)
{
	assert(phead);
	//开辟新结点
	LinkStack* newnode = malloc(sizeof(LinkStack));
	if (newnode==NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	newnode->data = x;
	//头插=压栈
	newnode->next = phead->next;
	phead->next = newnode;
}

//获取栈顶元素
LSDataType LinkStTop(LinkStack* phead)
{
	assert(phead);
	//空栈
	assert(phead->next != NULL);

	return phead->next->data;
}

//出栈
void StackPop(LinkStack* phead)
{
	assert(phead);
    //空栈不可删除
	assert(phead->next != NULL);

	LinkStack* Second = phead->next->next;
	free(phead->next);//释放首结点
	phead->next = Second;
}

//判断栈是否为空
bool StackEmpty(LinkStack* phead)
{
	assert(phead);

	return (phead->next == NULL);
}

//获取栈中有效数据的个数
int StackSize(LinkStack* phead)
{
	assert(phead);
	int count = 0;
	LinkStack* cur = phead->next;
	while (cur != NULL)
	{
		count++;
		cur = cur->next;
	}
	return count;
}