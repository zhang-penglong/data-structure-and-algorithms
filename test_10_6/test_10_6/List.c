# define  _CRT_SECURE_NO_WARNINGS 1
# include "List.h"

//创建链表结点,返回链表结点地址
ListNode* BuyListNode(LTDataType x)
{
	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
	if (newnode == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}

	newnode->data = x;
	newnode->next = NULL;
	newnode->prev = NULL;

	return newnode;
}
//初始化链表(空链表)
ListNode* ListInit()
{
	//创建哨兵位结点
	ListNode* head = BuyListNode(0);//0不是有效数据

	//初始化哨兵位结点的指针域
	head->next = head;
	head->prev = head;

	return head;
}

//销毁链表,包含哨兵位结点
void DestoryList(ListNode* phead)
{
	assert(phead);

	//创建寻址指针
	ListNode* cur = phead;
	//断开循环链表
	phead->prev->next = NULL;
	while (cur != NULL)
	{
		//记录下一结点地址
		ListNode* next = cur->next;
        //释放当前结点
		free(cur);
		//寻找下一节点
		cur = next;
	}
	return;
}

//显示链表
void PrintList(ListNode* phead)
{
	assert(phead != NULL);

	ListNode* cur = phead->next;

	printf("phead<==>");
	while (cur != phead)
	{
		printf("%d<==>", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//链表尾插
void ListPushBack(ListNode* phead, LTDataType x)
{
	assert(phead);
    
	//寻找尾结点
	ListNode* tail = phead->prev;
    //创建新结点
	ListNode* newnode = BuyListNode(x);

   //尾插
	newnode->prev = tail;
	tail->next = newnode;

	newnode->next = phead;
	phead->prev = newnode;
}

//链表头插
void ListPushFront(ListNode* phead, LTDataType x)
{
	assert(phead);

	//保存原先的首节点
	ListNode* firstnode = phead->next;
	//创建新结点
	ListNode* newnode = BuyListNode(x);

	//头插
	newnode->prev = phead;
	phead->next = newnode;

	newnode->next = firstnode;
	firstnode->prev = newnode;
}

//链表尾删
void ListPopBack(ListNode* phead)
{
	assert(phead);

	//链表中只剩哨兵位的情况
	assert(phead->next != phead);

	//查找尾结点
	ListNode* tail = phead->prev;
	//保存尾结点的上一节点
	ListNode* tailprev = tail->prev;

   //尾删
	free(tail);
   //建立链接关系
	tailprev->next = phead;
	phead->prev = tailprev;

}

//链表头删
void ListPopFront(ListNode* phead)
{
	assert(phead);
	//只剩哨兵位,不再头删
	assert(phead->next != phead);

	//保存原先的首节点
	ListNode* head = phead->next;

	//保存首结点的下一节点
	ListNode* headnext = phead->next->next;

	//头删
	free(head);
	//建立链接关系
	headnext->prev = phead;
	phead->next = headnext;

}
ListNode* ListFind(ListNode* phead, LTDataType x)
{
	assert(phead);

	//创建遍历指针
	ListNode* cur = phead->next;

	//遍历链表
	while (cur != phead)
	{
		if ((cur->data) == x)
		{
			//找到返回下标
			return cur;
		}
		cur = cur->next;
	}
	//没找到返回空指针
	return NULL;
}
//双向链表在pos位置前插入结点
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos != NULL);

	//创建新结点
	ListNode* newnode = BuyListNode(x);
	//保存pos位置的前一个结点
	ListNode* posprev = pos->prev;

	//前插
	newnode->prev = posprev;
	posprev->next = newnode;
	
	newnode->next = pos;
	pos->prev = newnode;
}
//双向链表删除pos位置
void ListEarse(ListNode* pos)
{
	assert(pos);

	//保存pos位置处的前一个和后一个结点;
	ListNode* posprev = pos->prev;
	ListNode* posnext = pos->next;
	//删除pos位置结点
	free(pos);

	//建立前后节点的链接关系
	posprev->next = posnext;
	posnext->prev = posprev;

}

void Listpushback(ListNode* phead, LTDataType x)
{
	assert(phead);
	ListInsert(phead, x);
}
void Listpushfront(ListNode* phead, LTDataType x)
{
	assert(phead);
	ListInsert(phead->next, x);
}
void Listpopback(ListNode* phead)
{
	assert(phead);

	//链表中只剩哨兵位的情况
	assert(phead->next != phead);

	ListEarse(phead->prev);
}
void Listpopfront(ListNode* phead)
{
	assert(phead);
	//只剩哨兵位,不再头删
	assert(phead->next != phead);

	ListEarse(phead->next);
}


int ListLength(ListNode* phead)
{
	assert(phead);

	int size = 0;
	ListNode* cur = phead->next;
	while (cur != phead)
	{
		++size;
		cur = cur->next;
	}
	return size;
}
