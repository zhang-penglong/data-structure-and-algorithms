# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>

typedef int LTDataType;
typedef struct ListNode
{
	struct ListNode* prev;
	LTDataType data;
	struct ListNode* next;
}ListNode;

//创建链表结点,返回链表头结点
ListNode* BuyListNode(LTDataType x);

//初始化链表
ListNode* ListInit();

//销毁链表
void DestoryList(ListNode* phead);

//打印链表
void PrintList(ListNode* phead);

//双向链表尾插
void ListPushBack(ListNode* phead, LTDataType x);

//双向链表头插
void ListPushFront(ListNode* phead, LTDataType x);

//双向链表尾删
void ListPopBack(ListNode* phead);

//双向链表头删
void ListPopFront(ListNode* phead);

//双向链表查找
ListNode* ListFind(ListNode* phead, LTDataType x);

//双向链表pos位置前插
void ListInsert(ListNode* pos, LTDataType x);

//双向链表删除pos位置
void ListEarse(ListNode* pos);

//双向链表长度计算
int ListLength(ListNode* phead);