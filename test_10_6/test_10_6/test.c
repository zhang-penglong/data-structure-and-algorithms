# define  _CRT_SECURE_NO_WARNINGS 1

# include "List.h"

void Test1()
{
	ListNode* plist=ListInit();
	
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	PrintList(plist);
}
void Test2()
{
	ListNode* plist = ListInit();
	ListPushFront(plist, 10);
	ListPushFront(plist, 20);
	ListPushFront(plist, 30);
	ListPushFront(plist, 40);
	ListPushFront(plist, 50);
	PrintList(plist);

}
void Test3()
{
	ListNode* plist = ListInit();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

	ListPopBack(plist);
	PrintList(plist);

}
void Test4()
{
	ListNode* plist = ListInit();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);

	ListPopFront(plist);
	PrintList(plist);
}

void Test5()
{
	ListNode* plist = ListInit();
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);

	int x = 0;
	printf("请输入查找的数值:");
	scanf("%d", &x);
	ListNode* pos = ListFind(plist, x);
	if (pos == NULL)
	{
		printf("要查找的值不存在\n");
		return;
	}
	//在查找到数值前插入100
	ListInsert(pos, 100);
	PrintList(plist);

}
void Test6()
{
	ListNode* plist = ListInit();
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	PrintList(plist);

	int x = 0;
	printf("请输入删除的数值:");
	scanf("%d", &x);
	ListNode* pos = ListFind(plist, x);
	if (pos == NULL)
	{
		printf("要删除的值不存在\n");
		return;
	}
	
	ListEarse(pos);
	PrintList(plist);

}
int main()
{
	Test6();
	return 0;
}