# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;


void Fun(int& x) 
{ 
	cout << "左值引用" << endl; 
}
void Fun(const int& x) 
{ 
	cout << "const 左值引用" << endl;
}
void Fun(int&& x) 
{ 
	cout << "右值引用" << endl; 
}
void Fun(const int&& x) 
{ 
	cout << "const 右值引用" << endl; 
}

// 模板中的&&不代表右值引用，而是万能引用，其既能接收左值又能接收右值。
// 模板的万能引用只是提供了能够接收同时接收左值引用和右值引用的能力，
// 但是引用类型的唯一作用就是限制了接收的类型，后续使用中都退化成了左值，
// 我们希望能够在传递过程中保持它的左值或者右值的属性, 就需要使用完美转发
template<typename T>
void PerfectForward(T&& t)
{
	Fun(forward<T>(t));
}

//->实例化以下四个版本的函数
//void PerfectForward(int& t)
//{
//	//Fun(t);
//	cout << "void PerfectForward(int& t)" << endl;
//}
//
//void PerfectForward(const int& t)
//{
//	//Fun(t);
//	cout << "void PerfectForward(const int& t)" << endl;
//}
// 
//void PerfectForward(int&& t)
//{
//	//Fun(t);
//	cout << "void PerfectForward(int&& t)" << endl;
//}
//
//void PerfectForward(const int&& t)
//{
//	//Fun(t);
//	cout << "void PerfectForward(const int&& t)" << endl;
//}

int main()
{
	PerfectForward(10); // 右值

	int a;
	PerfectForward(a); // 左值

	PerfectForward(std::move(a)); // 右值

	const int b = 8;
	PerfectForward(b); // const 左值

	PerfectForward(std::move(b)); // const 右值

	return 0;
}

//c++ 98
////需求:A类对象不能被拷贝
//class  A
//{
//public:
//	//拷贝构造函数只声明不实现
//	//声明拷贝构造,编译器不再自动生成构造函数需要强制生成构造函数
//	A() = default;
//	A(const A& aa);//error 发生链接性错误
//private:
//	int _a = 0;
//};
//int main()
//{
//	A aa1;
//	A aa2 = aa1;
//	return 0;
//}

//需求:A类对象不能被拷贝
//class  A
//{
//public:
//	//拷贝构造函数只声明不实现
//	//声明拷贝构造,编译器不再自动生成构造函数需要强制生成构造函数
//	A() = default;
//private:
//	int _a = 0;
//	A(const A& aa);
//};
//int main()
//{
//	A aa1;
//	//A aa2 = aa1;
//	return 0;
//}

//c++ 11
//class  A
//{
//public:
//	//拷贝构造函数只声明不实现
//	//声明拷贝构造,编译器不再自动生成构造函数需要强制生成构造函数
//	A() = default;
//	A(const A& aa) = delete;
//private:
//	int _a = 0;
//	
//};
//int main()
//{
//	A aa1;
//	//A aa2 = aa1;
//	return 0;
//}






















