# define  _CRT_SECURE_NO_WARNINGS 1
# include <iostream>
# include <string>
using namespace std;

//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//	}
//};
//class Student :public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//	}
//};


//void Func(Person& people)
//{
//	people.BuyTicket();
//}


//int main()
//{
//	Person Peter;
//	Func(Peter);//引用类型为Person
//
//	Student Linda;
//	Func(Linda);//引用类型为Student
//}

//int main()
//{   
//	Person* stu = new Student;
//	stu->BuyTicket();
//	Person* ps = new Person;
//	ps->BuyTicket();
//	return 0;
//}

//多态调用与普通调用的区别
//class Person
//{
//public:
//	void BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//	}
//	void Print()
//	{
//		cout << "price:" << _price << endl;
//	}
//protected:
//	int _price = 10;
//};
//class Student :public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//	}
//};
////父类对象调用虚函数
//void Func(Person ps)
//{
//	ps.BuyTicket();
//}
//int main()
//{  
//	//多态调用:看指针所指向的对象的类型或者引用所引用的对象类型;
//	Person* p1 = new Student;
//	p1->BuyTicket();//父类指针调用虚函数且虚函数构成重写 ---> 多态调用
//	//普通调用:看指针的类型或者引用的类型或者对象的类型
//	Person* p2 = new Student;
//	p2->Print();//父类指针调用非虚函数 ---> 普通调用
//
//	//父类对象接受不同类型的参数且调用虚函数--->普通调用
//	Person per;
//	Student stu;
//	Func(per);
//	Func(stu);
//	return 0;
//}

//class Person
//{
//public:
//	virtual Person* Func()  //返回值为Person*
//	{
//		cout << "Person* Func()" << endl;
//		return new Person;
//	}
//};
//class Student : public Person
//{
//public:
//	virtual Student* Func()  //返回值为Student*
//	{
//		cout << "Student* Func()" << endl;
//		return new Student;
//	}
//};
//
//int main()
//{
//	Person* p1 = new Student;
//	p1->Func();//调用子类的Func()函数
//	Person* p2 = new Person;
//	p2->Func();//调用父类的Func()函数
//	return 0;
//}


//class Person
//{
//public:
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//class Student : public Person 
//{
//public:
//	virtual ~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//int main()
//{
//	Person* ps = new Person;
//	delete ps;
//
//	Person* stu = new Student;
//	delete stu;
//	return 0;
//}

//class Person
//{
//public:
//	virtual void BuyTicket(int x) 
//	{
//		cout << "买票-全价" << endl;
//	}
//protected:
//	int _price ;
//};
//class Student :public Person
//{
//public:
//	virtual void BuyTicket(int y)override
//	{
//		cout << "买票-半价" << endl;
//	}
//protected:
//	float _price;
//};
//int main()
//{
//	Person* stu = new Student;
//	stu->BuyTicket(12.5);
//
//	Person* ps = new Person;
//	ps->BuyTicket(25);
//	return 0;
//}

////抽象类
//class Car
//{
//public:
//	virtual void Drive() = 0;//纯虚函数 
//};
//
//int main()
//{
//	Car c;//抽象类不能实例化出对象
//	return 0;
//}
//class Car
//{
//public:
//	//纯虚函数一般只声明不实现(可以实现，但没有价值，因为不能实例化出对象，可以定义指针或引用)
//	virtual void Drive() = 0;
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//class BMW :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "BMW-操控" << endl;
//	}
//};
//
//int main()
//{
//	//派生类只有重写了虚函数才能实例化出对象
//	Benz b1;
//	BMW b2;
//	//通过父类的指针去调用不同对象的函数
//	Car* pBenz = new Benz;
//	pBenz->Drive();
//	Car* pBMW = new BMW;
//	pBMW->Drive();
//}


////多态底层探索
////普通继承类对象模型探索
//class Base
//{
//public:
//	void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//protected:
//	int _b=10;
//};
//class Derive :public Base
//{
//public:
//	void Func3()
//	{
//		cout << "Derive::Func3()" << endl;
//	}
//protected:
//	int _d=20;
//};
//
//int main()
//{
//	Base bb;
//	Derive dd;
//	return 0;
//}
//// 普通继承类对象模型探索
//class Base
//{
//public:
//	Base()
//		:_b(20)
//	{
//
//	}
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//protected:
//	int _b = 10;
//};
//class Derive :public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//protected:
//	int _d = 20;
//};
//
//int main()
//{
//	Base bb;
//	//printf("%p\n", *((int*)(&bb)));
//	Derive dd;
//	//printf("%p\n", *((int*)(&dd)));
//	return 0;
//}

//验证虚表的存储位置
//class Base
//{
//public:
//	Base()
//		:_b(20)
//	{
//
//	}
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//protected:
//	int _b = 10;
//};
//class Derive :public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//protected:
//	int _d = 20;
//};
//
//int main()
//{
//	int i = 0;
//	static int j = 1;
//	int* p1 = new int;
//	const char* p2 = "abcdef";
//
//	printf("栈区: %p\n", &i);
//	printf("静态区: %p\n", &j);
//	printf("堆区: %p\n", p1);
//	printf("常量区: %p\n", p2);
//	Base bb;
//	Base* p3 = &bb;
//	printf("Base虚表地址: %p\n", *((int*)p3));
//
//	Derive dd;
//	Derive* p4 = &dd;
//	printf("Derive虚表地址: %p\n", *((int*)p4));
//	return 0;
//} 