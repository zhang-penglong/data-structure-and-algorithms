# define  _CRT_SECURE_NO_WARNINGS 1
# include <iostream>
//using namespace std;
//using std::cout;
//using std::endl;
//using std::cin;
//int main()
//{
//	int a = 0;
//	cout << "hello world"<<endl;
//	cout << "hello bit" << endl;
//	cout << "hello Linux"<<endl;
//	cin >> a;
//	cout << a<<endl;
//	return 0;
//
//}
//# include <stdio.h>
//# include <stdlib.h> //stdlib.h文件中包含库函数rand()函数
//int rand = 20;      //全局变量rand与stdlib.h文件中的库函数rand()发生命名冲突
//int main()
//{
//	printf("%d\n", rand);
//	return 0;
//}
//代码运行结果 error C2365:"rand":重定义,以前定义是"函数"
//namespace Qspace 
//{
//	int a = 10;
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//	struct ListNode
//	{
//		int val;
//		struct ListNode* next;
//	};
//}
//int main()
//{
//	printf("%d\n", Qspace::a);  //变量的访问方式
//	int ret = Qspace::Add(2, 3);//函数的访问方式
//	printf("%d\n", ret);
//	struct Qspace::ListNode node = { 0, NULL };//结构体类型的访问方式
//	return 0;
//}

//test.cpp文件
//# include "test.h"
//namespace MSpace
//{
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//	namespace Subspace
//	{
//		int sub(int x, int y)
//		{
//			return x - y;
//		}
//	}
//}
//int main()
//{
//	int ret = MSpace::Mul(2, 3);
//	printf("%d\n", ret);
//	return 0;
//}


//namespace MSpace
//{
//	int a = 10;
//	int b = 20;
//}
////using 命名空间名::命名空间成员名
//using MSpace::a;
//
//int main()
//{
//	printf("%d ", a);
//	return 0;
//}

//namespace Addspace
//{
//	int a = 10;
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//}
//using namespace Addspace;
//int main()
//{
//	scanf("%d", &a);
//	int ret= Add(2, 3);
//	printf("%d\n", ret);
//	return 0;
//}

//cin遇到空格键 Tab键 Enter键停止读取
//# include <iostream>
//using namespace std;
//int main()
//{
//	char a[10] = { 0 };
//	cin >> a;
//	cout << a << endl;
//	return 0;
//}
# include <iostream>
using namespace std;
void Fun(int a,int b=20,int c=30)//半缺省参数
{
	cout << "a="<< a ;
	cout << "b="<< b ;
	cout << "c="<< c << endl;
}
int main()
{
	Fun(1);
	Fun(1, 2);
	Fun(1, 2, 3);
	return 0;
}