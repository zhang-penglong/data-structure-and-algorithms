# define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
using namespace std;
//struct Date
//{
//	int _year = 2024;
//	int _month = 1;
//	int _day = 1;
//};
//int main()
//{
//	//链表中的数据成员为自定义类型Date
//	list<Date> lt;
//	Date d1;
//	lt.push_back(d1);
//
//	//it为封装结点指针的迭代器类
//	list<Date>::iterator it = lt.begin();
//
//	//结点中的数据成员访问方式1: 结构体变量.结构体成员
//	cout << (*it)._year << " " << (*it)._month << " " << (*it)._day << endl;
//	cout << it.operator*()._year << " " << it.operator*()._month << " " << it.operator*()._day << endl;
//
//	//结点中的数据成员访问方式2: 结构体指针->结构体成员
//
//	//it->_year本应该为it->->_year,但由于->->可读性差,编译器优化为->;
//	//第一个->去调用operator->重载函数返回Date*的指针，第二个->用来去访问自定义类型的成员变量;
//	cout << it->_year << " " << it->_month << " " << it->_day << endl;
//	cout << it.operator->()->_year << " " << it.operator->()->_month <<" " << it.operator->()->_day<< endl;
//}


namespace Lt
{
	template<class T>
	struct ListNode
	{
		//结点中的成员变量
		T _data;//数据
		ListNode<T>* _prev;//前驱指针
		ListNode<T>* _next;//后继指针

		//结点类的构造函数
		ListNode(const T& val = T())
			: _data(val)
			, _prev(nullptr)
			, _next(nullptr)
		{}
	};
	template<class T, class Ref, class Ptr>
	struct __List_iterator
	{
		typedef ListNode<T> Node;
		Node* _node;
		__List_iterator(Node* node)
			:_node(node)
		{}
		typedef __List_iterator<T> self;
		//++it
		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		//it++
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		//--it
		self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		//it--
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
		bool operator==(const self& s)
		{
			return _node == s._node;
		}
		bool operator!=(const self& s)
		{
			return _node != s._node;
		}
		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
		}
	};

	template<class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef __List_iterator<T, T&, T*> iterator;
		typedef __List_iterator<T, const T&, const T*> const_iterator;
		//构造函数
		list()
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;
		}
		iterator begin()
		{
			//return iterator(_head->_next);
			//单参数的构造函数支持类型转换__List_iterator(Node* node)
			//支持Node* 转换为 迭代器对象
			return _head->_next;
		}

		iterator end()
		{
			return _head;
		}

		const_iterator begin() const
		{
			return _head->_next;
		}

		const_iterator end() const
		{
			return _head;
		}


		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;//当前结点指针
			Node* prev = cur->_prev;//前驱结点指针
			Node* newnode = new Node(x);//新开辟结点

			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			//return iterator(newnode);
			return newnode;
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());//不能删除空双向循环链表

			Node* cur = pos._node;//当前结点指针
			Node* prev = cur->_prev;//前驱结点指针
			Node* next = cur->_next;//后继结点指针
			prev->_next = next;
			next->_prev = prev;

			delete cur;

			return next;//返回删除位置的下一位置
		}

		void push_back(const T& x)
		{
			insert(end(), x);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		T& front()
		{  
			//*begin-->T& operator*()
			return *begin();
		}
		const T& front()const
		{
			return *begin();
		}
		T& back()
		{
			return *(--end());
		}
		const T& back()const
		{
			return *(--end());
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		//判断容器是否非空
		bool empty()const
		{
			return begin() == end();
		}

		//交换容器的头指针
		void swap(list<T>& tmp)
		{
			std::swap(_head, tmp._head);
		}


		list(const list<T>& lt)
		{
			_head = new Node; // 申请一个头结点
			_head->_next = _head; // 后继指针指向自己
			_head->_prev = _head; // 前驱指针指向自己
			for (const auto& e : lt) // 拷贝到新构造的容器中
			{
				push_back(e);
			}
		}

		//list<T>& operator=(const list<T>& lt)
		//{
		//	// 防止自己给自己赋值
		//	if (this != &lt)
		//	{
		//		clear(); // 清空数据
		//		for (const auto& e : lt) // 拷贝到新构造的容器中
		//		{
		//			push_back(e);
		//		}
		//	}
		//	return *this; // 支持连续赋值
		//}


		list<T>& operator=(list<T> lt) //拷贝构造lt对象
		{
			std::swap(_head, lt._head); //交换指针
			return *this; //支持连续赋值
		}

		~list()
		{
			clear();

			delete _head;
			_head = nullptr;
		}



	private:
		Node* _head;
	};
}




















