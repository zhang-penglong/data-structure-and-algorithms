#include <iostream>
#include <assert.h>
using namespace std;
namespace bit
{
	template <class T1,class T2>
	struct pair
	{
		T1 first;//key
		T2 second;//value
		//键值对构造函数
		pair()
			:first(T1())
			,second(T2())
		{}
		//键值对拷贝构造函数
		pair(const T1& a, const T2& b)
			:first(a)
			,second(b)
		{}
	};
}
//结点类
//k(key) v(value)
template<class k,class v>
struct AVLTreeNode
{
	AVLTreeNode<k, v>* _left;//指向左孩子
	AVLTreeNode<k, v>* _right;//指向右孩子
	AVLTreeNode<k, v>* _parent;//指向父节点
	int _bf = 0;//平衡因子
	pair<k, v> _kv;//有效数据

	//开辟结点时需要构造函数
	AVLTreeNode(const pair<k,v>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_bf(0)
		,_kv(kv)
	{}
};
template <class k,class v>
class AVLTree
{
	typedef AVLTreeNode<k, v> Node;
public:
	//首先按照搜索二叉树的规则插入数据
	//其次更新平衡因子bf
	bool Insert(const pair<k,v>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}
		//parent记录父节点的位置,cur寻找插入位置
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur != nullptr)
		{
			//根据键值key比较,确定迭代左子树或是右子树
			if ((cur->_kv).first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if ((cur->_kv).first>kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			//为维护搜索二叉树逻辑结构,同一颗树不能出现相等的数值
			else
			{
				return false;
			}
		}
		//开辟结点,存储数据，建立链接关系
		cur = new Node(kv);
		if ((parent->_kv).first > kv.first)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		
		cur->_parent = parent;
		//更新平衡因子_bf
		while (parent)
		{
			//更新父节点的平衡因子
			//若cur是parent的左孩子,则parent->_bf--;
			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			//若cur是parent的右孩子孩子,则parent->_bf++;
			else
			{
				parent->_bf++;
			}
			//更新祖先结点的平衡因子,根据以parent作为根结点的子树的高度是否发生变化进行更新
			if (parent->_bf == 0)
			{
				//子树高度未发生变化,对其祖先结点的平衡因子无影响
				break;
			}

			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				//向上迭代更新平衡因子
				cur = parent;
				parent = parent->_parent;
			}

			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//迭代过程中以parent作为根结点的子树违背平衡规则---旋转处理
				//左单旋场景
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateLeft(parent);
				}
				//右单旋场景
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateRight(parent);
				}
				//先左旋后右旋场景
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}
				//先右旋后左旋场景
				else
				{
					RotateRL(parent);
				}
				break;
			}
			//插入前该树不是AVL树
			else
			{
				assert(false);
			}
			}
		return true;
		}

	//左单旋
	void RotateLeft(Node* parent)
	{
		Node* SubR = parent->_right;
		Node* SubRL = SubR->_left;
		Node* ppNode = parent->_parent;

		parent->_right = SubRL;
		parent->_parent = SubR;
		SubR->_left = parent;
		if (SubRL != nullptr)
		{
			SubRL->_parent = parent;
		}
		//当前树为整棵树,更新_root
		if (parent == _root)
		{
			_root = SubR;
			SubR->_parent = nullptr;
		}
		//当前树为某棵树的子树
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = SubR;
			}
			else
			{
				ppNode->_right = SubR;
			}
			SubR->_parent = ppNode;
		}
		//更改平衡因子
		SubR->_bf = 0;
		parent->_bf = 0;
	}

	//右单旋
	void RotateRight(Node* parent)
	{
		Node* SubL = parent->_left;
		Node* SubLR = SubL->_right;
		Node* ppNode = parent->_parent;
		parent->_left = SubLR;
		parent->_parent = SubL;
		if (SubLR != nullptr)
		{
			SubLR->_parent = parent;
		}
		SubL->_right = parent;
		//当前树为整棵树
		if (ppNode==nullptr)
		{
			_root = SubL;
			SubL->_parent = nullptr;
		}
		//当前树为某棵树的子树
		else
		{
			if (ppNode->_left == parent)
			{
				ppNode->_left = SubL;
			}
			else
			{
				ppNode->_right = SubL;
			}
			SubL->_parent = ppNode;
		}

		SubL->_bf = 0;
		parent->_bf = 0;
	}
	//先左单旋后右单旋
	//根据SubLR的平衡因子调节三种情形下SubL parent SubLR的平衡因子
	void RotateLR(Node* parent)
	{
		Node* SubL = parent->_left;
		Node* SubLR = SubL->_right;
		int bf = SubLR->_bf;
		//双旋中的单旋会更改平衡因子,应记录旋转之前SubLR中的平衡因子
		RotateLeft(parent->_left);
		RotateRight(parent);
		//SubLR本身为新增结点
		if (bf == 0)
		{
			parent->_bf = 0;
			SubLR->_bf = 0;
			SubL->_bf = 0;
		}
		//子树b被拆解的左子树d插入结点
		else if (bf == -1)
		{
			parent->_bf = 1;
			SubLR->_bf = 0;
			SubL->_bf = 0;
		}
		//子树b被拆解的右子树e插入结点
		else if (bf == 1)
		{
			parent->_bf = 0;
			SubLR->_bf = 0;
			SubL->_bf = -1;
		}
		else
		{
			assert(false);
		}
	}

	//先右单旋后左单旋
	void RotateRL(Node* parent)
	{
		Node* SubR = parent->_right;
		Node* SubRL = SubR->_left;
		int bf = SubRL->_bf;

		RotateRight(SubR);
		RotateLeft(parent);
		
		if (bf == 0)
		{
			parent->_bf = 0;
			SubR->_bf = 0;
			SubRL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			SubR->_bf = 1;
			SubRL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			SubR->_bf = 0;
			SubRL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	//AVL树的验证 1.首先检查其是否为搜索二叉树---二叉树的中序遍历得到有序序列
	//            2.每个结点子树的高度差的绝对值小于2
	//            3.验证平衡因子是否计算正确

	void InOrder()
	{
		_InOrder(_root);
	}
	int _Height(Node* root)
	{
		if (root == nullptr)
			return 0;
		int LeftHeight = _Height(root->_left);
		int RightHeight = _Height(root->_right);
		return LeftHeight > RightHeight ? LeftHeight + 1 : RightHeight + 1;
	}
	int Height()
	{
		return _Height(_root);
	}
	//验证方案一:前序遍历验证其是否为平衡树,验证根时求取右子树与左子树的高度,
	//当验证下一层又要求下一层左子树与右子树的高度,并未保留上一次计算时的数据,效率低下
	//bool _IsBalance(Node* root)
	//{
	//	if (root == nullptr)
	//		return true;
	//	//求取左子树，右子树的高度
	//	int LeftHeight = Height(root->_left);
	//	int RightHeight = Height(root->_right);
	//	//判断当前树是否为平衡树
	//	if (abs(RightHeight - LeftHeight) >= 2)
	//	{
	//		cout << (root->_kv).first << ":" << (root->_kv).second << "不平衡" << endl;
	//		return false;
	//	}
	//	//判断当前根结点处的平衡因子是否正常
	//	if ((RightHeight - LeftHeight)!=root->_bf)
	//	{
	//		cout << (root->_kv).first << ":" << (root->_kv).second << "平衡因子异常" << endl;
	//		return false;
	//	}
	//	return _IsBalance(root->_left) && _IsBalance(root->_right);
	//}

	//验证方案二:后序遍历验证其是否为平衡树
	bool _IsBalance(Node* root, int& height)
	{
		if (root == nullptr)
		{
			height = 0;
			return true;
		}

		int leftHeight = 0, rightHeight = 0;
		if (!_IsBalance(root->_left, leftHeight)
			|| !_IsBalance(root->_right, rightHeight))
		{
			return false;
		}

		if (abs(rightHeight - leftHeight) >= 2)
		{
			cout << root->_kv.first << "不平衡" << endl;
			return false;
		}

		if (rightHeight - leftHeight != root->_bf)
		{
			cout << root->_kv.first << "平衡因子异常" << endl;
			return false;
		}

		height = leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;

		return true;
	}
	bool IsBalance()
	{
		int height = 0;
		return _IsBalance(_root, height);
	}
	size_t Size()
	{
		return _Size(_root);
	}

	size_t _Size(Node* root)
	{
		if (root == NULL)
			return 0;

		return _Size(root->_left)
			+ _Size(root->_right) + 1;
	}

	Node* Find(const int& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (cur->_kv.first < key)
			{
				cur = cur->_right;
			}
			else if (cur->_kv.first > key)
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}

		return NULL;
	}

private:
	
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);
		cout << (root->_kv).first << ":" << (root->_kv).second << endl;
		_InOrder(root->_right);
	}
	Node* _root = nullptr;
};
















