# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stddef.h>//offsetof头文件
//offsetof宏的实现-百度笔试
//计算结构体中某变量相对于首地址的偏移
//偏移量为结构体中偏移量为0的地址与其中任意元素偏移量地址处之间相差的字节数
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//int main()
//{
//	printf("%d\n", offsetof(struct S, c1));
//	printf("%d\n", offsetof(struct S, i));
//	printf("%d\n", offsetof(struct S, c2));
//	return 0;
//}


//假设0为结构体的首地址
//成员的地址即为偏移量

//# define OFFSETOF(type,member)   (size_t)&(((type*)0)->member)
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//int main()
//{
//	printf("%d\n", OFFSETOF(struct S, c1));
//	printf("%d\n", OFFSETOF(struct S, i));
//	printf("%d\n", OFFSETOF(struct S, c2));
//	return 0;
//}

//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换
//以8个二进制位为例
//10011101
//从0开始，最高位为偶数位
//01101110 - 交换完成
//偶数位向右移动一位，奇数位向左移动一位
//思路:
//将所有的偶数位全部拿到(奇数位全部置为0) 即10001000 向右移动一位 01000100
//将所有的奇数位全部拿到(偶数位全部置为0) 即00010101 向左移动一位 00101010
//相加 01101110
# define SWAP(n) (n=((n&0xaaaaaaaa)>>1)+((n&0x55555555)<<1))
int main()
{
	int n = 10;//1010
	SWAP(n);//0101 5
	printf("%d\n", n);
	return 0;
}










