# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;

//class Person
//{
//public:
//
//protected:
//	string _name;
//	int _age=20;
//};
//
//class Student :public Person
//{
//public:
//	void Func()
//	{
//		cout << "Student: " << _age << endl;
//		cout << "Person: " << Person::_age << endl;
//	}
//protected:
//	float _age=10.0;
//};
//
//int main()
//{
//	Student stu;
//	stu.Func();
//	return 0;
//}

//类中默认构造函数存不存在取决于外部的调用方式,默认构造不一定必须存在
//class Date
//{
//public:
//	Date(int year)
//	{
//		_year = year;
//	}
//protected:
//	int _year;
//};
//int main()
//{
//	Date d(2020);
//	return 0;
//}



//静态成员变量的本质受类域限制的全局变量
//局部变量原先存储于栈区,static修饰局部变量之后,改变局部变量的存储位置
//此时局部变量存储于静态区,获得与全局变量一样长的生命周期
//class Test
//{
//public:
//	static int c;
//	void Print()
//	{
//		//静态成员Test类内部无论公有/私有都可以访问
//		cout << "b=" << b << endl;
//		cout << "c=" << c << endl;
//	}
//private:
//	int a;
//	static int b;//静态成员变量的声明
//};
//
//int Test::b = 1;//静态成员变量的定义
//int Test::c = 2;//静态成员变量的定义
//
//int main()
//{
//	Test t;
//
//	//访问限定符private修饰静态成员变量b,类外不可访问
//	//cout << "b= " << Test::b << endl;
//
//	//访问限定符public修饰静态成员变量c,类外可以访问
//	cout << "c=" << Test::c << endl;//访问方式一:  类名::静态成员变量
//	cout << "c=" << t.c << endl;    //访问方式二:  对象.静态成员变量
//
//	t.Print();
//	return 0;
//}


//静态成员函数

//类的成员函数被static修饰称为静态成员函数
//使用场景: 类中的成员函数没有使用类中的成员变量,函数声明前加上static使之成为静态成员函数
//静态成员函数属于整个类，不属于类实例化出的某一个对象(与类相关联而不是与类实例化对象相关联)
//当调用一个类实例化的对象的成员函数(非静态成员函数)时,操作系统将此对象的起始地址赋给成员函数的this指针;
//由于静态成员函数不属于某一个对象,与任何对象都无关,因此静态成员函数没有this指针;
//静态成员函数没有this指针,不指向某一个对象，也就无法对一个对象中的非静态成员进行访问
//
//class Person
//{
//public:
//	//非静态成员函数无法访问静态成员变量
//	/*
//	int Getcount()
//	{
//		return _count;
//	}
//	*/
//
//	//静态成员函数可以访问静态成员变量
//	int static Getcount()
//	{
//		return _count;
//	}
//
//	//非静态成员函数可以调用静态成员函数
//	void Print()
//	{
//		int count = Person::Getcount();
//		cout << "count=" << count << endl;
//	}
//
//	//静态成员函数不可以调用非静态成员函数
//	/*
//	int Getage() 
//	{
//		return _age;
//	}
//	void static Test()
//	{
//		int num = Getage();
//	}
//	*/
//private:
//	string _name = "Linda";
//	int _age=20;
//	static int _count;//静态成员变量声明
//};
//int Person::_count = 1;//静态成员变量定义
//int main()
//{
//	Person p;
//	//静态成员函数调用方式一: 对象.静态成员
//	p.Getcount();
//	//静态成员函数调用方式二: 类名::静态成员
//	Person::Getcount();
//
//	p.Print();
//	return 0;
//}


//class Person
//{
//public:
//	Person()
//	{ 
//		count++;
//	}
//	static int count;//统计对象的个数
//};
//int Person::count = 0;
//class Student :public Person
//{
//public:
//
//protected:
//	int _stuid;
//};
//int main()
//{
//	Person per; //对象per调用Person类的构造函数
//
//	//对象stu的自身成员调用student类的构造函数,对象stu的父类成员调用Person类的构造函数
//	//只需要父类(person类)的构造函数++count便可完成统计;
//	Student stu;
//	cout << "Student:" << stu.count << endl;//访问方式一: 对象.静态成员
//	cout << "Student:" << Person::count << endl;//访问方式二: 类名::静态成员
//}

//class Person
//{
//public:
//	string _name; // 姓名
//};
//class Student : virtual public Person
//{
//protected:
//	int _num; //学号
//};
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _course; //主修课程
//};
//
//int main()
//{
//	//菱形继承统一解决数据冗余与二义性问题
//	Assistant assist;
//	assist._name = "peter";
//	return 0;
//}

//class A
//{
//public:
//	int _a;
//};
//class B : virtual public A
//{
//public:
//	int _b;
//};
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
////当前机器为小端存储
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d._b = 2;
//	d.C::_a = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}

//class A
//{
//public:
//	void func1()
//	{
//
//	}
//
//protected:
//	void func2()
//	{
//
//	}
//
//	int _a1;
//	int _a2;
//};
//
//// 组合和继承
//// 黑箱复用
//class B
//{
//public:
//	void func()
//	{
//		//_a.func2();
//	}
//protected:
//	A _a;
//	int _b;
//};

// 白箱复用
//class B ：public A
//{
//protected:
//	// ...
//	int _b;
//};

class A
{
public:
	//...
protected:
	int _num=10;
};

class B
{
public:

protected:
	A _a;
};















