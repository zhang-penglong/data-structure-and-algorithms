# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
using namespace std;

void TestVector1()
{
	// 1、无参构造
	// explicit vector();
	vector<int> v1;
	// 2、用n个val初始化vector
	// explicit vector (size_type n, const value_type& val = value_type());
	vector<int> v2(8, 5);
	// 3、迭代器区间构造
	// template <class InputIterator>
	// vector(InputIterator first, InputIterator last);
		// 任意合理迭代器区间都可（左闭右开）
	vector<int> v3(v2.begin(), v2.end());
	// 还可以用数组名(左闭右开)
	int arr[] = { 10, 20, 30, 40, 50, 60, 70 };
	vector<int> v4(arr, arr + 4);
	// 4、vector拷贝构造
	// vector (const vector& vec);
	vector<int> v5(v4);
}
void TestVector2()
{
	vector<int> v1;
	vector<int> v2(10, 5);
	v1 = v2;
}
void TestVector3()
{
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);
	v1.push_back(40);
	v1.push_back(50);

	//正向遍历
	vector<int>::iterator vit = v1.begin();
	while (vit != v1.end())
	{
		cout << *vit << " ";
		++vit;
	}
	cout << endl;

	//反向遍历
	vector<int>::reverse_iterator rit = v1.rbegin();
	while (rit != v1.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}
void TestVector6()
{
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);
	v1.push_back(40);
	v1.push_back(50);
	cout << "size: "<<v1.size() << endl;
	cout << "capacity: "<< v1.capacity() << endl;
	v1.resize(2); // n < size()
	cout << "size: " << v1.size() << endl;
	cout << "capacity: " << v1.capacity() << endl;
	
	v1.resize(4, 6);// size() < n < capacity()
	cout << "size: " << v1.size() << endl;
	cout << "capacity: " << v1.capacity() << endl;
	vector<int>::iterator vit = v1.begin();
	cout << "容器v1中的数据:";
	while (vit != v1.end())
	{
		cout << *vit << " ";
		++vit;
	}
	cout << endl;
	
	v1.resize(10);// n > capacity()
	cout << "size: " << v1.size() << endl;
	cout << "capacity: " << v1.capacity() << endl;
   
}
void TestVector7()
{
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);
	v1.push_back(40);
	v1.push_back(50);
	// 返回vector头部数据
	cout << v1.front() << endl;
	// 返回vector的尾部数据
	cout << v1.back() << endl;

	// []+下标遍历vector中的元素
	for (int i = 0; i < v1.size(); i++)
	{
		cout << v1[i] << " ";
	}
	cout << endl;
}
void TestVector8()
{
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);
	v1.push_back(40);
	v1.push_back(50);

	// vector没有提供find接口,使用算法库中的find即可;
	vector<int>::iterator it = find(v1.begin(), v1.end(), 30);
	v1.insert(it, 100);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
	// 插入n个值
	it = find(v1.begin(), v1.end(), 20);
	v1.insert(it, 3, 10);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
	// 删除一段迭代区间的值（左闭右开）
	v1.erase(v1.begin() + 1, v1.begin() + 5);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}
int main()
{
	TestVector8();
	return 0;
}
