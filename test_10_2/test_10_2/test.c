# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>

//去重算法
int removeDuplicates(int* nums, int numsSize)
{
	int src = 1;
	int dest = 0;
	while (src<numsSize)
	{
		if (nums[src] != nums[dest])
		{
			dest++;
			nums[dest] = nums[src];
			src++;
		}
		else
		{
			src++;
		}
	}
	return dest + 1;
}

//反转链表
struct ListNode
{
	int val;
	struct ListNode* next;
};
struct ListNode* reverseList(struct ListNode* head)
{
	struct ListNode* n1 = NULL;
	struct ListNode* n2 = head;
	struct ListNode* n3;
	if (n2 != NULL)
	{
		n3 = n2->next;
	}
	while (n2 != NULL)
	{
		n2->next = n1;

		n1 = n2;
		n2 = n3;
		if (n3 != NULL)
		{
			n3 = n3->next;
		}
	}
	return n1;
}

//移除链表元素

struct ListNode
{
	int val;
	struct ListNode* next;
};

struct ListNode* removeElements(struct ListNode* head, int val)
{
	struct ListNode* cur = head;
	struct ListNode* curprev = NULL;
	while (cur != NULL)
	{
		if (cur->val == val)
		{
			//首元素即为删除的元素
			if (cur == head)
			{
				head = cur->next;
				free(cur);
				cur = head;
			}
			else
			{
				curprev->next = cur->next;
				free(cur);
				cur = curprev->next;
			}
		}
		else
		{
			curprev = cur;
			cur = cur->next;
		}
	}
	return head;
}

//快慢指针
//当链表中的结点为奇数个，慢指针走一步，快指针走两步，当快指针指向的结点的next为空，慢指针指向的即为中间结点
//当链表中的结点为偶数个，慢指针走一步，快指针走两步，当快指针指向空指针，慢指针指向的即为中间结点
struct ListNode* middleNode(struct ListNode* head)
{
	struct ListNode* fast = head;
	struct ListNode* slow = head;

	while ((fast) && (fast->next))
		//(fast->next==NULL)和(fast==NULL)俩个有一个为真，循环停止;
	{
		fast = fast->next->next;
		slow = slow->next;
	}
	return slow;

}

//创建新链表，node->val=val 删除，否则进行尾插；
struct ListNode
{
	int val;
	struct ListNode* next;
};
struct ListNode* removeElements(struct ListNode* head, int val)
{
struct ListNode* newhead = NULL;
struct ListNode* tail = NULL;
struct ListNode* cur = head;
while (cur)
{


	if (cur->val == val)
	{
		struct ListNode* del = cur;
		cur = cur->next;
		free(del);
	}
	else
	{
		if (tail == NULL)
		{
			newhead = tail = cur;
			cur = cur->next;
		}
		else
		{
			tail->next = cur;
			tail = tail->next;
			cur = cur->next;
		}
	}
}
if (tail)
tail->next = NULL;


return newhead;
}?