# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	//判断打开文件是否成功
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//文件打开成功，写文件
//	fputc('a', pf);
//	fputc('b', pf);
//	fputc('c', pf);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	//判断打开文件是否成功
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//文件打开成功，读文件 - 读一行
//	char arr[10] = { 0 };
//	fgets(arr, 10, pf);
//	printf("%s\n", arr);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//struct s
//{
//	int a;
//	float b;
//};
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	//判断打开文件是否成功
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//文件打开成功，读文件
//	struct s s1 = { 0 };
//	fscanf(pf, "%d %f", &(s1.a), &(s1.b));
//	printf("%d %f", s1.a, s1.b);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//struct S
//{
//	int a;
//	float b;
//	char arr[10];
//};
//int main()
//{
//	struct S s1 = { 0 };
//	//以二进制读取的形式打开文件
//	FILE* pf = fopen("data.txt", "rb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	fread(&s1, sizeof(struct S), 1, pf);
//	printf("%d %f %s\n", s1.a, s1.b, s1.arr);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//定位文件指针到f的位置
//	fseek(pf, 5, SEEK_SET);
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//定位文件指针到f的位置
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	 ch = fgetc(pf);
//	printf("%c\n", ch);//b
//
//	rewind(pf);//让文件内部的位置指针回到起始位置
//	ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

int main()
{
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL)
	{
		perror("File opening failed");
		return 1;
	}
	//fgetc当读取失败或者遇到文件结束时，都会返回EOF
	int c;
	while ((c = fgetc(pf)) != EOF)
	{
		putchar(c);//正常读取
	}
	printf("\n");
	//读取结束
	//判断什么原因读取结束
	if (ferror(pf))
	{
		printf("I/O error when reading\n");
	}
	else if (feof(pf))
	{
		printf("End of File reached successfully\n");
	}
	return 0;
}