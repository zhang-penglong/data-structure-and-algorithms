# define  _CRT_SECURE_NO_WARNINGS 1
# include <iostream>
using namespace std;

//拷贝构造函数
//利用已经存在的对象创建一个新对象时(拷贝对象)，就会调用新对象的拷贝构造函数初始化新对象的成员变量;
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		{
//			_year = year;
//			_month = month;
//			_day = day;
//		}
//	//假设日期类的拷贝构造函数如下
//	Date(Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	void Print()
//		{
//		    cout << _year << "/" << _month << "/" << _day << endl;
//		}
//
//};
//void func1(Date d)
//{
//	d.Print();
//}


//class Stack
//{
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//public:
//	//构造函数
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (nullptr == _a)
//		{
//			perror("malloc failed: ");
//		}
//		_capacity = capacity;
//		_top = 0;
//	}
//	//析构函数
//	~Stack()
//	{
//		free(_a);
//		_capacity = _top = 0;
//		_a = nullptr;
//	}
//};
//void func2(Stack st)
//{
//	//...
//}

//int main()
//{
//	Date d1(2023, 12, 20);
//	Date d2(d1);
//
//	return 0;
//}


//class Stack
//{
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//public:
//	//构造函数
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int)* capacity);
//		if (nullptr == _a)
//		{
//			perror("malloc failed: ");
//		}
//		_capacity = capacity;
//		_top = 0;
//	}
//	//拷贝构造函数
//	//this指向st stt为st1的别名
//	Stack(Stack& stt)
//	{
//		//开辟与st1一样大的空间
//		this->_a = (int*)malloc(sizeof(int)*stt._capacity);
//		if (nullptr ==this->_a)
//		{
//			perror("malloc failed:");
//			exit(-1);
//		}
//		//拷贝指向的资源
//		memcpy(this->_a, stt._a, sizeof(int)*stt._top);
//		this->_top = stt._top;
//		this->_capacity = stt._capacity;
//	}
//	//析构函数
//	~Stack()
//	{
//		free(_a);
//		_capacity = _top = 0;
//		_a = nullptr;
//	}
//};
//void func2(Stack st)
//{
//	//...
//}
//int main()
//{
//	Stack st1;
//	func2(st1);
//	return 0;
//}

//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	Date(int year = 2000, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//
//};
//void func(Date d)
//{
//	d.Print();
//}
//int main()
//{
//	Date d1(2023, 12, 20);
//	func(d1);
//	return 0;
//}




//class Stack
//{
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//public:
//	//构造函数
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int)* capacity);
//		if (nullptr == _a)
//		{
//			perror("malloc failed: ");
//		}
//		_capacity = capacity;
//		_top = 0;
//	}
//	//拷贝构造函数
//	//this指向st stt为st1的别名
//	Stack(Stack& stt)
//	{
//		//开辟与st1一样大的空间
//		this->_a = (int*)malloc(sizeof(int)*stt._capacity);
//		if (nullptr ==this->_a)
//		{
//			perror("malloc failed:");
//			exit(-1);
//		}
//		//拷贝指向的资源
//		memcpy(this->_a, stt._a, sizeof(int)*stt._top);
//		this->_top = stt._top;
//		this->_capacity = stt._capacity;
//	}
//	//析构函数
//	~Stack()
//	{
//		free(_a);
//		_capacity = _top = 0;
//		_a = nullptr;
//	}
//};
//
//Stack& func()
//{
//	static Stack st;
//
//	return st;//传值返回,返回谁？-->返回时先将局部变量st保存于寄存器中;
//	//局部变量st出作用域销毁,因此返回的是st的拷贝;所以此处调用拷贝构造函数;
//}
//int main()
//{
//	func();
//	return 0;
//}

///////////////////////////////////////////////////////
//运算符重载
# include <stdbool.h>
# include <assert.h>
class Date
{
//private:
public:
	int _year;
	int _month;
	int _day;
	//构造函数
	Date(int year=2023, int month=12, int day=20)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
	//拷贝构造函数
	Date(Date& dd)
	{
		_year = dd._year;
		_month = dd._month;
		_day = dd._day;
	}
	//对于类中的成员函数,第一个参数为隐藏的this指针;
	//bool operator>(const Date& x, const Date& y)--->报错:参数太多; ==运算符只有两个操作数
	//运算符重载函数作为类成员函数重载时，其形参看起来比操作数数目少1，因为成员函数的第一个参数为隐藏的this;
	//
	bool operator>(const Date& y)
	{
		if (this->_year > y._year)
			return true;
		else if (this->_year == y._year&&this->_month > y._month)
			return true;
		else if (this->_year == y._year&&this->_month == y._month&&this->_day > y._day)
			return true;

		return false;
	}
	bool operator== ( const Date& y)
	{
		return _year == y._year
			&& _month == y._month
			&& _day == y._day;
	}

	//d1 + 100;
	//获取每个月的天数
	int GetMonthDay(int year, int month)
	{
		assert(year >= 1 && (month >= 1 && month <= 12));
		//平年二月28天 闰年二月29天;
		int MonthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		//判断是不是闰年
		/*if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) && month == 2)
		{
			return 29;
		}*/
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}
		return MonthArray[month];
	}
	//d1+=天数
	Date& operator+=(int day)
	{
		_day = _day + day;
		while(_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month == 13)
			{
				_year++;
				_month = 1;
			}
		}
		//注意: this==&d1,d1在operator+=()函数作用域外,出作用域并不会被销毁,可以传引用返回;
		//这样避免调用拷贝构造函数,提升效率;
		return *this;
	}
	//d1+100;
	//d1+day;
	//Date operator+(int day)
	//{
	//	Date tmp(*this);//利用拷贝构造函数初始化tmp中的数据,对象tmp与对象d1数据内容相同;
	//	tmp._day += day;
	//	while (tmp._day > GetMonthDay(_year, _month))
	//	{
	//		tmp._day -= GetMonthDay(_year, _month);
	//		tmp._month++;
	//		if (tmp._month == 13)
	//		{
	//			tmp._year++;
	//			tmp._month = 1;
	//		}
	//	}
	//	return tmp;
	//	//此处不可传引用返回,tmp出作用域销毁，只能传值返回;
	//}
	Date operator+(int day)
	{
		Date tmp(*this);
		tmp += day;
		return tmp;
	}
};
// bool Greater(Date x,Date y)
//bool compare(Date x,Date y)
//增强代码的可读性,规避命名不规则 函数名:operator运算符
//运算符重载: 自定义类型可以直接使用运算符;
//函数重载:   支持参数不同的同名函数;
//bool operator>(const Date& x, const Date& y)
//{
//	if (x._year > y._year)
//		return true;
//	else if (x._year == y._year&&x._month > y._month)
//		return true;
//	else if (x._year == y._year&&x._month == y._month&&x._day > y._day)
//		return true;
//
//	return false;
//}
//bool operator== (const Date& x, const Date& y)
//{
//	return x._year == y._year
//		&& x._month == y._month
//		&& x._day == y._day;
//}
int main()
{
	Date d1;
	//Date d3(d1);
	Date d2(2000, 12, 20);
	//d1.Print();
	/*int x = 3, y = 0;
	x > y;*/
	/*d1 > d2;
	d1 = d2;*/
	//cout << Equal(d1, d2) << endl;
	//cout << Greater(d1, d2) << endl;
	/*cout << operator>(d1, d2) << endl;
	cout << operator==(d1, d2) << endl;
	cout << (d1 > d2) << endl;
	cout << (d1 == d2) << endl;
	bool ret1 = d1 > d2;// d1>d2 ---> operator>(d1,d2)
	bool ret2 = d1 == d2;*/
	//operator>(d1, d2);//传参的本质为拷贝,传参顺序从右向左,此处两次调用拷贝构造函数;
	//为防止两次调用拷贝构造函数,采取传引用方式传参; 判断相等同理;
	//调用方式为operator(d1,d2)，作为类成员函数，this指针指向左操作数;
	cout << d1.operator>(d2) << endl;
	cout << d1.operator==(d2) << endl;
	bool ret3 = d1 > d2; //d1>d2 ---> d1.operator(d2)--->d1.operator(&d1,d2)
	bool ret4 = d1 == d2;//d1==d2 ---> d1.operator(d2)--->d1.operator(&d1,d2)

    //一个类重载那些运算符呢？取决于运算符对于该类有没有实际意义;
    //对于日期类 d1-d2 过了多少天;
	//d1+=day 从现在开始过去多少天;
	//Date d4=d1 += 50;
	//d4.Print();
	//d1.Print();

	//d1+day;
	Date d5 = d1 + 30;
	d1.Print();
	d5.Print();
	return 0;
}