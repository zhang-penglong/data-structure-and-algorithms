# define  _CRT_SECURE_NO_WARNINGS 1
# include <iostream>
using namespace std;
//
//int main()
//{
//	int a = 10;
//	int& pa = a;
//	printf("%p\n", &a);
//	printf("%p\n", &pa);
//	return 0;
//}
//int main()
//{
//	//正确示例
//	int a = 10;
//	int& pa = a;
//	//错误示例
//	int b = 20;
//	int& pc;//编译时出错
//	return 0;
//}
//
//int main()
//{
//	int a = 10;
//	int& pa = a;
//	int& pb = a;
//	int& pc = pa;
//	cout << "a=" << a << endl;
//	cout << "pa=" << pa << endl;
//	cout << "pb=" << pb << endl;
//	cout << "pc=" << pc << endl;
//	return 0;
//}

// int main()
//{
//	int a = 10;
//	int& pa = a;
//	int d = 1;
//	// pa变成d的别名？还是d赋值给pa？
//	pa = d;//pa的引用实体为a,让引用变量pa引用d
//	cout << a << endl;
//	return 0;
//}

//int main()
//{
//	const int a = 10;
//	int& b = a;//错误做法
//	const int& b = a;//正确做法
//	return 0;
//}
//int main()
//{
//	//权限可以缩小
//	int c = 20;
//	const int& d = c;
//	const int& e = 10;
//	return 0;
//}

//int main()
//{
//	int i = 10;
//    double j = i;//整型提升
//    double& rj = i;//错误做法
//	const double& rm = i;//正确做法
//	return 0;
//}

////指针方式
////void Swap1(int* pa, int* pb)
////{
////	int tmp = *pa;
////	*pa = *pb;
////	*pb = tmp;
////}
////引用方式
////void Swap2(int& a, int& b)
////{
////	int tmp = a;
////	a = b;
////	b = tmp;
////}
////int main()
////{
////	int a = 10;
////	int b = 20;
////	cout << "a=" << a;
////	cout << "b=" << b << endl;
////	Swap1(&a, &b);
////	Swap2(a, b);
////	cout << "a=" << a;
////	cout << "b=" << b << endl;
////	return 0;
////}
//
//
//int& Add(int a, int b)
//{
//	static int c = a + b;
//	return c;
//}
//int main()
//{
//	int& ret1 = Add(1, 2);
//	cout << "Add(1,2)=" << ret1 << endl;
//	int& ret2 = Add(3, 4);
//	cout << "Add(3,4)=" << ret2 << endl;
//	return 0;
//}
//void Func()
//{
//	static int c = 0;
//	cout << &c << endl;
//}
//int main()
//{
//	Func();//Func()函数第一次调用结束,销毁第一次为func()函数所开辟的函数栈帧
//	Func();
//	return 0;
//}

//宏函数
//# define Add(x,y) ((x)+(y))
inline int Add(int x, int y)
{
	return x + y;
}
int main()
{
	int a = 10;
	int b = 20;
	int ret = Add(10, 20);
	cout << "ret=" << ret << endl;
	return 0;
}
