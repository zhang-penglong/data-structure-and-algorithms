//list反向迭代器实现代码
//template<class T, class Ref, class Ptr>
//struct __List_reverse_iterator//修改类名
//{
//	typedef ListNode<T> Node;
//	Node* _node;
//	__List_reverse_iterator(Node* node)
//		:_node(node)
//	{}
//	typedef __List_reverse_iterator<T> self;
//
//	//修改++/--接口的指向
//	self& operator++()
//	{
//		_node = _node->_prev;
//		return *this;
//	}
//	//it++
//	self operator++(int)
//	{
//		self tmp(*this);
//		_node = _node->_prev;
//		return tmp;
//	}
//	//--it
//	self& operator--()
//	{
//		_node = _node->_next;
//		return *this;
//	}
//	//it--
//	self operator--(int)
//	{
//		self tmp(*this);
//		_node = _node->_next;
//		return tmp;
//	}
//	bool operator==(const self& s)
//	{
//		return _node == s._node;
//	}
//	bool operator!=(const self& s)
//	{
//		return _node != s._node;
//	}
//	Ref operator*()
//	{
//		return _node->_data;
//	}
//	Ptr operator->()
//	{
//		return &_node->_data;
//	}
//};
//
//template<class T>
//class list
//{
//	typedef ListNode<T> Node;
//public:
//	typedef __List_reverse_iterator<T, T&, T*> reverse_iterator;//重命名反向迭代器
//	typedef __List_reverse_iterator<T, const T&, const T*> const_reverse_iterator;//重命名const反向迭代器
//
//	//......
//
//private:
//	Node* _head;
//};
//template <class _Iterator>
//class reverse_iterator
//{
//protected:
//	_Iterator current;
//public:
//	typedef typename iterator_traits<_Iterator>::reference
//		reference;
//
//	reference operator*() const
//	{
//		_Iterator __tmp = current;
//		return *--__tmp;
//	}
//
//};
#include "ReverseIterator.h"
#include <iostream>
using namespace std;
namespace bit
{
	template<class T>
	struct ListNode
	{
		//结点中的成员变量
		T _data;//数据
		ListNode<T>* _prev;//前驱指针
		ListNode<T>* _next;//后继指针

		//结点类的构造函数
		ListNode(const T& val = T())
			: _data(val)
			, _prev(nullptr)
			, _next(nullptr)
		{}
	};
	template<class T,class Ref,class Ptr>
	struct __List_iterator
	{
		typedef ListNode<T> Node;
		Node* _node;
		__List_iterator(Node* node)
			:_node(node)
		{}
		typedef __List_iterator<T,Ref,Ptr> self;
		//++it
		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		//it++
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		//--it
		self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		//it--
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
		bool operator==(const self& s)
		{
			return _node == s._node;
		}
		bool operator!=(const self& s)
		{
			return _node != s._node;
		}
		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
		}
	};

		template<class T>
		class list
		{
		typedef ListNode<T> Node;
		public:
		typedef __List_iterator<T, T&, T*> iterator;
		typedef __List_iterator<T, const T&, const T*> const_iterator;

		//list类中统一名称
		typedef  ReverseIterator<iterator, T&, T*> reverse_iterator;
		typedef  ReverseIterator<iterator, const T&, const T*> const_reverse_iterator;

		//搭配出口,list类中实现rbegin()、rend()
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}
		//list类构造函数
		list()
		{
			_head = new Node; // 申请一个头结点
			_head->_next = _head; // 后继指针指向自己
			_head->_prev = _head; // 前驱指针指向自己
		}

		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;//当前结点指针
			Node* prev = cur->_prev;//前驱结点指针
			Node* newnode = new Node(x);//新开辟结点

			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			//return iterator(newnode);
			return newnode;
		}

		void push_back(const T& x)
		{
			insert(end(), x);
		}

		iterator begin()
		{
			//return iterator(_head->_next);
			//单参数的构造函数支持类型转换__List_iterator(Node* node)
			//支持Node* 转换为 迭代器对象
			return _head->_next;
		}

		iterator end()
		{
			return _head;
		}

	private:
		Node* _head;
	};
}