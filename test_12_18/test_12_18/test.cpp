# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>

# include <iostream>
using namespace std;

//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//void print(Date* const this) 成员函数中不可显示的写this指针的实参与形参，但是可以可以在类里面显示的使用this指针;
//	void print()
//	{
//	cout << this->_year << "-" <<this-> _month << "-" <<this-> _day << endl;
//    }
//	/*void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}*/
//};
//
//int main()
//{
//	Date d1;
//	Date d2;
//	d1.Init(2023, 10, 18);
//	d2.Init(2022, 11, 15);
//	d1.print();
//	//d1.print(&d1) &d1--->Date* const this
//	//d2.print();
//	return 0;
//}

//构造函数
//构造函数的作用在创建对象时,系统自动调用它初始化数据成员;
//构造函数的定义:
//用类创建对象时,对象表达现实世界的实体
//举例:对于学生类，创建一个学生对象，这个学生就应该具有姓名 年龄 学号等
//但是这些数据成员没有初始化,该学生的姓名 年龄 学号等数据成员的值将为随机值或0;此时这个学生对象无任何意义！
//创建对象时，C++自动初始化对象的工作专门由该类的构造函数完成;

//构造函数的特点
//1. 类名即为函数名;
//2. 没有返回值(函数名前什么也不写,也没有void);
//3. 构造函数支持函数重载;
//4. 类创建对象时，编译器自动调用构造函数(调试)

//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	Date()
//	{
//		//声明
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}
//    /*void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//};
//
//int main()
//{
//	Date d;
//	d.print();
//	return 0;
//}

//构造函数支持函数重载
//函数重载(同一作用域中的同名函数,同名函数的参数个数 参数类型 类型顺序不同)
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	Date()
//	{
//		//声明
//		_year = 2000;
//		_month = 12;
//		_day = 29;
//	}
//	Date(int year, int month, int day)
//	{
//	_year = year;
//	_month = month;
//	_day = day;
//	}
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//};
//
//int main()
//{
//	Date d1;
//	d1.print();
//
//	Date d2(2023, 12, 18);
//	d2.print();
//
//	return 0;
//}

 /*默认构造函数:
 1.用户在类中没有显示定义构造函数, 编译器自动生成的构造函数,叫默认构造函数;
 2.无参构造函数也可以叫默认构造;
 3.全缺省也可以叫默认构造;
 可以不传参数就调用的构造函数，都可以叫默认构造函数;
 这三个函数不能同时存在，只能存在一个;*/
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	Date()//无参的构造函数--->默认构造函数
//	{
//		_year = 2021;
//		_month = 12;
//		_day = 18;
//	}
//	Date(int year=2023, int month=10, int day=18)//全缺省参数的构造函数--->默认构造函数
//	{
//	_year = year;
//	_month = month;
//	_day = day;
//	}  //默认构造函数只能存在一个;
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//};
//int main()
//{
//	Date d1;
//	d1.print();
//	return 0;
//}


//类中用户没有显示定义构造函数,C++编译器自动生成一个无参的默认构造函数;
//当显示定义构造函数,C++编译器不再自动生成;
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//public:
//	//用户没有显示定义, 编译器自动生成的构造函数--->默认构造函数
//	//构造函数初始化数据成员时,对于内置类型(int float dobule 指针)的数据,不做任何处理--->内置类型的数据为随机值;
//	//构造函数初始化数据成员时,对于自定义类型(struct class union)的数据,调用类中的默认构造函数;
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//};
//int main()
//{
//	Date d1;
//	d1.print();
//	return 0;
//}

//默认生成构造函数:内置类型成员不做任何处理,自定义类型会调用自定义类型的默认构造;
//class Time
//{
//private:
//	int hour; 
//	int minute;
//	int second;
//public:
//	//显示定义构造函数
//	Time()
//	{
//		cout << "Time()" << endl;
//		int hour = 0;
//		int minute = 0;
//		int second = 0;
//	}
//};
//
//class Date
//{
//private:
//	int _year;
//	int _month;//内置类型--->_year _month _day
//	int _day;
//	Time _t;//自定义类型----> _t
//public:
//	void print()
//	  {
//		cout << _year << "-" << _month << "-" << _day << endl;
//	  }
//};
//int main()
//{
//	Date d;
//	d.print();
//	return 0;
//}


//默认构造函数对于内置类型成员不做任何处理,为弥补这种缺陷,C++11在类中声明时给缺省值;
//class Date
//{
//private:
//	int _year = 2023;
//	int _month = 12;//内置类型成员声明时给缺省值
//	int _day = 18;
//public:
//	/*Date(int year=1, int month=1, int day=1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//	void print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//};
//int main()
//{
//	Date d;
//	d.print();
//	return 0;
//}

//析构函数
//析构函数的作用是在对象生命周期结束时自动调用,用于完成对象中资源的清理工作,例如释放内存、关闭文件等操作;
//析构函数的特性
//析构函数的函数名: ~ 类名
//析构函数无参无返回值
//一个类只能有一个析构函数,若未显式定义，系统会自动生成默认的析构函数;   注意:析构函数不能重载
//class Stack
//{
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//public:
//	//默认构造函数
//	Stack(int capacity=4)
//	{
//		cout << "Stack()" << endl;
//		_a = (int*)malloc(sizeof(int)* capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc failed:");
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//	}
//	//析构函数
//	~Stack()
//	{
//		cout << "~stack()" << endl;
//		free(_a);
//		_a = nullptr;
//		_top = 0;
//		_capacity = 0;
//	}
//};

//int main()
//{
//	Stack st;
//	return 0;
//}

//析构函数对于内置类型成员不做任何处理
//对于自定义类型中的成员,清空那个类的对象中的数据就调用那个类的析构函数;
class Time
{
private:
	int _hour;
	int _minute;
	int _second;
public:
	~Time()
	{
		cout << "~Time" << endl;
	}
};
class Date
{
private:
	int _year;
	int _day;
	int _month;
	Time _t;
};
int main()
{
	Date d;
	return 0;
}