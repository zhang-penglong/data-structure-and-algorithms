# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>

void InsertSort(int* a, int n)
{
	//先写单趟，再写整体;
	for (int i = 0; i <n-1 ; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				//向后挪动一位
				a[end + 1] = a[end];
			}
			else
			{
				//考虑极端情况，当插入的数值小于整个有序数组，不断向前挪动过程中，直至end=-1;
				//出现插入的数值大于a[end];
				break;
			}
			end--;
		}
		a[end + 1] = tmp;
	 }
} 
int main()
{
	int arr[] = { 3, 2, 6, 7, 8, 1, 9 };
	int n = sizeof(arr) / sizeof(arr[0]);
	InsertSort(arr, n);
	return 0;
}