#pragma once
#include <iostream>
#include <vector>
using namespace std;

enum State
{
	 EMPTY,//此位置为空
	 EXIST,//此位置已经存放元素
	 DELETE//此位置元素已经删除
};


template<class K, class V>
struct HashData
{
	pair<K, V> _kv;
	State _state = EMPTY;
};

template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

template<>
struct HashFunc<string>
{
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (auto e : key)
		{
			hash += e;
			hash *= 31;//BKDR字符串哈希算法,累乘因子为31
		}
		return hash;
	}
};

template<class K,class V,class Hash=HashFunc<K>>
class HashTable
{
public:
	//哈希表最初长度为0,线性探测出现%0错误
	//利用构造函数,将最初长度设置为10并且size()与capacity()相等
	HashTable(size_t size = 10)
	{
		_tables.resize(size);
	}
	//哈希表一定存在空位置,要控制负载因子在0.7以下,当实际存放的数据增多时,会进行扩容操作
	HashData<K, V>* Find(const K& key)
	{
		Hash hs;
		size_t hashi = hs(key)%_tables.size();
	
		while (_tables[hashi]._state != EMPTY)
		{
			if (_tables[hashi]._kv.first == key && _tables[hashi]._state==EXIST)
			{
				return &_tables[hashi];
			}
			++hashi;
			hashi = hashi%_tables.size();
		}
		return nullptr;
	}
	//插入逻辑
	//插入的哈希数据键值key若在哈希表中已经存在,则插入失败返回false;
	//控制负载因子在0.7以下,即插入的数据量增多,需要进行扩容
	//线性探测合适的插入位置,插入数据,哈希表中存放数据的个数自增1并返回true;
	bool Insert(const pair<K, V>& kv)
	{
		//哈希表中查找插入的数据的键值key是否存在,存在则插入失败
		HashData<K, V>* ret = Find(kv.first);
		if (ret != nullptr)
		{
			return false;
		}
		//控制负载因子小于0.7
		if ((_n * 10) / _tables.size() >= 7)
		{
			//异地扩容:
			//思路1:
			//size_t newsize=_tables.size()*2;
			//开辟哈希表长度为newsize的新表
			//vector<HashData<K,V>> newtables(newsize);
			//遍历旧表,映射到新表(插入新表时所有数据重新映射,继续在新表中线性探测查找插入位置)
			//...
			//交换旧表与新表
			//_tables.swap(newtables);

			//思路2:(本质:复用哈希表中insert()中的线性探测)
			//创建<key,value>的哈希表,利用构造函数解决容量
			HashTable<K, V, Hash> newHT(_tables.size() * 2);
			//遍历旧表,插入到新表
			for (auto& e : _tables)
			{
				//取出旧表中状态存在的值插入到新表
				if (e._state == EXIST)
				{
					newHT.Insert(e._kv);
				}
			}
			//交换旧表与新表
			_tables.swap(newHT._tables);
		}
		//线性探测
		Hash hs;
		size_t hashi = hs(kv.first)%_tables.size();
		//由于哈希表底层结构采用vector,vector连续存储数据,有效数据的个数为_tables.size()
		//size_t hashi = kv._first%_tables.capacity();
		//数据集合虽然可以映射到[_tables.size(),_tables.capacity()]区间范围内,但无法插入到此段区间内,因为vector连续存储
		while (_tables[hashi]._state == EXIST)
		{
			++hashi;
			hashi = hashi%_tables.size();//走到哈希表末尾位置,返回起始位置查找
		}
		_tables[hashi]._kv = kv;
		_tables[hashi]._state = EXIST;

		++_n;
		return true;
	}
	
	//根据键值key查找元素,查找到将该元素的状态标记为DELETE
	//哈希表中记录实际存放数据的个数自减1
	bool Erase(const K& key)
	{
		HashData<K, V>* ret = Find(key);
		if (ret != nullptr)
		{
			ret->_state = DELETE;
			--_n;
			return true;
		}
		return false;
	}
private:
	vector<HashData<K,V>> _tables;
	size_t _n = 0;//记录哈希表中实际存放的数据个数
};

void TestHT1()
{
	int a[] = { 1, 4, 24, 34, 7, 44, 17, 37 };
	HashTable<int, int> ht;
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}

	for (auto e : a)
	{
		auto ret = ht.Find(e);
		if (ret)
		{
			cout << ret->_kv.first << ":E" << endl;
		}
		else
		{
			cout << ret->_kv.first << ":D" << endl;
		}
	}
	cout << endl;

	ht.Erase(34);
	ht.Erase(4);

	for (auto e : a)
	{
		auto ret = ht.Find(e);
		if (ret)
		{
			cout << ret->_kv.first << ":E" << endl;
		}
		else
		{
			cout << e << ":D" << endl;
		}
	}
	cout << endl;
}

//哈希表中HashData中存储string,除留余数法还能适用吗,string不支持取模


void TestHT2()
{
	HashTable<string, string,HashFuncString> dict;
	dict.Insert(make_pair("sort", "排序"));
	dict.Insert(make_pair("string", "字符串"));
}
























