# define  _CRT_SECURE_NO_WARNINGS 1

#include <memory>
#include <string>
#include <list>
#include <vector>
#include <iostream>
using namespace std;

double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	else
		return ((double)a / (double)b);
}

//template<class T>
//class SmartPtr
//{
//public:
//	//借助构造函数保存资源
//	SmartPtr(T* ptr)
//		:_ptr(ptr)
//	{}
//	//借助析构函数释放资源
//	~SmartPtr()
//	{
//		cout << "delete:" << _ptr << endl;
//		delete[] _ptr;
//	}
//private:
//	T* _ptr;
//};
//void Func()
//{
//	SmartPtr<int> sp1 = new int[10];
//	SmartPtr<int> sp2 = new int[20];
//	SmartPtr<int> sp3 = new int[30];
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << "exception errmsg:" << errmsg << endl;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	return 0;
//}


//template<class T>
//class SmartPtr
//{
//public:
//	//借助构造函数保存资源
//	SmartPtr(T* ptr)
//		:_ptr(ptr)
//	{}
//	//借助析构函数释放资源
//	~SmartPtr()
//	{
//		cout << "delete:" << _ptr << endl;
//		delete[] _ptr;
//	}
//private:
//	T* _ptr;
//};
//
//void Func()
//{
//	SmartPtr<int> sp1 = new int[10];
//	SmartPtr<int> sp2 = new int[20];
//	SmartPtr<int> sp3 = new int[30];
//}
//
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	catch (...)
//	{
//		cout << "unknow exception" << endl;
//	}
//	return 0;
//}


//template<class T>
//class SmartPtr
//{
//public:
//	//借助构造函数保存资源
//	SmartPtr(T* ptr)
//		:_ptr(ptr)
//	{}
//	//借助析构函数释放资源
//	~SmartPtr()
//	{
//		cout << "delete:" << _ptr << endl;
//		delete _ptr;
//	}
//	T& operator*()
//	{
//		return *_ptr;
//	}
//	T* operator->()
//	{
//		return _ptr;
//	}
//private:
//	T* _ptr;
//};
//
//int main()
//{
//	SmartPtr<int> sp1(new int(10));
//	SmartPtr<int> sp2(sp1);
//	return 0;
//}

//int main()
//{
//	SmartPtr<int> sp1(new int(0));
//	SmartPtr<int> sp2(new int(0));
//	*sp1 = 10;
//
//	SmartPtr<pair<string, int>> sp3(new pair<string, int>);
//	sp3->first = "apple";// <====> sp3.operator->()->first = "apple";
//	sp3->second = 1;//<====> sp3.operator->()->second = 1;
//	return 0;
//}


//int main()
//{
//	vector<int> v1 = { 1, 2, 3, 4, 5 };
//	vector<int> v2(v1);
//	return 0;
//}
//#include <memory>
//int main()
//{
//	std::auto_ptr<int> ap1(new int(10));
//	std::auto_ptr<int> ap2(ap1);
//
//	*ap1 = 20;//对象ap1管理的空间数值修改为20
//
//	return 0;
//}


namespace SmartPtr
{
	template<class T>
	class auto_ptr
	{
	public:

		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}
		
		~auto_ptr()
		{
			cout << "delete:" << _ptr << endl;
			delete _ptr;
		}
		//ap2(ap1)
		auto_ptr(auto_ptr<T>& ap)
		{
			_ptr = ap._ptr;
			ap._ptr = nullptr;
		}
		//ap2=ap1
		auto_ptr<T>& operator=(auto_ptr<T>& ap)
		{
			//检测是否为自己给自己赋值
			if (this != &ap)
			{
				//释放当前对象中的资源
				if (_ptr)
				{
					delete _ptr;
				}
				//转移ap中的资源到当前对象中
				_ptr = ap._ptr;
				ap._ptr = nullptr;
			}

			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
}
//void test()
//{
//	SmartPtr::auto_ptr<int> ap1(new int(10));
//	SmartPtr::auto_ptr<int> ap2(new int(20));
//	ap1 = ap2;
//
//}
//int main()
//{
//	test();
//	return 0;
//}


namespace SmartPtr
{
	template<class T>
	class unique_ptr
	{
	public:

		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		~unique_ptr()
		{
			cout << "delete:" << _ptr << endl;
			delete _ptr;
		}
		
		//禁用拷贝构造函数
		unique_ptr(const unique_ptr<T>& ap) = delete;
		
		//禁用拷贝赋值函数
		unique_ptr<T>& operator=(const unique_ptr<T>& ap) = delete;
		
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
}

//int main()
//{
//	//std::unique_ptr<int> ap1(new int(10));
//	//std::unique_ptr<int> ap2(ap1);// err: unique_ptr类的拷贝构造函数已删除
//
//	//SmartPtr::unique_ptr<int> ap1(new int(10));
//	//SmartPtr::unique_ptr<int> ap2(ap1);// err: unique_ptr类的拷贝构造函数已删除
//	return 0;
//}


//int main()
//{
//	std::shared_ptr<int> sp1(new int(1));
//	cout << sp1.use_count() << endl;
//
//	std::shared_ptr<int> sp2(sp1);
//	cout << sp1.use_count() << endl;
//
//	*sp2 += 10;
//	*sp1 += 10;
//
//	bit::shared_ptr<int> sp4(new int(5));
//
//	bit::shared_ptr<int> sp3(sp2);
//	cout << sp3.use_count() << endl;
//
//	sp4 = sp4;
//	sp1 = sp2;
//
//	sp1 = sp4;
//
//	return 0;
//}


//#include <memory>
//int main()
//{
//	std::shared_ptr<int> sp1(new int(1));
//	cout << sp1.use_count() << endl;//use_count()查看多少对象管理此资源
//
//	std::shared_ptr<int> sp2(sp1);
//	cout << sp1.use_count() << endl;
//
//	*sp1 += 10;
//	*sp2 += 10;
//
//	return 0;
//}


//template<class T>
//class shared_ptr
//{
//public:
//	//...
//private:
//	T* _ptr;
//	static size_t _count;//静态成员变量_count记录多少对象管理此资源
//};
////静态成员变量初始化
//template<class T>//类外需要模版参数,类外声明模版参数
//size_t shared_ptr<T>::_count = 0;

namespace SmartPtr
{
	template<class T>
	class shared_ptr
	{
	public:
		// RAII
		// 创建对象时才需要开辟计数空间并且将引用计数的值置为1
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pcount(new int(1))
		{}

		~shared_ptr()
		{
			// 析构时，--计数，计数减到0，
			// 说明最后一个管理对象销毁则可以释放资源
			if (--(*_pcount) == 0)
			{
				cout << "delete:" << _ptr << endl;
				delete _ptr;
				delete _pcount;
			}
		}

		// 拷贝构造 sp2(sp1)
		shared_ptr(const shared_ptr<T>& sp)
		{
			_ptr = sp._ptr;
			_pcount = sp._pcount;

			// 拷贝时++计数
			++(*_pcount);
		}
		
		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
		
			if (_ptr != sp._ptr)//防止case3的出现
			{
			
				//case2
				if (--(*_pcount) == 0)
				{
					cout << "delete:" << _ptr << endl;
					delete _ptr;
					delete _pcount;
				}
				_ptr = sp._ptr;
				_pcount = sp._pcount;

				// 拷贝时++计数
				++(*_pcount);
			}

			return *this;
		}

		// 行为类似指针
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		//获取原生指针
		T* get() const
		{
			return _ptr;
		}
		//获取引用计数
		int use_count()
		{
			return *_pcount;
		}
	private:
		T* _ptr;
		int* _pcount;
	};
}
//int main()
//{
//	SmartPtr::shared_ptr<int> sp1(new int(1));
//	SmartPtr::shared_ptr<int> sp2(sp1);
//	SmartPtr::shared_ptr<int> sp4(new int(5));
//	SmartPtr::shared_ptr<int> sp3(sp2);
//
//	sp4 = sp4;
//	sp1 = sp2;
//	sp1 = sp4;
//
//	return 0;
//}

namespace SmartPtr
{
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}

		// 拷贝构造 
		weak_ptr(const shared_ptr<T>& sp)
		{
			_ptr = sp.get();
		}
		weak_ptr(const weak_ptr<T>& wp)
		{
			_ptr = wp._ptr;
		}

		//operator=
		weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			_ptr = sp.get();
			return *this;
		}
		weak_ptr<T>& operator=(const weak_ptr<T>& wp)
		{
			_ptr = wp._ptr;
			return *this;
		}

	private:
		T* _ptr;
	};
}

struct ListNode
{
	int _data;

	SmartPtr::weak_ptr<ListNode> _next;
	SmartPtr::weak_ptr<ListNode> _prev;

	ListNode(int val=0)
		:_data(val)
	{}

	~ListNode()
	{
		cout << "~ListNode()" << endl;
	}
};
int main()
{
	SmartPtr::shared_ptr<ListNode> n1(new ListNode(10));
	SmartPtr::shared_ptr<ListNode> n2(new ListNode(20));

	n1->_next = n2;
	n2->_prev = n1;
	return 0;
}