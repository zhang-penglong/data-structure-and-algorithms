# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include<stdbool.h>

typedef struct
{
	int* nums;
	int head;
	int tail;
	int k;
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k)
{
	MyCircularQueue* ps = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
	//k+1=maxsize
	ps->nums = (int*)malloc(sizeof(ps->nums)*(k + 1));
	ps->head = ps->tail = 0;
	ps->k = k;

	return ps;
}
//判空
bool myCircularQueueIsEmpty(MyCircularQueue* obj)
{
	//tail=head 说明队列为空
	return obj->tail == obj->head;
}
//判满
bool myCircularQueueIsFull(MyCircularQueue* obj)
{
	//tail的下一个为head 说明队列已满 (tail+1)%(maxsize)==head;
	return (obj->tail + 1) % (obj->k + 1) == obj->head;
}

//入队列
bool myCircularQueueEnQueue(MyCircularQueue* obj, int value)
{
	//入队，先进行判满
	if (myCircularQueueIsFull(obj))
	{
		return false;
	}
	else
	{
		//队列未满,队尾指针tail总是指向队尾元素的下一个位置;
		obj->nums[obj->tail] = value;
		obj->tail++;
		//防止数组越界，有可能tail刚好处于队列元素已满的位置;
		obj->tail %= (obj->k + 1);
		return true;
	}
}
//出队列
bool myCircularQueueDeQueue(MyCircularQueue* obj)
{
	//判空
	if (myCircularQueueIsEmpty(obj))
	{
		return false;
	}
	else
	{
		//删除一个元素，只需要头指针head向后移动1步，空间不释放，重复利用；
		obj->head++;
		obj->head %= (obj->k + 1);
		return true;
	}
}
//获取队头元素
int myCircularQueueFront(MyCircularQueue* obj)
{
	//队列为空返回-1;
	if (myCircularQueueIsEmpty(obj))
	{
		return -1;
	}
	//队头指针始终指向对头元素的位置;
	return obj->nums[obj->head];
}
//获取队尾元素
int myCircularQueueRear(MyCircularQueue* obj)
{
	//队列为空返回-1;
	if (myCircularQueueIsEmpty(obj))
	{
		return -1;
	}
	//队尾指针tail始终指向队尾元素的下一个位置;
	//寻找队尾元素的方法：由于循环队列的长度(包含多出来的一个位置)为maxsize=k+1;
	//尾指针向后走K+1,相当于绕循环队列一圈,回到尾指针原先位置;
	//尾指针向后走k步，相当于回到尾指针原先位置的上一个位置,相当于队尾元素的位置;
	//为了实现循环，只需要%maxsize即可;
	return obj->nums[(obj->tail + obj->k) % (obj->k + 1)];
}

//销毁循环队列
void myCircularQueueFree(MyCircularQueue* obj)
{
	free(obj->nums);
	free(obj);
}
