# define  _CRT_SECURE_NO_WARNINGS 1
# include "contact.h"
void menu()
{
	printf("********************************\n");
	printf("****1.Add   *****  2.Del********\n");
	printf("****3.Search ****  4.Modify*****\n");
	printf("****5.Show  ***** 6.Sort********\n");
	printf("******* 0.Exit  ****************\n");
}

void test()
{
	int input = 0;
	//创建通讯录
	Contact con;
	InitContact(&con);
	do
	{
		menu();
		printf("请输入:>\n");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			AddContact(&con);
			break;
		case DEL:
			DelContact(&con);
			break;  
		case SEARCH:
			SearchContact(&con);
			break;
		case MODIFY:
			ModifyContact(&con);
			break;
		case SHOW:
			ShowContact(&con);
			break;
		case SORT:
			break;
		case EXIT:
			printf("退出通讯录\n");
			break;
		default:
			printf("输入错误，请重新输入!\n");
			break;
		}
	}
	while (input);
}


int main()
{
	test();
	return 0;
}