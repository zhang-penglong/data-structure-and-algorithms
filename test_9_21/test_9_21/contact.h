# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <string.h>
# include <assert.h>

# define DATA_MAX 100
# define NAME_MAX 20
# define SEX_MAX  5
# define TELE_MAX 12
# define ADDR_MAX 30

enum OPTION
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	SORT
};
//类型的声明以及头文件的应用
typedef struct	PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char tele[TELE_MAX];
	char addr[ADDR_MAX];
}PeoInfo;
typedef struct Contact
{
	PeoInfo data[DATA_MAX];
	int sz;//记录当前通讯录中存在的元素个数
	//对于通讯录中元素的增删查改都会改变元素个数，需要记录;
}Contact;

//初始化通讯录
void InitContact(Contact* pc);

//增加联系人

void AddContact(Contact* pc);

//显示通讯录中的信息                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

void ShowContact(Contact* pc);

//删除指定联系人的信息

void DelContact(Contact* pc);

//查找指定联系人的信息
 
void SearchContact(const Contact* pc);

//修改指定联系人的信息

void ModifyContact(Contact * pc);



