# define  _CRT_SECURE_NO_WARNINGS 1
# include "contact.h"

//结构体初始化
void InitContact(Contact* pc)
{
	assert(pc != NULL);
	//使用memset()函数初始化结构体数组
	memset(pc->data, 0, sizeof(pc->data));
	pc->sz = 0;
}


void AddContact(Contact* pc)
{
	assert(pc != NULL);
	if (pc->sz == DATA_MAX)
	{
		printf("通讯录已满，无法添加\n");
		return;
	}
	//添加联系人信息
	printf("请输入姓名: ");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入年龄: ");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入性别: ");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入电话号码: ");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入家庭住址: ");
	scanf("%s", pc->data[pc->sz].addr);

	pc->sz++;
	printf("成功添加联系人\n");
}

void ShowContact(Contact* pc)
{
	assert(pc != NULL);
	int i = 0;
	//printf()函数的用法
	printf("%-10s\t%-4s\t%-5s\t%-12s\t%-20s\n", "姓名", "年龄", "性别", "电话号码", "家庭住址");
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-10s\t%-4d\t%-5s\t%-12s\t%-20s\n",
			pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].tele,
			pc->data[i].addr);
	}
}
int FindByName(const Contact * pc, char* name)
{
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
		{
			return i;
			break;
		}
	}
	//要删除的联系人找不到
	return -1;
}
void DelContact(Contact* pc)
{
	assert(pc != NULL);
	if (pc->sz == 0)
	{
		printf("通讯录为空，无法删除\n");
		return;
	}
	//删除通讯录中的联系人
	char name[NAME_MAX];
	printf("请输入要删除的联系人的姓名:");
	scanf("%s", name);
	//查找联系人-每个模块均用到，进行改造
	//int pos = 0;
	//int i = 0;
	//int flag = 0;
	//for (i = 0; i < pc->sz; i++)
	//{
	//	if (strcmp(pc->data[i].name, name)==0)
	//	{
	//		pos = i;
	//		flag = 1;
	//		break;
	//	}
	//}
	////要删除的联系人找不到
	//if (flag == 0)
	//{
	//	printf("该联系人不存在\n");
	//	return;
	//}
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("该联系人不存在\n");
		return;
	}
	int i = 0;
	//查找到指定联系人，从该位置开始不断向前覆盖数据，直到最后一个数据
	for (i = pos; i < pc->sz-1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("成功删除指定联系人\n");
}

void SearchContact(const Contact* pc)
{
	char name[NAME_MAX];
	printf("请输入查找联系人的姓名:");
	scanf("%s", name);
	int pos=FindByName(pc, name);
	if (pos == -1)
	{
		printf("该联系人不存在\n");
	}
	else
	{
		printf("%-10s\t%-4s\t%-5s\t%-12s\t%-20s\n", "姓名", "年龄", "性别", "电话号码", "家庭住址");
	
			printf("%-10s\t%-4d\t%-5s\t%-12s\t%-20s\n",
				pc->data[pos].name,
				pc->data[pos].age,
				pc->data[pos].sex,
				pc->data[pos].tele,
				pc->data[pos].addr);
	}
}

void ModifyContact(Contact * pc)
{
	char name[NAME_MAX];
	printf("请输入修改联系人的姓名:");
	scanf("%s", name);
	int pos = FindByName(pc, name);
	if (pos == -1)
	{
		printf("该联系人不存在\n");
	}
	else
	{
		printf("请输入姓名: ");
		scanf("%s", pc->data[pos].name);
		printf("请输入年龄: ");
		scanf("%d", &(pc->data[pos].age));
		printf("请输入性别: ");
		scanf("%s", pc->data[pos].sex);
		printf("请输入电话号码: ");
		scanf("%s", pc->data[pos].tele);
		printf("请输入家庭住址: ");
		scanf("%s", pc->data[pos].addr);

	}



}










