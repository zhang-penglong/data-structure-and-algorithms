# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <assert.h>
# include <stdlib.h>
# include <time.h>

//void swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
////向下调整算法
//void AdjustDown(int* a, int n, int parent)
//{
//	int child = 2 * parent + 1;
//	while (child<n)
//	{
//		if (child+1 < n && a[child] > a[child + 1])
//		{
//			child++;
//		}
//		//child为左右孩子较小的一个;
//		if (a[child] < a[parent])
//		{
//			//孩子结点处的数值小于父亲结点的数值,交换;
//			swap(&a[child], &a[parent]);
//			parent = child;
//			child = 2 * child + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
////堆排序-降序-建小堆
//void HeapSort(int* a, int n)
//{
//	//向下调整建堆的时间复杂度O(N)优于向上调整建堆的时间复杂度O(N*logN);
//	//建堆
//	for (int i = (n - 1 -1) / 2; i >= 0; i--)
//	{
//		AdjustDown(a, n, i);
//	}
//	//小堆-交换根节点与尾结点处的数值;
//	//向下调整为堆，继续交换，直到整个数组结束;
//	int end = n - 1;
//	while (end >= 0)
//	{
//		swap(&a[0], &a[end]);
//		AdjustDown(a, end, 0);
//		end--;
//	}
//}
//int main()
//{
//	int arr[] = { 1, 3, 7, 9, 23, 22, 13, 100, 70, 50 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	HeapSort(arr, sz);
//	for (int j = 0; j < sz; j++)
//	{
//		printf("%d ", arr[j]);
//	}
//	return 0;
//}

//Top-K问题:求取一组数据中前K个最大的元素或者最小的元素
//1.数据集合前K个元素建堆
//前K个最大的元素，则建小堆;
//前K个最小的元素，则建大堆;
//2.剩余的N-K个元素依次与堆顶元素比较,不满足则替换堆顶元素;

// 造数据
void CreateNData()
{
	int n = 100;
	srand(time(0));
    const char* file = "data.txt";
	FILE* fin = fopen(file, "w");
	if (fin == NULL)
	{
		perror("fopen error");
		return;
	}

	for (int i = 0; i < n; ++i)
	{
		int x = rand() % 100;
		fprintf(fin, "%d\n", x);
	}

	fclose(fin);
}
//交换函数
void swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
//向下调整算法
void AdjustDown(int* a, int n, int parent)
{
	int child = 2 * parent + 1;
	while (child<n)
	{
		if (child+1 < n && a[child] > a[child + 1])
		{
			child++;
		}
		//child为左右孩子较小的一个;
		if (a[child] < a[parent])
		{
			//孩子结点处的数值小于父亲结点的数值,交换;
			swap(&a[child], &a[parent]);
			parent = child;
			child = 2 * child + 1;
		}
		else
		{
			break;
		}
	}
}

void PrintTopK(const char* filename,int k)
{
	//打开文件
	FILE* pf = fopen(filename, "r");
	if (pf == NULL)
	{
		perror("fopen failed:");
		exit(-1);
	}
	//前K个数据建堆
	int* a = (int*)malloc(sizeof(int)*k);
	if (a == NULL)
	{
		perror("malloc failed:");
		exit(-1);
	}
	for (int i = 0; i < k; i++)
	{
		fscanf(pf, "%d", &a[i]);
	}
	//向下调整建堆
	for (int i = (k - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, k, i);
	}
	//以第k+1个数据为起点，依次比较进堆，直至所有数据读取结束;
	int x = 0;//将数据读取到x里;
	while (fscanf(pf, "%d", x) != EOF)
	{
		if (x > a[0])
		{
			a[0] = x;
			AdjustDown(a, k, 0);
		}
	}
	for (int j = 0; j < k; j++)
	{
		printf("%d ", a[j]);
	}
	printf("\n");

	free(a);
	fclose(pf);

}
int main()
{
	//CreateNData();
	PrintTopK("data.txt", 5);
	return 0;
}









