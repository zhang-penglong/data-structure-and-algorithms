
# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>
# include <stdlib.h>

//qsort()函数默认为升序

//排序整型数组
//int cmp_int(const void* e1, const void* e2)
//{
//	if ((*(int*)e1) > (*(int*)e2))
//	{
//		return 1;
//	}
//	else if ((*(int*)e1) < (*(int*)e2))
//	{
//		return -1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//void print(int* p,int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//}
//int main()
//{
//	int arr[5] = {1,3,2,7,6};
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr,sz);
//	                  
//	return 0;
//}

//排序字符型数组
//int cmp_char(const void* e1, const void* e2)
//{
//	if ((*(char*)e1) > (*(char*)e2))
//	{
//		return 1;
//	}
//	else if ((*(char*)e1) < (*(char*)e2))
//	{
//		return -1;
//	}
//	else
//		return 0;
//}

//int cmp_char(const void* e1, const void* e2)
//{
//	return (*((char*)e1)) - (*((char*)e2));
//}
//void print(char* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%c ", *(p + i));
//	}
//}
//int main()
//{
//	char arr[5] = { 'f', 'b', 'd', 'c', 'e'};
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_char);
//	print(arr, sz);
//	return 0;
//}

//排序浮点型数组
//int cmp_float(const void* e1, const void* e2)
//{
//	if (*((float*)e1) > *((float*)e2))
//	{
//		return 1;
//	}
//
//	else if (*((float*)e1) < *((float*)e2))
//	{
//		return -1;
//	}
//	else
//	return 0;
//}
//void print(float* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%f ", *(p + i));
//	}
//}
//int main()
//{
//	float arr[5] = { 7.2f, 2.3f, 3.4f, 3.8f, 5.5f };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_float);
//	print(arr, sz);
//	return 0;
//}

//排序结构体类型的数据
//# include <string.h>
//struct stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//int cmp_by_name(const void* e1, const void* e2)
//{
//	return (strcmp(((struct stu *)e1)->name, ((struct stu *)e2)->name));
//}
//int main()
//{
//	struct stu s[3] = { { "刘波", 20, "男" }, { "张非", 30, "男" }, { "刘波文", 18, "女" } };
//	int sz = sizeof(s) / sizeof(s[0]);
//	qsort(s, sz, sizeof(s[0]), cmp_by_name);
//	printf("%s %d %s\n", s[0].name, s[0].age, s[0].sex);
//	printf("%s %d %s\n", s[1].name, s[1].age, s[1].sex);
//	printf("%s %d %s\n", s[2].name, s[2].age, s[2].sex);
//	return 0;
//}
//# include <string.h>
//# include <assert.h>
//
//void* my_memcpy(void* dest, const void* src, int num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	void* ret = dest;
//	while (num)
//	{
//		*((char*)dest) = *((char*)src);
//		(char*)dest = (char*)dest + 1;
//		(char*)src = (char*)src + 1;
//		num = num - 1;
//	}
//	return ret;
//
//
//}
//int main()
//{
//	int arr1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int arr2[10] = { 0 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	my_memcpy(arr1, arr2, 20);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//memove函数的模拟实现

//# include <string.h>
//# include <assert.h>
//void* my_memmove(void* dest, void* src, int num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	void* ret = dest;
//	if (src < dest)
//	{
//		while (num)
//		{
//			*((char*)dest + num-1) = *((char*)src + num-1);
//			num = num - 1;
//		}
//	}
//	else
//		{
//		while (num)
//		{
//			*((char*)dest) = *((char*)src);
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//			num = num - 1;
//		}
//		}
//	return ret;
//	}
//
//int main()
//{
//	int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	my_memmove(&arr[0], &arr[2], 20);
//
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//# include <string.h>
//# include <assert.h>
//int my_memcmp(void* ptr1, void* ptr2, int num)
//{
//	assert(ptr1 != NULL);
//	assert(ptr2 != NULL);
//
//	while (num)
//	{
//		if (*((char*)ptr1) != *((char*)ptr2))
//		{
//			return *((char*)ptr1) - *((char*)ptr2);
//		}
//		ptr1 = (char*)ptr1 + 1;
//		ptr2 = (char*)ptr2 + 1;
//		num = num - 1;
//	}
//
//	return 0;
//}
//int main()
//{
//	char arr1[] = "abbdef";
//	char arr2[] = "abc";
//	int ret=my_memcmp(arr1, arr2, 3);
//	printf("%d ", ret);
//	return 0;
//}































