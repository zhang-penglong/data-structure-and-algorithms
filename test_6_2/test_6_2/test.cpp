# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

///////cin从第一个非空白字符(空格 制表符 换行符)开始读取,当遇到下一个空白字符停止读取/////////////////

/*int main()
{
	string str;
	printf("输入:\n");
	cin >> str;
	printf("输出:\n");
	cout << str;
	system("pause");
	return 0;
}
*/

////////cin读取字符时会忽略空白字符,使用getline函数可以读取整行,并将结果存储于string对象中////////////////
//////// istream& getline(istream& cin, string& str);   /////////////////////////////////////////////////
//
//int main()
//{
//	string str;
//	printf("输入:\n");
//	getline(cin, str);
//	printf("输出:\n");
//	cout << str;
//	system("pause");
//	return 0;
//}

//////////////////////////////////柔性数组////////////////////////////////////////////

//1.数组成员不占结构体内存空间
//2.数组成员必须为结构体中最后一个成员

//struct Test
//{
//	int size;
//	int* ptr;
//	int a[0];
//};


//struct Test
//{
//  int a[0];
//};
//int main()
//{
//	struct Test t;
//	printf("%d\n", sizeof(struct Test));
//	return 0;
//}

////结构体中动态开辟数组空间
//typedef struct test
//{
//	int size;//数组元素的个数
//	int* ptr;//数组的起始地址
//}test;
//
//int main()
//{
//	int size = 10;
//	struct test* pt = (struct test*)malloc(sizeof(test));//结构体空间
//	pt->size = size;
//	pt->ptr = (int*)malloc(sizeof(int)*size);//数组空间=元素类型*元素个数
//	//....
//	free(pt->ptr);
//	free(pt);
//	return 0;
//}
////缺陷:动态开辟空间两次,容易导致内存泄漏,造成内存碎片


//结构体中动态开辟数组空间
//typedef struct test
//{
//	int size;//数组中元素个数
//	int arr[0];//类型:int* arr (注意:不占任何空间)
//}test;
//
//int main()
//{
//	int size = 10;
//	struct test* ptr = (struct test*)malloc(sizeof(test)+sizeof(int)*size);
//	ptr->size = size;
//	//...
//	free(ptr);
//	return 0;
//}

//int main()
//{
//	ofstream fout("./test.txt",ios::app);//1.创建文件输出流对象 && 2.打开文件
//	//ofstream(); 无参构造
//	//ofstream(const char* filename, ios_base::openmode mode = ios_base::out); 有参构造
//	//c++文件打开的两种方式
//	//方式一:调用ofstream类的有参构造函数打开文件,默认打开方式为ios::out
//	//       ios::out 以只写的方式打开文件,若文件不存在则创建一个新文件,若文件存在则将文件内容清空,再进行写入
//	//       ios::app 以追加的方式打开文件,若文件不存在则创建一个新文件
//	
//	//ofstream类的成员函数is_open()检测文件打开是否成功 文件打开成功返回true,打开失败返回false
//	if (fout.is_open())
//	{
//		//文件打开成功
//		//3.向文件中写入数据
//		fout << "apple:苹果" << endl;
//		fout << "banana:香蕉" << endl;
//		fout << "orange:橘子" << endl;
//		fout << "grape:葡萄" << endl;
//
//		//fout << "courage:勇气" << endl;
//		//fout << "persistence:坚持" << endl;
//	}
//	
//	//4.关闭文件
//	fout.close();
//	return 0;
//}

//const string filename = "./data.txt";
//int main()
//{
//	ofstream fout;//1.创建文件输出流对象
//	//ofstream(); 无参构造
//	
//	//方式二:调用open()函数打开文件
//	//void open (const char* filename,  ios_base::openmode mode = ios_base::out);//c++98
//	//void open (const string& filename,  ios_base::openmode mode = ios_base::out);//c++11
//	//2.打开文件
//	fout.open(filename.c_str(), ios::app);
//
//	//3.is_open()检测文件打开是否成功 
//	//bool is_open() const;
//	if (fout.is_open())
//	{
//		//文件打开成功
//		//4.向文件中写入数据
//		fout << "apple:苹果" << endl;
//		fout << "banana:香蕉" << endl;
//		fout << "orange:橘子" << endl;
//		fout << "grape:葡萄" << endl;
//		fout.write("Hello Linux!", sizeof("Hello Linux!"));
//	}
//	//5.关闭文件
//	fout.close();
//	return 0;
//}


//int main()
//{
//	ifstream fin("./test.txt",ios::in);//1.创建文件输出流对象 && 2.打开文件
//	// ifstream();
//	// ifstream(const char* filename, ios_base::openmode mode = ios_base::in);
//
//	//c++文件打开的两种方式
//	//方式一:调用ofstream类的有参构造函数打开文件,默认打开方式为ios::in
//	//       ios::in 以只读的方式打开文件,若文件不存在则出错返回
//	   
//	
//	//ofstream类的成员函数is_open()检测文件打开是否成功 文件打开成功返回true,打开失败返回false
//	if (fin.is_open())
//	{
//		//文件打开成功
//		//3.从文件中读取数据
//		string line;
//		while (getline(fin,line))
//		{
//			cout << line <<endl;
//		}
//	}
//	//4.关闭文件
//	fin.close();
//	return 0;
//}


const string filename = "./data.txt";
int main()
{
	// 1.创建文件输出流对象
	ifstream fin;
	// ifstream();
	// ifstream(const char* filename, ios_base::openmode mode = ios_base::in);

	// 2.打开文件
	fin.open(filename.c_str(), ios::in);
	// 3.is_open()检测文件打开是否成功 文件打开成功返回true,打开失败返回false
	if (fin.is_open())
	{
		//文件打开成功
		//4 .从文件中读取数据
		string line;
		while (true)
		{
			fin >> line;
			if (fin.eof())
			{
				break;
			}
			cout << line << endl;
		}
	}
	//5.关闭文件
	fin.close();
	return 0;
}

















