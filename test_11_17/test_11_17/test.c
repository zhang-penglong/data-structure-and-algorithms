# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>

//直接插入排序
void InsertSort(int* arr, int n)
{
	for (int i = 0; i < n-1; i++)
	{
		//单趟排序
		int end = i;
		int tmp = arr[end + 1];
		while (end >= 0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
			{
				break;
			}
		}
		//end=-1时，arr[0]=tmp;
		arr[end + 1] = tmp;
	}
	
}
void PrintArray(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
//希尔排序
void ShellSort(int* arr, int n)
{

	int gap = n;
	//预排序:分组排序,间距为gap分为一组,共计gap组;
	//最后进行直接插入排序，即gap==1;
	while (gap > 1)
	{
		gap = gap / 2;
		//其次排序其余gap组,一次预排序完成,会进行多次预排序;
		for (int j = 0; j < gap; j++)
		{
			//首先排序以第一个元素为起点,间距为gap的第一组;相当于整个数组排序;
			for (int i = j; i < n - gap; i += gap)
			{
				//保证[0,end]有序,控制[0,end+1]有序;
				int end = i;
				int tmp = arr[end + gap];
				while (end >= 0)
				{
					if (tmp < arr[end])
					{
						arr[end + gap] = arr[end];
						end = end - gap;
					}
					//else会出现俩种情况;
					//第一种为第一个元素以后且间距为gap的某个位置插入;
					//第二种会不断满足tmp<arr[end],直至end=-gap;
					else
					{
						break;
					}
					//end=-gap的位置，直接将arr[end+gap]=tmp;
					arr[end + gap] = tmp;
				}
			}
		}
	}
}
//冒泡排序
void swap(int* p, int* q)
{
	int tmp = *p;
	*p = *q;
	*q = tmp;
}
void BubbleSort(int* arr, int n)
{
	//确定趟数,n个元素需要n-1趟;
	for (int i = 0; i < n; i++)
	{
		//单趟排序(升序)
		for (int j = 0; j <n - 1-i; j++)
		{
			if (arr[j]>arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}

}
//直接选择排序
//遍历整个数组,选出最小元素和最大元素,将最小元素置于左边,最大元素置于右边;
//即交换最小元素和第一个元素的位置，最大元素和最后一个元素的位置;

void SelectSort(int* arr, int n)
{
	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		//单趟排序,将[begin,end]范围内最大与最小的数据调整于左右;
		int maxi = begin;//最大元素的下标,假设最大元素在最左边;
		int mini = begin;//最小元素的下标,假设最小元素在最左边;
		for (int i = begin+1; i <=end; i++)
		{
			if (arr[i]>arr[maxi])
			{
				maxi = i;
			}
			if (arr[i]<arr[mini])
			{
				mini = i;
			}
		}
		swap(&arr[begin], &arr[mini]);
		if (maxi == begin)
		{
			maxi = mini;
		}
		swap(&arr[end], &arr[maxi]);
		end--;
		begin++;
	}

}
//堆排序
//堆的向下调整算法
void AdjustDown(int* nums, int n, int parent)
{
	//假设法求同一深度左右孩子最小的一个,假设左孩子为最小
	int child = parent * 2 + 1;
	while (child<n)
	{
		if (child + 1 < n && nums[child] < nums[child + 1])
		{
			child++;
		}
		//无论左右孩子,child为最大,调整为大堆;
		if (nums[child] > nums[parent])
		{
			swap(&nums[child], &nums[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapSort(int* arr, int n)
{
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(arr, n, i);
	}
	int end = n - 1;
	while (end > 0)
	{
		swap(&arr[0], &arr[end]);
		AdjustDown(arr, end, 0);
		end--;
	}
}
int main()
{
	int arr[] = { 9, 1, 2, 5, 7, 4, 8, 6, 3, 5, 1, 2, 3, 5, 1, 8, 3 };
	int n = sizeof(arr) / sizeof(arr[0]);
	/*InsertSort(arr, n);
	PrintArray(arr, n);*/
	/*ShellSort(arr, n);
	PrintArray(arr, n);*/
	/*BubbleSort(arr, n);
	PrintArray(arr, n);*/

	/*SelectSort(arr, n);
	PrintArray(arr, n);*/

	HeapSort(arr, n);
	PrintArray(arr, n);

	return 0;
}
























