# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>

//函数递归
//int main()
//{
//	printf("hehe\n");
//
//	main();
//
//	return 0;
//}

//只需要打印，不需要返回值
//void print(unsigned int x)
//{
//	if (x > 9)
//	{
//		print(x / 10);//剥离n位数的前n-1位
//	}
//	printf("%d ", x % 10);//打印最后一位
//}
//int main()
//{
//	unsigned int num = 0;
//	scanf("%d", &num);
//	print(num);
//	return 0;
//}
//int my_strlen(char* s)
//{
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}

//int my_strlen(char* s)
//{
//
//	if (*s == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(s + 1);
//}
//int main()
//{
//	char arr[] = "abc";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}
//迭代解法
//前两个斐波那契数不用计算，计算第n个斐波那契数即程序执行n-2次
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;	
//
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	unsigned char a = 5;
//	//00000000000000000000000000000101
//	//截断：00000101
//	unsigned char b = 127;
//	//00000000000000000000000001111111
//	//截断后存储:01111111
//	unsigned char c = a + b;
//	//a+b 00000000 00000000 00000000 10000100
//	//截断后存储：100001000
//	printf("%d\n", c);
//
//
//
//	return 0;
//}
//机器大小端的判断
//int main()
//{
//	int a = 1;
//	char* p = (char*)&a;
//	if (*p == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}
 
//int main()
//{
//	int arr1[] = { 1, 2, 3, 4, 5 };
//	int arr2[] = { 2, 3, 4, 5, 6 };
//	int arr3[] = { 3, 4, 5, 6, 7 };
//	int* parr[] = { arr1, arr2, arr3 };
//	int sz = sizeof(parr) / sizeof(parr[0]);
//	int i = 0;
//	int k = sizeof(arr1) / sizeof(arr1[0]);
//	for (i = 0; i < sz; i++)
//	{
//		int j = 0;
//		for (j = 0; j < k; j++)
//		{
//			printf("%d ", *(parr[i] + j));
//			//printf("%d ", parr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//void print(int(*p)[5], int row, int col)
//{
//	int i = 0;
//	for (i = 0; i < row; i++)
//	{
//		int j = 0;
//		for (j = 0; j < col; j++)
//		{
//			//printf("%d ", *(*(p + i) + j));
//			printf("%d ", p[i][j]);
//		}
//		printf("\n");
//	}
//
//}
//int main()
//{
//	int arr[3][5] = { { 1, 2, 3, 4, 5 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 } };
//	print(arr, 3, 5);
//	return 0;
//}

//void test()
//{
//	printf("hehe\n");
//}
//int main()
//{
//	printf("%p\n", test);
//	printf("%p\n", &test);
//
//	return 0;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int(*pf)(int, int) = Add;
//	int ret=pf(2, 3);
//	int call = (*pf)(3, 5);
//
//	printf("%d ", ret);
//	printf("%d ", call);
//
//	return 0;
//}

//void menu()
//{
//	printf("***********************\n");
//	printf("**1.Add      2.Sub*****\n");
//	printf("**3.Mul      3.Div*****\n");
//	printf("******* 0.Exit ********\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int ret = 0;
//	int input = 0;
//	int n = 0;
//	int m = 0;
//	do
//	{
//		menu();
//		printf("请输入选项\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 0:
//			printf("退出计算器\n");
//			break;
//		case 1:
//			printf("请输入两个操作数\n");
//			scanf("%d%d", &n, &m);
//			ret=Add(n,m);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数\n");
//			scanf("%d%d", &n, &m);
//		    ret=Sub(n,m);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数\n");
//			scanf("%d%d", &n, &m);
//			ret=Mul(n,m);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数\n");
//			scanf("%d%d", &n, &m);
//			ret=Div(n,m);
//			printf("%d\n", ret);
//			break;
//		default:
//			printf("输入错误，请重新输入!\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//void menu()
//{
//	printf("***********************\n");
//	printf("**1.Add      2.Sub*****\n");
//	printf("**3.Mul      3.Div*****\n");
//	printf("******* 0.Exit ********\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int ret = 0;
//	int input = 1;
//	int n = 0;
//	int m = 0;
//	
//	int(*parr[5])(int, int) = { 0, Add, Sub, Mul, Div };
//	while (input)
//	{
//		menu();
//		printf("请输入选项\n");
//		scanf("%d", &input);
//		if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数\n");
//			scanf("%d%d", &n, &m);
//			ret = (*parr[input])(n, m);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("输入错误，请重新输入!!!\n");
//		}
//
//	}
//	return 0;
//}




