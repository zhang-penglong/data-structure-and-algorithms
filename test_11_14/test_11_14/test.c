# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <stdbool.h>

//创建二叉树
typedef int BTDataType;
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
	BTDataType val;
}BTNode;

# include "Queue.h"
BTNode* CreateTree(char* str,int n,int* pi)
{
	if (str[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}
	BTNode* root = (BTNode*)malloc(sizeof(BTNode));
	root->val = str[*pi];
	(*pi)++;
	root->left = CreateTree(str, n, pi);
	root->right = CreateTree(str, n, pi);

	return root;
}
//二叉树的销毁
void BinaryTreeDestory(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeDestory(root->left);
	BinaryTreeDestory(root->right);
	free(root);
}
//二叉树结点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	return BinaryTreeSize(root->left) + BinaryTreeSize(root->right) + 1;
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->left == NULL && root->right == NULL)
		return 1;
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
	
}
// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	assert(k > 0);
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	return  BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}
//二叉树的高度
int BinaryTreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int LeftHeight = BinaryTreeHeight(root->left);
	int RightHeight = BinaryTreeHeight(root->right);
	return LeftHeight > RightHeight ? LeftHeight + 1 : RightHeight + 1;
}
// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->val == x)
		return root;
	struct BinaryTreeNode* ret = NULL;
	ret = BinaryTreeFind(root->left, x);
	if (ret != NULL)
		return ret;
	ret = BinaryTreeFind(root->right, x);
	if (ret != NULL)
		return ret;
	return NULL;
}
//二叉树的前序遍历
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%c ", root->val);
	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}
//二叉树的中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeInOrder(root->left);
	printf("%c ", root->val);
	BinaryTreeInOrder(root->right);
}
//二叉树的后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePostOrder(root->left);
	BinaryTreePostOrder(root->right);
	printf("%c ", root->val);
}

//二叉树的层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	InitQueue(&q);
	if (root != NULL)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		printf("%c ", front->val);
		if (front->left != NULL);
		    QueuePush(&q, front->left);

		if (front->right != NULL)
			QueuePush(&q, front->right);

		QueuePop(&q);
	}
	DestroyQueue(&q);
}
//判断二叉树是否是完全二叉树,是完全二叉树返回true,否则返回false;
//利用二叉树层序遍历，非空结点连续为完全二叉树，非空结点不连续，存在空节点,则为非完全二叉树;
int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	InitQueue(&q);
	if (root != NULL)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		if (front == NULL)
			break;
		QueuePush(&q, front->left);
		QueuePush(&q, front->right);
		QueuePop(&q);
	}
	//若已经出现空结点，队列后面结点存在非空结点，则为非完全二叉树
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);

		if (front != NULL)
		{
			DestroyQueue(&q);
			return false;
		}
	}
	DestroyQueue(&q);
	return true;
}
int main()
{
	char str[] = "ABD##E#H##CF##G##";
	int n = sizeof(str) / sizeof(str[0]);

	int j = 0;
	BTNode* root = CreateTree(str, n, &j);

	int BTSize = BinaryTreeSize(root);
	printf("BTSize=%d ", BTSize);

	int BTLeafSize = BinaryTreeLeafSize(root);
	printf("BTLeafSize=%d ", BTLeafSize);

	int BTKSize = BinaryTreeLevelKSize(root,3);
	printf("BTKSize=%d ", BTKSize);

	int BTHeight =BinaryTreeHeight(root);
	printf("BTHeight=%d ", BTHeight);

	BTNode* ret=BinaryTreeFind(root, 'G');
	printf("ret=%p\n", ret);

	BinaryTreePrevOrder(root);
	printf("\n");

	BinaryTreeInOrder(root);
	printf("\n");

	BinaryTreePostOrder(root);
	printf("\n");

	BinaryTreeLevelOrder(root);
	printf("\n");

	printf("TreeComplete=%d\n", BinaryTreeComplete(root));

	BinaryTreeDestory(root);
	return 0;
}






















