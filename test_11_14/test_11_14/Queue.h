# include<stdio.h>
# include<assert.h>
# include <stdlib.h>
# include <stdbool.h>

typedef struct BinaryTreeNode* QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QueueNode;

typedef struct Queue
{
	QueueNode* head;
	QueueNode* tail;
	int size;
}Queue;
//初始化队列
void InitQueue(Queue* ps);

//销毁队列
void DestroyQueue(Queue* ps);

//入队列
void QueuePush(Queue* ps, QDataType x);

//出队列
void QueuePop(Queue* ps);

//取队头元素
QDataType QueueFront(Queue* ps);

//取队尾元素
QDataType QueueBack(Queue* ps);

//队列判空
bool QueueEmpty(Queue* ps);

//队列的长度
int QueueLength(Queue* ps);