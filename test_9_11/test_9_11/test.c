# define  _CRT_SECURE_NO_WARNINGS 1

# include <stdio.h>

//int main()
//{
//	char arr1[] = "bit";
//	char arr2[] = { 'b', 'i', 't' };
//	char arr3[] = { 'b', 'i', 't', '\0' };
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//	return 0;
//}
//int main()
//{
//	int i = 0;
//	//int b = 0;
//	//b=i++;
//
//	//printf("%d ", b);
//	++i;
//	
//	printf("%d ", i);
//	return 0;
//}

//void test()
//{
//	static int i = 0;
//	i++;
//	printf("%d ", i);
//
//}
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		test();
//
//	}
//	return 0;
//}

//1 2 3 4 5 6 7 8 9 10
//extern int global;
//int main()
//{
//	printf("%d\n", global);
//	return 0;
//}
//extern int Add(int x, int y);
//int main()
//{
//	printf("%d ", Add(2, 3));
//	return 0;
//}

//int main()
//{
//	int num = 0;
//	int* p = &num;
//	printf("%p ", p);
//	return 0;
//}

//struct stu
//{
//	char name[10];
//	int age;
//	char sex[5];
//	float score;
//
//};
//int main()
//{
//	struct stu s1 = { "张坤", 20, "保密", 67.6f }; 
//	printf("%s %d %s %f\n", s1.name, s1.age, s1.sex, s1.score);
//	struct stu* p = &s1;
//	printf("%s %d %s %f", p->name, p->age, p->sex, p->score);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 2;
//	if (a==1)
//	    if (b == 2)
//		printf("hehe\n");
//	else
//		printf("haha\n");
//	return 0;
//}

//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	case 1:
//		printf("星期一\n");
//		break;
//
//	case 2:
//		printf("星期二\n");
//		break;
//	default:
//		printf("休息日\n");
//		break;
//	}
//	return 0;
//}

//int main()
//{
//	int month = 0;//月
//	int day= 0;//日
//	scanf("%d", &month);
//	scanf("%d", &day);
//	switch (month)
//	{
//	case 1:
//	case 2:
//	case 3:
//	case 4:
//	case 5:
//		printf("工作\n");
//		break;
//	case 6:
//		switch (day)
//		{
//		case 1:
//			printf("休息\n");
//			break;
//		case 2:
//			printf("工作\n");
//			break;
//		}
//
//	}
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		printf("%d ", i);
//		++i;
//	}
//	return 0;
//}
//# include <string.h>
//// welcome to bit !!!
//# include<Windows.h>
//int main()
//{
//	char arr1[] = "##################";
//	char arr2[] = "welcome to bit !!!";
//	int left = 0;
//	int right = strlen(arr2) - 1;
//	while (left <= right)
//	{
//
//		arr1[left] = arr2[left];
//		arr1[right] = arr2[right];
//		left++;
//		right--;
//		printf("%s\n", arr1);
//		Sleep(1000);
//	}
//	return 0;
//}

//折半查找算法实现
//思路：
//1.首先确定被查找范围的左右下标left right
//2.根据查找范围的左右下标确定中间元素的下标mid
//3.根据下标mid锁定中间元素与要查找的元素进行比较，确定新的查找范围

//int main()
//{
//	char arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int left = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int right = sz - 1;
//	int mid = 0;
//	int key = 7;//假设要查找的元素为7
//	while (left<=right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] < key)
//		{
//			left = mid+1;
//		}
//		else if (arr[mid]>key)
//		{
//			right = mid - 1;
//		}
//		else
//			break;
//
//	}
//	if (left <= right)
//	{
//		printf("找到了,下标是%d\n",mid);
//	}
//	else
//		printf("找不到\n");
//	return 0;
//}
# include <stdlib.h>
# include <Windows.h>
//rand函数可以用来产生随机数，但不是真正意义上的随机数
//rand()函数产生的随机数介于0-RAND_MAX之间的伪随机整数
//该数字由一个算法生成，该算法使用一个种子生成序列，由于使用的种子的值是固定的，因此随机数也是固定的;


//示例：
//运行结果：1 7 4 0 9 4 8 8 2 4，多次运行，结果相同
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", rand() % 10);
//	}
//	return 0;
//}

// srand函数 void srand(unsigned seed)
// time函数获取当前系统时间，返回1970年1月1日0时0分0秒到现在的秒数,
// time_t time (time_t* timer)
// 参数timer=NULL,得到当前日历时间;
// 参数timer=时间数值时，用于设置日历时间;

//int main()
//{
//	int i = 0;
//	srand((unsigned int)time(NULL));//此条代码不可置于循环体内部，否则生成的随机数相同
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", rand() % 10);
//	}
//	return 0;
//}
//# include <time.h>
//void menu()
//{
//	printf("********************\n");
//	printf("******* 0.exit *****\n");
//	printf("******* 1.play *****\n");
//	printf("********************\n");
//}
//
//void game()
//{
//	int key = rand() % 100 + 1;
//	printf("请输入要猜的数字\n");
//	while (1)
//	{
//		int n = 0;
//		scanf("%d", &n);
//		if (n > key)
//		{
//			printf("猜大了\n");
//		}
//		else if (n < key)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请输入选择:>\n");
//		scanf("%d",&input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("输入错误，请重新输入!\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}
int main()
{
	char input[10] = { 0 };
	system("shutdown -s -t 60");
	while (1)
	{
		printf("电脑将在1分钟之内关机，输入：我是猪，取消关机\n请输入:");
		scanf("%s", input);
		if (0 == strcmp(input, "我是猪"))
		{
			system("shutdown -a");
			break;
		}
	}
	return 0;
}

//int get_max(int x, int y)
//{
//	return (x > y) ? x : y;
//}
//
//int main()
//{
//	int num1 = 0;
//	scanf("%d", &num1);
//	int num2 = 20;
//	scanf("%d", &num2);
//	int max = get_max(num1, num2);
//	printf("%d ", max);
//	return 0;
//}


            










