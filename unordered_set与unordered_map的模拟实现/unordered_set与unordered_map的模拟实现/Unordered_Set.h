#pragma once

#include "HashBucket.h"
template<class K, class Hash = HashFunc<K>>
class Unordered_Set
{
	struct SetKeyOfT
	{
		const K& operator()(const K& key)
		{
			return key;
		}
	};
public:
	typedef typename HashTable<K, K, SetKeyOfT, Hash>::iterator iterator;

	iterator begin()
	{
		return _ht.begin();
	}

	iterator end()
	{
		return _ht.end();
	}

	//����
	pair<iterator, bool> insert(const K& key)
	{
		return _ht.Insert(key);
	}
	//����
	iterator find(const K& key)
	{
		return _ht.Find(key);
	}
	//ɾ��
	bool erase(const K& key)
	{
		return _ht.Erase(key);
	}
private:
	HashTable<K, K, SetKeyOfT, Hash> _ht;
};


void test_set1()
{
	Unordered_Set<int> us;
	us.insert(3);
	us.insert(1);
	us.insert(5);
	us.insert(15);
	us.insert(45);
	us.insert(7);

	Unordered_Set<int>::iterator it = us.begin();
	while (it != us.end())
	{
		//*it += 100;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : us)
	{
		cout << e << " ";
	}
	cout << endl;
}





