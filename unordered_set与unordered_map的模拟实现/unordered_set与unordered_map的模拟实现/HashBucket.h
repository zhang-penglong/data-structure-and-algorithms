#pragma once


#include <vector>
#include <string>
#include <iostream>
using namespace std;


template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

// 特化
template<>
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto e : s)
		{
			hash += e;
			hash *= 31;
		}
		return hash;
	}
};

template<class T>
struct HashNode
{
	HashNode<T>* _next;
	T _data;
	HashNode(const T& data)
		:_data(data)
		, _next(nullptr)
	{}
};

//哈希表前置声明
template<class K, class T, class KeyOfT, class Hash>
class HashTable;

template<class K, class T, class KeyOfT, class Hash>
struct __HTIterator
{
	typedef HashNode<T> Node;
	typedef HashTable<K, T, KeyOfT, Hash> HT;
	typedef __HTIterator<K, T, KeyOfT, Hash> Self;

	Node* _node;//记录当前结点
	HT* _ht;//哈希表指针用于查找下一个桶

	__HTIterator(Node* node, HT* ht)
		:_node(node)
		, _ht(ht)
	{}

	Self& operator++()
	{
		//当前桶未遍历结束,指向桶中下一个结点
		if (_node->_next != nullptr)
		{
			_node = _node->_next;
		}
		//当前桶已遍历结束,寻找下一个桶
		else
		{
			//首先确定当前桶在哈希表中的位置
			Hash hs;
			KeyOfT kot;
			size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();
			//查找下一个桶
			++hashi;
			while (hashi < _ht->_tables.size())
			{
				//找到下一个桶,it指向桶的头节点
				if (_ht->_tables[hashi] != nullptr)
				{
					_node = _ht->_tables[hashi];
					break;//++结束
				}
				++hashi;
			}
			//跳出循环具有两种情形
			//case1:未找到哈希表中的下一个桶,采用空指针作为迭代器的end()
			//case2:找到哈希表中的下一个桶的头节点,返回迭代器
			if (hashi == _ht->_tables.size())
			{
				//case1
				_node = nullptr;
			}
		}
		//case2
		return *this;
	}

	T& operator*()
	{
		return _node->_data;
	}

	T* operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& sl) const
	{
		return _node != sl._node;
	}

	bool operator==(const Self& sl) const
	{
		return _node == sl._node;
	}
};

template<class K, class T,class KeyOfT, class Hash>
class HashTable
{
	template<class K, class T, class KeyOfT, class Hash>
	friend struct __HTIterator;

	typedef HashNode<T> Node;
public:
	typedef __HTIterator<K, T, KeyOfT, Hash> iterator;

	iterator end()
	{
		return iterator(nullptr, this);
	}

	iterator begin()
	{
		for (size_t i = 0; i < _tables.size(); i++)
		{
			// 哈希表中第一个桶的第一个节点
			if (_tables[i])
			{
				return iterator(_tables[i], this);
			}
		}
		return end();
	}

	//构造函数
	HashTable(size_t n = 10)
	{
		_tables.resize(n, nullptr);
		_n = 0;
	}
	//析构函数
	~HashTable()
	{
		for (size_t i = 0; i < _tables.size(); i++)
		{
			Node* cur = _tables[i];
			while (cur != nullptr)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}
			_tables[i] = nullptr;
		}
	}

	//拷贝构造函数  ht2(ht1)
	HashTable(const HashTable& ht)
	{
		Hash hs;
		KeyOfT kot;
		 //开辟相同大小的空间
		_tables.resize(ht._tables.size());
		//遍历旧表,头插到新表
		for (size_t i = 0; i < ht._tables.size(); i++)
		{
			Node* cur = ht._tables[i];
			while (cur != nullptr)
			{
				Node* next = cur->_next;

				//计算新表的插入位置
				size_t hashi = hs(kot(cur->_data)) % _tables.size();

				cur->_next = _tables[hashi];
				_tables[hashi] = cur;
				cur = next;
			}
		}
		_n = ht._n;
	}

	//赋值运算符重载 ht2=ht1
	HashTable& operator=(HashTable ht)
	{
		_tables.swap(ht._tables);
		swap(_n, ht._n);

		return *this;
	}
	//查找到返回哈希表指针与当前哈希表的结点指针
	//若查找不到返回哈希表指针与空指针
	iterator Find(const K& key)
	{
		KeyOfT kot;
		Hash hs;
		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];
		while (cur != nullptr)
		{
			//仿函数对象kot获取结点中的键值
			if (kot((cur->_data)) == key)
			{
				return iterator(cur, this);
			}
			cur = cur->_next;
		}
		return iterator(nullptr, this);
	}


	pair<iterator,bool> Insert(const T& data)
	{
		KeyOfT kot;
		Hash hs;

		iterator ret = Find(kot(data));
		if (ret != end())
		{
			//键值key原先已存在,插入失败,返回已存在结点的迭代器并且返回false
			return make_pair(ret, false);
		}

		if (_n == _tables.size())
		{
			vector<Node*> _newtables(_tables.size() * 2);
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur != nullptr)
				{
					Node* nextnode = cur->_next;
				
					size_t hashi = hs(kot(cur->_data)) % _newtables.size();
					cur->_next = _newtables[hashi];
					_newtables[hashi] = cur;
					cur = nextnode;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(_newtables);
		}

		size_t hashi = hs(kot(data)) % _tables.size();
		Node* newnode = new Node(data);

		newnode->_next = _tables[hashi];
		_tables[hashi] = newnode;
		++_n;

		//键值key原先不存在,插入成功,返回新结点的迭代器并且返回true
		return make_pair(iterator(newnode, this), true);
	}

	bool Erase(const K& key)
	{
		Hash hs;
		KeyOfT kot;
		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];
		Node* prev = nullptr;
		while (cur != nullptr)
		{
			//仿函数(KeyOfT)对象kot获取数据data的键值key确定待删除数据的位置
			if (kot((cur->_data)) == key)
			{
				//删除
				if (prev != nullptr)
				{
					prev->_next = cur->_next;
				}
				else
				{
					_tables[hashi] = cur->_next;
				}
				delete cur;

				--_n;
				return true;
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return false;
	}
private:
	vector<Node*> _tables;
	size_t _n;
};