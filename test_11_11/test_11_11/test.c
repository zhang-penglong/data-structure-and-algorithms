# define  _CRT_SECURE_NO_WARNINGS 1
# include "Test.h"

//二叉树前序遍历
void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL->");
		return;
	}
	printf("%d->",root->val);
	PrevOrder(root->left);
	PrevOrder(root->right);
}
//二叉树的中序遍历 
//左子树->根->右子树
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL->");
		return;
	}
	InOrder(root->left);
	printf("%d->", root->val);
	InOrder(root->right);
}
//二叉树的后序遍历
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL->");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d->", root->val);
}
//结点个数
int TreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	//
	return TreeSize(root->left) + TreeSize(root->right) + 1;
}
//叶子结点个数
int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL&&root->right == NULL)
	{
		return 1;
	}
	// root->left!=NULL  root->right!=NULL
	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}
//二叉树第k层节点个数
//当前树的第k层=左子树的k-1层+右子树的k-1层
int TreeKLevel(BTNode* root, int k)
{
	assert(k > 0);
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return TreeKLevel(root->left, k - 1) + TreeKLevel(root->right, k - 1);

}
//二叉树的查找
BTNode* TreeFind(BTNode* root, int x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->val == x)
	{
		return root;
	}
	//查找左子树
	BTNode* ret = NULL;
	ret = TreeFind(root->left, x);
	if (ret != NULL)
	{
		return ret;
	}
	//ret==NULL
	//查找右子树
	ret = TreeFind(root->right, x);
	if (ret != NULL)
	{
		return ret;
	}
	return NULL;

}
int main()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	              
	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node4->left = node5;
	node4->right = node6;

	PrevOrder(node1);
	printf("\n");
	InOrder(node1);
	printf("\n");
	PostOrder(node1);
	printf("\n");

	int k=TreeSize(node1);
	printf("%d", k);
	printf("\n");

	int m = TreeLeafSize(node1);
	printf("%d", m);
	printf("\n");
	BTNode* dest=TreeFind(node1, 4);
	printf("%p\n", dest);

	return 0;
}
























