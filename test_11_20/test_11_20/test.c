# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
# include "stack.h"
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
////堆的向下调整算法
////思路:堆向下调整算法的前提为左右子树皆为小堆或者左右子树皆为大堆;
////首先根据左右子树是大堆还是小堆，调整为大堆或者小堆;
////若左右子树皆为大堆,首先找到左右孩子中的较大值;
////其次与其父节点比较,当较大值大于其父节点时交换父节点与孩子节点出的数值;
////按此循环,直至整个数组调整完成;
//void swap(int* p, int* q)
//{
//	int tmp = *p;
//	*p = *q;
//	*q = tmp;
//}
//void AdjustDown(int*a, int n, int parent)
//{
//	int child = 2 * parent + 1;
//	while (child < n)
//	{
//		//寻找左右孩子中的大孩子
//		//child+1<n是为了保证右孩子存在,防止数组越界访问;
//		if (child+1<n && a[child] < a[child + 1])
//		{
//			child++;
//		}
//		//大孩子与父节点比较,满足条件交换
//		if (a[child]>a[parent])
//		{
//			swap(&a[child], &a[parent]);
//			parent = child;
//			child = 2 * child + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
//void HeapSort(int* a, int n)
//{
//	//建堆
//	//升序建大堆,降序建小堆;
//	//从倒数第一个非叶子结点开始向下调整建堆,既可以建大堆,也可以建小堆,左右子树可看做大堆,又可以看做小堆;
//	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
//	{
//		AdjustDown(a, n, i);
//	}
//	//排序
//	//建立大堆之后,堆顶数据最大,交换堆顶元素与数组最后一个元素的位置;
//	//将交换后堆中最后一个元素不再认为是堆中的元素,剩下的n-1个数据继续调整为堆,并且按此循环;
//	//当堆中仅剩一个元素,即为数组最小的元素,此时循环终止;
//	int end = n - 1;//数组最后一个元素的下标;
//	while (end > 0)
//	{
//		swap(&a[0], &a[end]);
//		AdjustDown(a, end, 0);
//		end--;
//	}
//}
//int main()
//{
//	int a[] = { 1,2,3,7,9,2,4,6,4,6,8,9,10};
//	int n = sizeof(a) / sizeof(a[0]);
//	HeapSort(a, n);
//	PrintArray(a, n);
//	return 0;
//}

//函数递归过程中,深度太深会导致栈溢出,栈区的空间一般为8M,10M;什么可以取代递归那？
//递归就是再次调用函数的功能,只不过每次递归调用的参数不同;
//使用循环就可不断调用函数，但是循环条件是固定不变的，如果将每一次循环的参数记录下来，就可模拟递归;
//快速排序非递归思路：
//非递归实现快排的思路是使用一个栈来保存待排序序列的左右边界，然后在栈不为空的情况下，取出栈顶的左右边界;
//对该区间进行划分，将划分后的左右子序列的左右边界入栈，直到栈为空为止;
void swap(int*p, int* q)
{
	int tmp = *p;
	*p = *q;
	*q = tmp;

}
int PartSort(int* a, int left, int right)
{
	int keyi = left;
	while (left < right)
	{
		while (left<right && a[right] >= a[keyi])
		{
			right--;
		}
		while (left<right && a[left] <= a[keyi])
		{
			left++;
		}
		swap(&a[left], &a[right]);	
	}
	swap(&a[keyi], &a[left]);
	return left;
}

void QuickSortNonR(int* a, int begin, int end)
{
	Stack st;
	InitStack(&st);
	StackPush(&st, end);
	StackPush(&st, begin);
	while (!StackEmpty(&st))
	{
		int left = StackTop(&st);
		StackPop(&st);

		int right = StackTop(&st);
		StackPop(&st);
		int keyi = PartSort(a, left, right);
		//[left keyi-1] keyi [keyi+1 right]
		if (right > keyi + 1)
		{
			StackPush(&st, right);
			StackPush(&st, keyi+1);
		}
		if (keyi - 1 > left)
		{
			StackPush(&st, keyi - 1);
			StackPush(&st, left);
		}
	}
	DestroyStack(&st);
}
int main()
{
	int a[] = { 1, 5, 3, 6, 8, 9, 2, 10, 7 };
	int begin = 0;
	int end = sizeof(a) / sizeof(a[0]) - 1;
	QuickSortNonR(a, begin, end);
	PrintArray(a, end + 1);
	return 0;
}