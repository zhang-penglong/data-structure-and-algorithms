﻿# define  _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct Goods
{
	string _name;// 名字
	double _price; // 价格
	int _evaluate; // 评价
	Goods(const char* str, double price, int evaluate)
		:_name(str)
		,_price(price)
		,_evaluate(evaluate)
	{}
};
//struct ComparePriceLess
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		return gl._price < gr._price;
//	}
//};
//struct ComparePriceGreater
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		return gl._price > gr._price;
//	}
//};
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), ComparePriceLess());//按价格升序排序
//	vector<Goods>::iterator it1 = v.begin();
//	while (it1 != v.end())
//	{
//		cout << "商品:" << (*it1)._name << " 价格:" << (*it1)._price << " 评价:" << (*it1)._evaluate << endl;
//		++it1;
//	}
//	cout << endl;
//
//	sort(v.begin(), v.end(), ComparePriceGreater());//按价格降序排序
//	vector<Goods>::iterator it2 = v.begin();
//	while (it2 != v.end())
//	{
//		cout << "商品:" << (*it2)._name << " 价格:" << (*it2)._price << " 评价:" << (*it2)._evaluate << endl;
//		++it2;
//	}
//	cout << endl;
//}


//class Date
//{
//public:
//	Date(int year = 2024, int month = 5, int day = 1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	void Swap(int y, int m,int d)
//	{
//		std::swap(_year, y);
//		std::swap(_month, m);
//		std::swap(_day, d);
//		auto func = [=] {
//			cout << "year=" << y << endl;
//			cout << "month=" << m << endl;
//			cout << "day=" << d << endl;
//			cout << "this: " << this << endl;
//		};
//		func();
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d(2024, 5, 10);
//	d.Swap(2000, 4, 15);
//	cout << &d << endl;
//	return 0;
//}
//int main()
//{               //[捕捉列表] ... {函数体}
//	auto func = [=](int x)mutable-> int {x++; std::cout << "x=" << x << std::endl; return x; };
//	func(10);
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int b = 20;
//	auto func = [&] {
//		++a;
//		cout << "a=" << a << endl;
//		++b;
//		cout << "b=" << b << endl;
//	};
//	func();
//	return 0;
//}

//struct Lambda
//{
//    void Print() {
//        std::cout << "Hello Linux!" << std::endl;
//    };
//    void lambda() {
//        int a = 10;
//        int b = 20;
//        auto func = [&,a,this] {
//            //++a;//error
//            ++b;
//            std::cout << "a=" << a << std::endl;
//            std::cout << "b=" << b << std::endl;
//            this->Print();
//        };
//        func();
//    }
//};
//
//int main()
//{
//    Lambda lba;
//    lba.lambda();
//}
//class Date
//{
//public:
//	Date(int year = 2024, int month = 5, int day = 1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	void Show()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	auto function = ([=, &a, &b] {
//		++a;
//		++b;
//		//++c;//error
//		std::cout << "a=" << a << std::endl;
//		std::cout << "b=" << b << std::endl;
//		std::cout << "c=" << c << std::endl;
//		}
//	);
//	function();
//
//	return 0;
//}


//int a = 10;//a-->全局变量
//
//int main() 
//{
//	int b = 20;//b-->局部变量
//	static int c = 0;
//	auto func = [&] {
//		std::cout << "a=" << a << std::endl;
//		std::cout << "b=" << b << std::endl;
//		std::cout << "c=" << c << std::endl;
//	};
//	func();
//	return 0;
//}



//int main() {
//    int a = 1;
//    int b = 2;
//
//     //错误示例：尝试捕捉非父作用域中的局部变量
//     if (a > 0) 
//     {
//         int c = 3;
//         auto lambda = [b]{ std::cout << b<< std::endl; };
//     }
//
//    //// 正确示例:捕捉父作用域中的局部变量
//    //if (b > 0) 
//    //{
//    //    int c = 30;
//    //    auto lambda = [c]() { std::cout << c << std::endl; };
//    //    lambda();
//    //}
//    return 0;
//}


//int main()
//{
//	auto Add = [](int first, int second) {
//		return first + second;
//	};
//	std::cout << Add(100, 200) << std::endl;
//
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	auto func = [&, a]()mutable{ ++a; ++b; std::cout << (a + b) << std::endl; };
//	func();
//}


//class Rate
//{
//public:
//	Rate(double rate) :
//		_rate(rate)
//	{}
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//int main()
//{
//	// 仿函数对象r1
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//	// lambda表达式
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year;};
//	r2(10000, 2);
//	return 0;
//}


//void (*pf)();
//int main()
//{
//	auto f1 = [] {cout << "hello world" << endl; };
//	auto f2 = [] {cout << "hello world" << endl; };
//
//    //f1 = f2;   //error
//	
//	// 允许使用一个lambda表达式拷贝构造一个新的副本
//	auto f3(f2);
//	f3();
//
//	// 可以将lambda表达式赋值给相同类型的函数指针
//	pf = f2;
//	pf();
//	return 0;
//}


int main()
{
	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,
   3 }, { "菠萝", 1.5, 4 } };
	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
		return g1._price < g2._price; });//按价格升序排序
	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
		return g1._price > g2._price; });//按价格降序排序
	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
		return g1._evaluate < g2._evaluate; });//按评价升序排序
	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
		return g1._evaluate > g2._evaluate; });//按评价降序排序
	return 0;
}

























