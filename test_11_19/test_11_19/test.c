# define  _CRT_SECURE_NO_WARNINGS 1
# include <stdio.h>
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void swap(int* p, int* q)
{
	int tmp = *p;
	*p = *q;
	*q = tmp;
}
//单趟排序(升序)
//选取待排序序列中的某元素作为基准值key,使得左子序列中所有元素均小于基准值，右子序列中所有元素均大于基准值;
//单趟排序一次就会唯一确定一个元素来到最终排序所确定的位置;
//单趟排序需要返回基准值的下标

//int PartSort(int* a, int left, int right)
//{
//	int key = a[left];//选取左边作为基准值，则右边先走
//	while (left < right)//left与right相遇,左边找大，右边找小的循环终止;
//	{
//		//右边找小
//		while (a[right] > key)
//		{
//			right--;
//		}
//		//左边找大
//		while (a[left] < key)
//		{
//			left++;
//		}
//		//交换
//		swap(&a[left], &a[right]);
//	}
//	//left==right
//	swap(&key, a[left]);
//	return left;
//}

//int PartSort(int* a, int left, int right)
//{
//	int key = a[left];//选取左边作为基准值，则右边先走
//	while (left < right)//left与right相遇,左边找大，右边找小的循环终止;
//	{
//		//右边找小
//		//若原序列为升序序列 1 2 3 4 5 6 key=1 left=0 right=5
//		//执行完下述代码最终right=0 
//		while (a[right] > key)
//		{
//			right--;
//		}
//		//左边找大
//		//对于升序序列1 2 3 4 5 6，最终执行完下述代码left=5
//		while (a[left] < key)
//		{
//			left++;
//		}
//		//left=5,right=0,但是出现left<right循环本应该停止;
//		//交换
//		swap(&a[left], &a[right]);
//	}
//	//left==right
//	swap(&key, a[left]);
//	return left;
//}


//int PartSort(int* a, int left, int right)
//{
//	int key = a[left];//选取左边作为基准值，则右边先走
//	while (left < right)//left与right相遇,左边找大，右边找小的循环终止;
//	{
//		//右边找小
//		//假设原序列为 6 1 2 3 6 7 6 8 6 9 10 left=0,right=10,key=6;
//		while (left<right && a[right] > key) 
//		{
//			right--;
//		}
//		//right=8 a[right]=6
//		//左边找大
//		while (left<right && a[left] < key)
//		{
//			left++; 
//		}
//		//left=0  a[left]=6;
//		swap(&a[left], &a[right]); //6和6交换,无任何意义;
//		//left=0,right=8;
//		//程序执行到此会陷入死循环
//	}
//	//left==right
//	swap(&key, a[left]);
//	return left;
//}

//int PartSort(int* a, int left, int right)
//{
//	int key = a[left];//选取左边作为基准值，则右边先走
//	while (left < right)//left与right相遇,左边找大，右边找小的循环终止;
//	{
//		//右边找小
//		//假设原序列为 6 1 2 3 6 7 6 8 6 9 10 left=0,right=10,key=6;
//		while (left<right && a[right] >= key)
//		{
//			right--;
//		}
//		//right=8 a[right]=6
//		//左边找大
//		while (left<right && a[left] <= key)
//		{
//			left++;
//		}
//		//left=0  a[left]=6;
//		swap(&a[left], &a[right]); //6和6交换,无任何意义;
//		//left=0,right=8;
//		//程序执行到此会陷入死循环
//		//进行严格找小或者找大,跳过相等;
//	}
//	//left==right
//	swap(&key, a[left]);//此处交换了局部变量与数组的数值，但是数组本身并没有进行交换;   
//	return left;
//}

//思考1: 为什么left与right相遇位置处的数值比key要小？
//思考2：为什么左边取基准值key,右边先走？

//思考1
//left与right相遇有俩种情形 
// 6 1 2 7 9 10 4 5 10 8 left=0 right=9 key=6
//right先走, right=7,a[right]=5;
//left后走，left=3 ,a[left]=7;
//交换
// 6 1 2 5 9 10 4 7 10 8
//right先走, right=6,a[right]=4;
//left后走，left=4 ,a[left]=9;
//交换
// 6 1 2 5 4 10 9 7 10 8
//right先走, right=4,a[right]=4;
//left==right,left不动
//相遇的第一种情形：right动left不动,相遇位置为left位置,left位置与前一个right位置进行了交换,交换之后left位置比key小;

// 6 1 2 5 9 3 4 7 10 8 left=0 right=9 key=6
//right先走, right=6,a[right]=4;
//left后走，left=4 ,a[left]=9;
//交换
// 6 1 2 5 4 3 9 7 10 8
//right先走，right=5,a[right]=3;
//由于left<right,left向前移动一步left==right
//相遇的第二种情形:right不动left动,相遇位置为right位置,此时left找大没有找到与right相遇,而right位置一定比key小;

//思考2 (反证法:假设left先走，会发生什么?)
// 6 1 2 5 9 10 4 7 10 8 left=0 right=9 key=6
//left先走，left=4 ,a[left]=9;
//right后走, right=6,a[right]=4;
//交换
// 6 1 2 5 4 10 9 7 10 8
//left先走，left=5 ,a[left]=10;
//right后走，由于left<right,right向前一步 right==left
//left不动right动，right找小未找到相遇于left位置，此时left找到的值必大于key,交换必导致错误;

//int PartSort(int* a, int left, int right)
//{
//	int keyi = left;
//	while (left < right)
//	{
//		while (left<right && a[right] >= a[keyi])
//		{
//			right--;
//		}
//		while (left<right && a[left] <= a[keyi])
//		{
//			left++;
//		}
//	
//		swap(&a[left], &a[right]);
//		
//	}
//	swap(&a[keyi], &a[left]);
//	return left;
//}
//void QuickSort(int*a, int begin, int end)
//{
//	//递归终止的条件
//	//1.区间不存在(begin>end)或者只有一个元素(begin=end)
//	if (begin >= end)
//		return;
//	int keyi = PartSort(a, begin, end);
//	// 一趟排序原排序区间划分为如下三部分
//	//[begin keyi-1]U[keyi]U[keyi+1,end]
//	QuickSort(a, begin, keyi - 1);
//	QuickSort(a, keyi + 1, end);
//}

//快排的时间复杂度是多少？最好情况是多少？最坏情况又为多少？
//假设数组中有N个数据
//最好情况:当keyi每次取中间值,则每趟排序需要遍历N个数据,一共遍历logN趟则时间复杂度为O(N*logN);
//最坏情况:当数组为有序数组(升序/降序),第一趟遍历N个数据，此时keyi=1,排好第一个数据，第二趟遍历N-2个数据,......
//时间复杂度为O(N^2)

//对单趟排序进行优化，使得key取中间值,keyi为中间值的下标
//三数取中
int GetMidi(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] > a[mid])
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		//mid为最小值
		else if (a[left] > a[right])
		{
			return right;
		}
		else
		{
			return left;
		}
	}
	else
	{
		if (a[mid] < a[right])
		{
			return mid;
		}
		//mid为最大值
		else if (a[left]>a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}

//单趟排序hoare版本
//思路如下:
//基本思想是选取一个基准值，将序列分为两部分,一部分是小于基准值的,另一部分是大于基准值的;
//然后对这两部分分别进行递归排序，最终得到有序序列;
int PartSort1(int* a, int left, int right)
{

	int midi = GetMidi(a, left, right);
	swap(&a[left], &a[midi]);

	int keyi = left;
	while (left < right)
	{
		while (left<right && a[right] >= a[keyi])
		{
			right--;
		}
		while (left<right && a[left] <= a[keyi])
		{
			left++;
		}

		swap(&a[left], &a[right]);

	}
	swap(&a[keyi], &a[left]);
	return left;
}
//单趟排序挖坑法
//思路如下：
//1.选择数组第一个数作为基准数,基准数的位置形成一个坑位;
//2.定义两个指针left与right分别指向数组的第一个数和最后一个数;
//3.从right开始向前遍历,找到第一个小于基准数的数a[right]，将其赋值给a[left],a[right]成为一个新的坑位;
//4.从left开始向后遍历,找到第一个大于基准数的数a[left]，将其赋值给a[right],a[left]成为一个新的坑位;
//5.重复步骤3和4，直到left > right;
//6.将基准数填入最后一个坑中,a[left]=key;
int PartSort2(int* a, int left, int right)
{
	int midi = GetMidi(a, left, right);
	swap(&a[left], &a[midi]);

	int key = a[left];
	int hole = left;
	while (left < right)
	{
		//右边先走
		while (left < right && a[right] >= key)
		{
			right--;
		}
		a[hole]=a[right];
		hole = right;
		//左边再走
		while (left < right&&a[left] <= key)
		{
			left++;
		}
		a[hole] = a[left];
		hole = left;
	}

	a[hole] = key;
	return hole;
}

//单趟排序快慢指针法
//思路如下:
//1.选取待排序序列的第一个元素作为基准值key; 
//2.定义两个指针prev和cur,prev指针指向序列开头,cur指针指向prev指针的后一个位置;
//3.从cur开始向前遍历，如果遇到比key小的元素，就将prev指针向后移动一位，并交换prev和cur指向的元素;
//4.遍历结束后，将key与prev指向的元素交换位置，此时prev指向的位置就是key的最终位置;
//int PartSort3(int* a, int left, int right)
//{
//	int keyi = left;
//	int prev= left;
//	int cur = prev + 1;
//	while (cur <= right)
//	{
//		if (a[cur] < a[keyi])
//		{
//			swap(&a[cur], &a[++prev]);
//		}
//		cur++;
//	}
//	swap(&a[keyi], &a[prev]);
//	return prev;
//}
int PartSort3(int* a, int left, int right)
{
	int keyi = left;
	int prev = left;
	int cur = prev + 1;
	while (cur <= right)
	{
		//(++prev)!=cur为了防止++prev与cur指向相同数值,此时交换毫无意义;
		if (a[cur] < a[keyi]&&(++prev)!=cur)
		{
			swap(&a[cur], &a[prev]);
		}
		cur++;
	}
	swap(&a[keyi], &a[prev]);
	return prev;
}
void QuickSort(int*a, int begin, int end)
{
	//递归终止的条件
	//1.区间不存在(begin>end)或者只有一个元素(begin=end)
	if (begin >= end)
		return;
	int keyi = PartSort3(a, begin, end);
	// 一趟排序原排序区间划分为如下三部分
	//[begin keyi-1]U[keyi]U[keyi+1,end]
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);
}
int main()
{
	int a[] = { 6, 1, 2, 7, 9, 3, 4, 5, 10, 8 };
	int n = sizeof(a) / sizeof(a[0]);
	QuickSort(a, 0, n-1);
	PrintArray(a, sizeof(a) / sizeof(int));
	return 0;
}